#!/bin/bash

# takes each file in a directory and processes the file using tree-tagger

if [ $# -ne 1 ]
then
        echo "Usage: $0 <input_directory>"
        echo
        echo "where <input_directory> is the directory where the files to be annotated are."
        echo
        exit
fi

input_dir=$1
treetagger_path=/home/desouza/Tools/tree-tagger
tt_en=$treetagger_path/cmd/tree-tagger-english

echo "Input directory: $input_dir"

for file in $input_dir/*
do
    $tt_en $file > $file.tt
done

echo "Done."
