#include "SOGP_aux.h"
#include <string.h>

//Basic cascades - Not efficient
ReturnMatrix SOGPKernel::kernelM(const ColumnVector& in, const Matrix &BV){
  ColumnVector k(BV.Ncols());
  
  stringstream temp;
  temp << "OnlineGP:: KernelM:: k: ";
  for(int i=1;i<=BV.Ncols();i++){
    k(i)=kernel(in,BV.Column(i));
    //Correction to avoid numerical instability
    //if (k(i) < 0.5) k(i) = 0.5;
    temp << k(i) << " ";
  }
  
   Write2Log2("./QE.log", temp.str());
  
  k.Release();
  return k;
}
double SOGPKernel::kstar(const ColumnVector& in){
  return kernel(in,in);
}
double SOGPKernel::kstar(){
  ColumnVector foo(1);
  foo(1)=0;
  return kernel(foo,foo);
}
//RBF
double RBFKernel::kernel(const ColumnVector &a, const ColumnVector &b){ 
 double d = a.Nrows();
 
 stringstream temp;
 temp << "OnlineGP:: RBFkernel:: ";
 
  if(d!=widths.Ncols()){//Expand if necessary
    //printf("RBFKernel:  Resizing width to %d\n",(int)d);
      temp << "Expansion Needed \n d = " << d << "\t A = " << A;
    double wtmp=widths(1);
    widths.ReSize(d);
    for(int i=1;i<=d;i++)
      widths(i)=wtmp;
  }
  //I think this bumps up against numerical stability issues.
 double answer;
 //To DO: Determine the necessary value of 10000... for numerical stability
 answer = A*exp(-(1/(2*d)) * SumSquare(SP(a-b,widths.t()))/1000000);
 /*double t0 = SumSquare(SP(a-b,widths.t()));
 temp << "\n \t t0 = " << t0;
 double t1 = -(1/(2*d))* SumSquare(SP(a-b,widths.t()))/1000;
 temp << "\n \t t1 = " << t1;
 double t2 = A*exp(t1);
 temp << "\n \t t2 = " << t2;
 
 Write2Log2("./QE.log", temp.str());
 */
 if (answer == 0) answer = 0.5;
 
  //return A*exp(-(1/(2*d)) * SumSquare(SP(a-b,widths.t())));
 return answer;
}
//POL
double POLKernel::kernel(const ColumnVector &a, const ColumnVector &b){
  double d = a.Nrows();
  double resp=1;
  double inner = (a.t()*b).AsScalar();
  for(int i=1;i<=scales.Ncols();i++)
    resp += pow((inner/(d*scales(i))),i);
  return resp;
}
double LINKernel::kernel(const ColumnVector &a, const ColumnVector &b){
  double d = a.Nrows();
  double resp=1;
  double inner = ((a.t()*b).AsScalar())/d;
  //for(int i=1;i<=scales.Ncols();i++)
  //  resp += pow((inner/(d*scales(i))),i);
  return inner;
}

//-------------------------------------------------------------
//Newmat printers
void printRV(RowVector rv,FILE *fp,const char *name,bool ascii){
  if(name)
    fprintf(fp,"%s ",name);
  fprintf(fp,"%d:",rv.Ncols());
  for(int i=0;i<rv.Ncols();i++)
    if(ascii)
      fprintf(fp,"%lf ",rv(i+1));
    else
      fwrite(&rv(i+1),sizeof(double),1,fp);
  fprintf(fp,"\n");
}
void readRV(RowVector &rv,FILE *fp,const char *name,bool ascii){
  if(name){
    char tn[128];//bad
    fscanf(fp,"%s ",tn);
    if(strcmp(tn,name))
      printf("readRV: Expected '%s', got '%s'\n",name,tn);
  }
  int len;
  fscanf(fp,"%d:",&len);
  rv.ReSize(len);
  for(int i=0;i<rv.Ncols();i++)
    if(ascii)
      fscanf(fp,"%lf ",&rv(i+1));
    else
      fread(&(rv(i+1)),sizeof(double),1,fp);
  fscanf(fp,"\n");
}
void printCV(ColumnVector cv,FILE *fp,const char *name,bool ascii){
  printRV(cv.t(),fp,name,ascii);
}
void readCV(ColumnVector &cv,FILE *fp,const char *name,bool ascii){
  RowVector rv;
  readRV(rv,fp,name,ascii);
  cv=rv.t();
}
void printMatrix(Matrix m,FILE *fp,const char *name,bool ascii){
  if(name)
    fprintf(fp,"%s ",name); 
  fprintf(fp,"(%d:%d)",m.Nrows(),m.Ncols());
  for(int i=0;i<m.Nrows();i++){
    for(int j=0;j<m.Ncols();j++){
      if(ascii)
	fprintf(fp,"%lf ",m(i+1,j+1));
      else
	fwrite(&(m(i+1,j+1)),sizeof(double),1,fp);
    }
    if(ascii)fprintf(fp,"\n");
  }
  if(ascii)fprintf(fp,"\n");
}
void readMatrix(Matrix &m,FILE *fp,const char *name,bool ascii){
  if(name){
    char tn[128];
    fscanf(fp,"%s ",tn);
    if(strcmp(tn,name))
      printf("readMatrix: Expected '%s', got '%s'\n",name,tn);
  }
  int wid,hgt;
  fscanf(fp,"(%d:%d)",&wid,&hgt);
  m.ReSize(wid,hgt);
  for(int i=0;i<m.Nrows();i++){
    for(int j=0;j<m.Ncols();j++){
      if(ascii)
	fscanf(fp,"%lf ",&m(i+1,j+1));
      else
	fread(&(m(i+1,j+1)),sizeof(double),1,fp);
    } 
    if(ascii)fscanf(fp,"\n");
  }
  if(ascii)fscanf(fp,"\n");
}

void Write2Log2(string LogFileName, string s){
    ofstream LogFile;
    LogFile.open(LogFileName.c_str(), ios::app);
    if (LogFile.is_open()) {
        LogFile << /*buffer <<*/ s << endl;
        LogFile.close();
    }
}
