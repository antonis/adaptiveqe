/* 
 * File:   main.cpp
 * Author: Antonis Anastasopoulos
 *
 * Created on July 17, 2013, 10:54 AM
 */

#include <cstdlib>
#include <iostream>
#include <cstring>      // Needed for memset
#include <sys/socket.h> // Needed for the socket functions
#include <sys/types.h>
#include <netdb.h>      // Needed for the socket functions
#include <map>
#include <utility>
#include <sstream>      // std::istringstream
#include <math.h>
#include <cmath>
#include <climits>
#include <fstream>
#include <time.h>
#include <sys/timeb.h>
#include "simple-cmd-line-helper.h"
#include <boost/regex.hpp>
using namespace std;

/* Example Usage
 * ./onlineqeclient --limit 1000 --getLabels --trainSrc ../Resources/Demo/TRAIN/TRAIN.src --trainFeat ../Resources/Demo/TRAIN/TRAIN.feats 
 * --trainTrg ../Resources/Demo/TRAIN/TRAIN.trg --trainPe ../Resources/Demo/TRAIN/TRAIN.pe --trainResult ../Resources/Demo/Results/Demo.TrainResults.SVR.csv
 * --testFeat ../Resources/Demo/TEST/TEST.feats --testSrc ../Resources/Demo/TEST/TEST.src 
 * --testTrg ../Resources/Demo/TEST/TEST.trg --testPe ../Resources/Demo/TEST/TEST.pe 
 * --testResult ../Resources/Demo/Results/Demo.TestResults.SVR.csv
 */

int InitServerConnection();

int getMilliCount();

int getMilliSpan(int nTimeStart);

int main(int argc, char** argv) {
    
    AddFlag("--doEmpty", "\n\t\tTest on an empty online model.\n"
            "\t\tCannot be used with --doStatic or --doAdaptive flags at the same time.\n"
            "\t\tDefault is not to do this.", bool(false));
    AddFlag("--doStatic", "\n\t\tTrain an online model, test in batch fashion.\n"
            "\t\tDefault is not to do this.", bool(false));
    AddFlag("--doAdaptive", "\n\t\tTrain an online model, test in online fashion [Default].", bool(false));
    
    AddFlag("--getLabels", "\n\t\tWait for response after sending a <pe>. [Default is false]", bool(false));
    
    AddFlag("--limit", "\n\t\tLimit the training to the specified number of points.\n"
            "\t\tDefault value of maximum int shows that this is not used.", int(2<<17));
    AddFlag("--trainSrc", "\n\t\tThe file with the source sentences for training.", string(""));
    AddFlag("--trainTrg", "\n\t\tThe file with the target sentences for training.", string(""));
    AddFlag("--trainPe", "\n\t\tThe file with the post-edited sentences for training.", string(""));
    AddFlag("--trainFeat", "\n\t\tThe file with the features of the sentences for training.\n"
            "\t\tIf not provided you should set the Server to ask QuEst for the features.", string(""));
    AddFlag("--trainResult", "\n\t\tThe file to store the results of training.\n"
            "\t\tIf not provided no information is saved.", string(""));
    AddFlag("--testSrc", "\n\t\tThe file with the source sentences for testing.", string(""));
    AddFlag("--testTrg", "\n\t\tThe file with the target sentences for testing.", string(""));
    AddFlag("--testPe", "\n\t\tThe file with the post-edited sentences for testing.", string(""));
    AddFlag("--testFeat", "\n\t\tThe file with the features of the sentences for testing.\n"
            "\t\tIf not provided you should set the Server to ask QuEst for the features.", string(""));
    AddFlag("--testResult", "\n\t\tThe file to store the results of testing.\n"
            "\t\tIf not provided no information is saved.", string(""));
    
            
    ParseFlags(argc, argv);
    std::cout << "--doEmpty" << " " << CMD_LINE_BOOLS["--doEmpty"] << std::endl;
    std::cout << "--doStatic" << " " << CMD_LINE_BOOLS["--doStatic"] << std::endl;
    std::cout << "--doAdaptive" << " " << CMD_LINE_BOOLS["--doAdaptive"] << std::endl;
    std::cout << "--getLabels" << " " << CMD_LINE_BOOLS["--getLabels"] << std::endl;
    std::cout << "--limit" << " " << CMD_LINE_INTS["--limit"] << std::endl;
    std::cout << "--trainSrc" << " " << CMD_LINE_STRINGS["--trainSrc"] << std::endl;
    std::cout << "--trainTrg" << " " << CMD_LINE_STRINGS["--trainTrg"] << std::endl;
    std::cout << "--trainPe" << " " << CMD_LINE_STRINGS["--trainPe"] << std::endl;
    std::cout << "--trainFeat" << " " << CMD_LINE_STRINGS["--trainFeat"] << std::endl;
    std::cout << "--trainResult" << " " << CMD_LINE_STRINGS["--trainResult"] << std::endl;
    std::cout << "--testSrc" << " " << CMD_LINE_STRINGS["--testSrc"] << std::endl;
    std::cout << "--testTrg" << " " << CMD_LINE_STRINGS["--testTrg"] << std::endl;
    std::cout << "--testPe" << " " << CMD_LINE_STRINGS["--testPe"] << std::endl;
    std::cout << "--testFeat" << " " << CMD_LINE_STRINGS["--testFeat"] << std::endl;
    std::cout << "--testResult" << " " << CMD_LINE_STRINGS["--testResult"] << std::endl;
    
    bool UseFeats = true;
    if (CMD_LINE_STRINGS["--trainFeat"].empty() || CMD_LINE_STRINGS["--testFeat"].empty())
        UseFeats = false;
    bool WriteTrainResult = true;
    if (CMD_LINE_STRINGS["--trainResult"].empty())
        WriteTrainResult = false;
    if (!(CMD_LINE_BOOLS["--doEmpty"]) && (CMD_LINE_STRINGS["--trainFeat"].empty() != CMD_LINE_STRINGS["--testFeat"].empty())){
        std::cout << "You must have the same configuration for the features of train and test." << std::endl;
        std::cout << "The client cannot run and will exit." << std::endl;
        return -1;
    }
    if (!(CMD_LINE_BOOLS["--doEmpty"]) && (CMD_LINE_STRINGS["--trainSrc"].empty() || CMD_LINE_STRINGS["--trainTrg"].empty() || CMD_LINE_STRINGS["--trainPe"].empty())){
        std::cout << "You have not provided all [src,trg,pe] files for training. Aborting..." << std::endl;
        return -1;
    }
    if (CMD_LINE_STRINGS["--testSrc"].empty() || CMD_LINE_STRINGS["--testTrg"].empty() || CMD_LINE_STRINGS["--testPe"].empty()){
        std::cout << "You have not provided all [src,trg,pe] files for testing. Aborting..." << std::endl;
        return -1;
    }
    if (CMD_LINE_STRINGS["--testResult"].empty()){
        std::cout << "Warning: You haven't specified a file to store the results of the test. Are you sure?" << std::endl;
    }
    if ((CMD_LINE_BOOLS["--doEmpty"]) && (CMD_LINE_BOOLS["--doStatic"])){
        std::cout << "You have specified both doEmpty and doStatic. What do you want to do? Aborting..." << std::endl;
        return -1;
    }
    if ((CMD_LINE_BOOLS["--doStatic"]) && (CMD_LINE_BOOLS["--doAdaptive"])){
        std::cout << "You have specified both doStatic and doAdaptive. What do you want to do? Aborting..." << std::endl;
        return -1;
    }
        
        
    // Initialize connection with the OnlineQEServer
    int Serversocketfd = InitServerConnection();
    //cout << Serversocketfd << endl;
    
    int limit = CMD_LINE_INTS["--limit"];
    int DoEmpty = CMD_LINE_BOOLS["--doEmpty"];
    int DoStatic = CMD_LINE_BOOLS["--doStatic"];
    int DoAdaptive = CMD_LINE_BOOLS["--doAdaptive"];
    int getLabels = CMD_LINE_BOOLS["--getLabels"];
    //cout << getLabels <<endl;
 
    if (DoStatic)
        DoAdaptive = 0;
    if (DoEmpty)
        DoAdaptive = 0;
    if(!DoAdaptive & !DoStatic & !DoEmpty)
        DoAdaptive =1 ;
    
    stringstream tmpsrc, tmptrg, tmppe, tmpfeat;
    stringstream outfile;
    stringstream tmpsrc3, tmptrg3, tmppe3, tmpfeat3;
    stringstream outfile3;

    tmpfeat << CMD_LINE_STRINGS["--trainFeat"];
    tmpsrc << CMD_LINE_STRINGS["--trainSrc"];
    tmptrg << CMD_LINE_STRINGS["--trainTrg"];
    tmppe << CMD_LINE_STRINGS["--trainPe"];
    outfile << CMD_LINE_STRINGS["--trainResult"];
    tmpfeat3 << CMD_LINE_STRINGS["--testFeat"];
    tmpsrc3 << CMD_LINE_STRINGS["--testSrc"];
    tmptrg3 << CMD_LINE_STRINGS["--testTrg"];
    tmppe3 << CMD_LINE_STRINGS["--testPe"];
    outfile3 << CMD_LINE_STRINGS["--testResult"];
    


    // Establish connection with Server (by sending first random message)
    ssize_t bytes_received;
    char incoming_data_buffer[4096];
    stringstream s;
    char *msg = "Hello! I am here! :D \n";
    int len = strlen(msg);
    ssize_t bytes_sent = send(Serversocketfd, msg, len, 0);
    
    ifstream Sourcefile, Targetfile, PostEditfile, Featfile;
    ofstream Outfile;
    string src, trg, pe, src1, trg1, hter, feats;
    string str1 = "<seg id=";
    string str2 = "><src>";
    string str3 = "</src><trg>";
    string str4 = "</trg></seg>";
    string str5 = "><pe>";
    string str6 = "</pe></seg>";
    string str7 = "</trg><feat>";
    string str8 = "</feat></seg>";
    int count = 0;
    
    // Regex for prediction and labels
    std::string sre1 = "(<seg id=)(.*)(><pred>)(.*)(</pred></seg>\n)";
    std::string sre2 = "(<seg id=)(.*)(><label>)(.*)(</label></seg>\n)";
    boost::regex e1,e2;
    boost::cmatch matches;
    
    try{
        e1 = sre1;
    }
    catch (boost::regex_error& e){
        cout << "Error: Not well-structured regular expression for <pred>";
    }
    try{
        e2 = sre2;
    }
    catch (boost::regex_error& e){
        cout << "Error: Not well-structured regular expression for <label>";
    }


    
    int OutTimeStart;
    int TotalProcessTime;

    // If not empty, perform training
    if (!DoEmpty){
        //IDfile.open((tmpid.str()).c_str(), ios::in);
        Sourcefile.open ((tmpsrc.str()).c_str(), ios::in);
        Targetfile.open ((tmptrg.str()).c_str(), ios::in);
        PostEditfile.open ((tmppe.str()).c_str(), ios::in);
        if (UseFeats)
            Featfile.open ((tmpfeat.str()).c_str(), ios::in);
        
        if (!(Sourcefile.is_open() & Targetfile.is_open() & PostEditfile.is_open())){
            std::cout << "Error: Could not open one of the training files. Client will exit." << std::endl;
            return 1;
        }
        
        if (!(Featfile.is_open()) && UseFeats){
            std::cout << "Error: Could not open the training features file. Client will exit." << std::endl;
            return 1;
        }
        
        
        if (WriteTrainResult)
            Outfile.open((outfile.str()).c_str(), ios::out);

        if (WriteTrainResult && Outfile.is_open()) {
            // The header at the training output file
            if (getLabels)
                Outfile << "Instance \t Prediction \t Time to predict \t HTER \t Time to retrain" << endl;
            else
                Outfile << "Instance \t Prediction \t Time to predict" << endl;
        }
    
        // Start timer
        OutTimeStart = getMilliCount();
        
        while (getline(Sourcefile, src)){
            count++;
            getline(Targetfile, trg);
            getline(PostEditfile, pe);
            if (UseFeats){
                getline(Featfile, feats);
                //feats.append("\n");
            }
            
            if(count<=limit) {
                // Create output string and send <src><trg> pair
                std::stringstream sstm;
                if (UseFeats)
                    sstm << str1 << count << str2 << src << str3 << trg << str7 << feats << str8 << endl;
                else
                    sstm << str1 << count << str2 << src << str3 << trg << str4 << endl;
                string one = sstm.str();
                const char *msg1 = one.c_str();
                len = strlen(msg1);
                ssize_t bytes_sent = send(Serversocketfd, msg1, len, 0);
                int SendTimeStart = getMilliCount();
                
                // Send features
                /*if (UseFeats){
                    const char *msg6 = feats.c_str();
                    len = strlen(msg6);
                    bytes_sent = send(Serversocketfd, msg6, len, 0);
                }*/
                // Receive prediction
                memset(incoming_data_buffer, '\0', sizeof incoming_data_buffer);
                bytes_received = recv(Serversocketfd, incoming_data_buffer,4096, 0);
                
                // If no data arrives, the program will just wait here until some data arrives.
                if (bytes_received == 0) std::cout << "host shut down." << std::endl ;
                if (bytes_received == -1) std::cout << "receive error!" << std::endl ;
                //std::cout << bytes_received << " bytes received :" << std::endl ;
                //std::cout << incoming_data_buffer << std::endl;

                int SendTimeElapsed = getMilliSpan( SendTimeStart );

                stringstream helpme;
                helpme << incoming_data_buffer;
                
                float prediction;
                int id2;
                stringstream temppred;
                //cout << helpme << endl;
                //helpme >> prediction;
                if (boost::regex_match((helpme.str()).c_str(), matches, e1)){
                    // Get id, pe
                    for (int i = 1; i < matches.size(); i++){
                        string match(matches[i].first, matches[i].second);
                        if (i == 2) id2 = atoi(match.c_str()); 
                        else if (i == 4) temppred.str(match);
                    }
                    temppred >> prediction;
                }
                else{
                    //cout << helpme << endl;
                    cout << "ERROR: TRAIN: received badly structured prediction!" << endl;
                    return -1;
                }

                if (WriteTrainResult)
                    if (getLabels)
                        Outfile << id2 << "\t" << prediction << "\t" << (SendTimeElapsed/1000.0);
                    else
                        Outfile << id2 << "\t" << prediction << "\t" << (SendTimeElapsed/1000.0) << endl;

                // Create output string and send <pe>
                std::stringstream sstm2;
                sstm2 << str1 << count << str5 << pe << str6 << endl;
                string two = sstm2.str();
                const char *msg2 = two.c_str();
                len = strlen(msg2);
                bytes_sent = send(Serversocketfd, msg2, len, 0);
                
                if (getLabels){
                    int SendTimeStart2 = getMilliCount();

                    // Client: Waiting to receive answer to continue...
                    memset(incoming_data_buffer, '\0', sizeof incoming_data_buffer);
                    bytes_received = recv(Serversocketfd, incoming_data_buffer,4096, 0);

                    // If no data arrives, the program will just wait here until some data arrives.
                    if (bytes_received == 0) std::cout << "host shut down." << std::endl ;
                    if (bytes_received == -1)std::cout << "receive error!" << std::endl ;

                    int SendTimeElapsed2 = getMilliSpan( SendTimeStart2 );

                    //int SendTimeStart = getMilliCount();
                    stringstream helpme2;
                    helpme2 << incoming_data_buffer;

                    float trueHTER;
                    //helpme2 >> trueHTER;
                    int id3;
                    stringstream temphter;

                    //helpme >> prediction;
                    if (boost::regex_match((helpme2.str()).c_str(), matches, e2)){
                        // Get id, pe
                        for (int i = 1; i < matches.size(); i++){
                            string match(matches[i].first, matches[i].second);
                            if (i == 2) id2 = atoi(match.c_str()); 
                            else if (i == 4) temphter.str(match);
                        }
                        temphter >> trueHTER;
                    }
                    else{
                        cout << "ERROR: TRAIN: received badly structured label!" << endl;
                        return -1;
                    }

                    if (WriteTrainResult)
                        Outfile << "\t" << trueHTER << "\t" << (SendTimeElapsed2/1000.0) << endl;
                }
            }
            
            else {
                count--;
                break;
            }
        }

        TotalProcessTime = getMilliSpan(OutTimeStart);
        Sourcefile.close();
        Targetfile.close();
        PostEditfile.close();
        if (UseFeats)
            Featfile.close();
        if (WriteTrainResult)
            Outfile.close();
        
        tmpsrc.clear();
        tmptrg.clear();
        tmppe.clear();
    }
    // Perform Testing in the appropriate fashion
    Sourcefile.open ((tmpsrc3.str()).c_str(), ios::in);
    Targetfile.open ((tmptrg3.str()).c_str(), ios::in);
    PostEditfile.open ((tmppe3.str()).c_str(), ios::in);
    if (UseFeats)
        Featfile.open ((tmpfeat3.str()).c_str(), ios::in);

    if (!(Sourcefile.is_open() & Targetfile.is_open() & PostEditfile.is_open())){
        std::cout << "Error: Could not open one of the test files. Client will exit." << std::endl;
        return 1;
    }

    if (!(Featfile.is_open()) && UseFeats){
        std::cout << "Error: Could not open the test features file. Client will exit." << std::endl;
        return 1;
    }


    if (WriteTrainResult)
        Outfile.open((outfile3.str()).c_str(), ios::out);

    if (WriteTrainResult && Outfile.is_open()) {
        // The header at the output file for test
        if (getLabels)
            Outfile << "Instance \t Prediction \t Time to predict \t HTER \t Time to retrain" << endl;
        else
            Outfile << "Instance \t Prediction \t Time to predict" << endl;
    }

    // Start timer
    OutTimeStart = getMilliCount();

    while (getline(Sourcefile, src)){
        // Continue count of instances
        count++;
        getline(Targetfile, trg);
        getline(PostEditfile, pe);
        if (UseFeats){
            getline(Featfile, feats);
            //feats.append("\n");
        }
        
        // Create output string and send <src><trg> pair
        std::stringstream sstm;
        if (UseFeats)
            sstm << str1 << count << str2 << src << str3 << trg << str7 << feats << str8 << endl;
        else
            sstm << str1 << count << str2 << src << str3 << trg << str4 << endl;

        string one = sstm.str();
        const char *msg1 = one.c_str();
        len = strlen(msg1);
        ssize_t bytes_sent = send(Serversocketfd, msg1, len, 0);
        int SendTimeStart = getMilliCount();

        // Send features
        /*if (UseFeats){
            const char *msg6 = feats.c_str();
            len = strlen(msg6);
            bytes_sent = send(Serversocketfd, msg6, len, 0);
        }*/
        // Receive prediction
        memset(incoming_data_buffer, '\0', sizeof incoming_data_buffer);
        bytes_received = recv(Serversocketfd, incoming_data_buffer,4096, 0);

        // If no data arrives, the program will just wait here until some data arrives.
        if (bytes_received == 0) std::cout << "host shut down." << std::endl ;
        if (bytes_received == -1) std::cout << "receive error!" << std::endl ;
        //std::cout << bytes_received << " bytes received :" << std::endl ;
        //std::cout << incoming_data_buffer << std::endl;

        int SendTimeElapsed = getMilliSpan( SendTimeStart );

        stringstream helpme;
        helpme << incoming_data_buffer;

        float prediction;
        //helpme >> prediction;
        int id2;
        stringstream temppred;

        //helpme >> prediction;
        if (boost::regex_match((helpme.str()).c_str(), matches, e1)){
            // Get id, pe
            for (int i = 1; i < matches.size(); i++){
                string match(matches[i].first, matches[i].second);
                if (i == 2) id2 = atoi(match.c_str()); 
                else if (i == 4) temppred.str(match);
            }
            temppred >> prediction;
        }
        else{
            cout << "ERROR: TEST: received badly structured prediction!" << endl;
            return -1;
        }



        if (WriteTrainResult)
            if (getLabels)
                Outfile << count << "\t" << prediction << "\t" << (SendTimeElapsed/1000.0);
            else
                Outfile << count << "\t" << prediction << "\t" << (SendTimeElapsed/1000.0) << endl;
        
        // Only send <pe> in adaptive mode - so that the server will also continue training
        if (DoAdaptive || DoEmpty){
            // Create output string and send <pe>
            std::stringstream sstm2;
            sstm2 << str1 << count << str5 << pe << str6 << endl;
            string two = sstm2.str();
            const char *msg2 = two.c_str();
            len = strlen(msg2);
            bytes_sent = send(Serversocketfd, msg2, len, 0);

            if (getLabels){
                int SendTimeStart2 = getMilliCount();

                // Client: Waiting to receive answer to continue...
                memset(incoming_data_buffer, '\0', sizeof incoming_data_buffer);
                bytes_received = recv(Serversocketfd, incoming_data_buffer,4096, 0);

                // If no data arrives, the program will just wait here until some data arrives.
                if (bytes_received == 0) std::cout << "host shut down." << std::endl ;
                if (bytes_received == -1)std::cout << "receive error!" << std::endl ;

                int SendTimeElapsed2 = getMilliSpan( SendTimeStart2 );

                //int SendTimeStart = getMilliCount();
                stringstream helpme2;
                helpme2 << incoming_data_buffer;

                float trueHTER;
                //helpme2 >> trueHTER;
                int id3;
                stringstream temphter;

                //helpme >> prediction;
                if (boost::regex_match((helpme2.str()).c_str(), matches, e2)){
                    // Get id, pe
                    for (int i = 1; i < matches.size(); i++){
                        string match(matches[i].first, matches[i].second);
                        if (i == 2) id2 = atoi(match.c_str()); 
                        else if (i == 4) temphter.str(match);
                    }
                    temphter >> trueHTER;
                }
                else{
                    //cout << incoming_data_buffer << endl;
                    cout << "ERROR: TEST: received badly structured label!" << endl;
                    return -1;
                }

                if (WriteTrainResult)
                    Outfile << "\t" << trueHTER << "\t" << (SendTimeElapsed2/1000.0) << endl;
            }
        }
        // in Static mode, just go on
        else
            if (WriteTrainResult)
                Outfile << endl;
        

    }

    TotalProcessTime = getMilliSpan(OutTimeStart);
    Sourcefile.close();
    Targetfile.close();
    PostEditfile.close();
    if (UseFeats)
        Featfile.close();
    if (WriteTrainResult)
        Outfile.close();

    tmpsrc.clear();
    tmptrg.clear();
    tmppe.clear();
    
    // Send end messages to the server so that it can close
    const char *msg4 = "END\n";
    int len2 = strlen(msg4);
    bytes_sent = send(Serversocketfd, msg4, len2, 0);
    const char *msg5 = "ENDEND\n";
    int len3 = strlen(msg5);
    bytes_sent = send(Serversocketfd, msg5, len3, 0);
    cout << "Run finished!" << endl;
    return 0;
}

int InitServerConnection(){
    int status;
    struct sockaddr_un {
        uint8_t sun_length; 
        short sun_family;// = AF_UNIX;
        char sun_path[100];//=null terminated pathname (100 is posix 1.g minimum)
    };
    
    std::cout << "Setting up the structs..."  << std::endl;
    struct addrinfo host_info;
    struct addrinfo *host_info_list;
    memset(&host_info, 0, sizeof host_info);
    host_info.ai_family = AF_UNSPEC;     // IP version not specified. Can be both.
    host_info.ai_socktype = SOCK_STREAM; 
    
    //Get addr info to fill the struct
    status = getaddrinfo("localhost", "9998", &host_info, &host_info_list);
    if (status != 0)  std::cout << "getaddrinfo error" << gai_strerror(status) ;
    
    //Create a socket
    std::cout << "Init: Creating a socket..."  << std::endl;
    int socketfd ; // The socket descriptor
    socketfd = socket(host_info_list->ai_family, host_info_list->ai_socktype, host_info_list->ai_protocol);
    if (socketfd == -1)  std::cout << "socket error " ;
    
    //Connect now with the server///
    std::cout << "Init: Connect()ing..."  << std::endl;
    status = connect(socketfd, host_info_list->ai_addr, host_info_list->ai_addrlen);
    if (status == -1)  std::cout << "connect error" ;
    
    
    return socketfd;
}

int getMilliCount(){
	timeb tb;
	ftime(&tb);
	int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
	return nCount;
}

int getMilliSpan(int nTimeStart){
	int nSpan = getMilliCount() - nTimeStart;
	if(nSpan < 0)
		nSpan += 0x100000 * 1000;
	return nSpan;
}
