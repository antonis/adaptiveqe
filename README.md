﻿## AQET ##

**AQET (Adaptive Quality Estimation Tool)** is an open-source package for performing Quality Estimation for Machine Translation, in an online-fashion, one input instance at a time. It was developed by Antonis Anastasopoulos, José C.G. de Souza, Marco Turchi, Matteo Negri and Nicola Bertoldi at Fondazione Bruno Kessler. This particular release was made possible through the MateCat project (http://www.matecat.com/).

AQET is open-source and it's distributed under the GPL v.3 license. For more information follow http://www.gnu.org/copyleft/gpl.html.


The currently available online machine learning algorithms are Online Support Vector Regression (Online SVR), Passive-Aggressive Algorithms and Sparse Online Gaussian Process.

Follow the steps given at the AQET Manual in order to use it. Only Linux systems are supported, and the software has ben tested in Ubuntu Linux 12.04 and Red Hat Enterprise Linux 6.

	Language: C++

	Dependencies:
		- QuEst (http://www.quest.dcs.shef.ac.uk/)
		- IRSTLM (http://sourceforge.net/projects/irstlm/)
		- boost library (http://www.boost.org/users/history/version_1_55_0.html)