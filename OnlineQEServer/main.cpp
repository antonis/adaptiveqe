/* 
 * File:   main.cpp
 * Author: Anastasopoulos Antonis
 *
 * Created on July 3, 2013, 12:33 PM
 */

#include <cstdlib>
#include <iostream>
#include <cstring>      // Needed for memset
#include <sys/socket.h> // Needed for the socket functions
#include <sys/types.h>
#include <netdb.h>      // Needed for the socket functions
#include <map>
#include <utility>
#include <sstream>      // std::istringstream
#include <boost/regex.hpp>
#include "TrainInstance.h"
#include "TERInterface.h"
#include "LearningInterface.h"
#include "crossvalidation.h"
//#include "helping.h"
#include "time.h" /* time_t, struct tm, time, localtime, strftime */


//#include "OnlineSVR.h"
//#include "OnlineSVRAdapter.h"
#include <math.h>
using namespace std;
using namespace onlinesvr; 

//Initializes the TER Adaptors (currently only one version)
TERInterface **initializeTERAdaptors();

//Initializes the Learning Libraries Adaptors (currently supports OnlineSVR, sofiaml PA and Sparse OnlineGP)
LearningInterface **initializeLearningAdaptors();

void AddTrainInstance2Vector(int currentID, onlinesvr::Matrix<double>* TrainingSetX, Vector<double>* TrainingSetY);

//Establishes the Connection with QUEST
int InitQUESTConnection(string QuestHost, string QuestPort);

// Establishes the Connection with Client
// Will receive input from there (may also receive features)
// Will send predictions
int InitClientConnectionBind(string ClientHost, string ClientPort);
int InitClientConnectionAccept(int socketfd );

// Initializes the parameters according to the PROPERTIES.CONFIG file
int Initialize(string pfile, string &QuestHost, string &QuestPort, int &UsePort, string &ClientHost, 
        string &ClientPort, int &DegubMode, string &LogFile, int &PreComputedFeatures, int &FeaturesNo, int &SendLabel,
        int &Algorithm, double &SVRC, double &SVREpsilon, int &KernelType, 
        double &KernelParam1, double &KernelParam2, float &SofiaC, float &SofiaEpsilon, 
        int &Capacity, double &s20, int &KernelType2,
        float &Lambda, EtaType &Eta_Type, float &Eta, int &DoCV, int &Folds, 
        int &Limit, string &ResultsCVFile, string &epslist, string &clist, string &kplist,
        int &StoreWeights, string &WeightsFile, int &StoreFEatures, string &FeatureFile, 
        int &LoadModelFromFile, string &LoadModelFile, int &SaveModelToFile, string &SaveModelFile);


//Sends a request to QUEST for the sentence pairs in msg (with id msgid)
//int socket must be the socket descriptor returned by InitQUESTConnection()
//char *getFeatureString(int socket, int msgid, char *msg);
void getFeatureString(int socket, int msgid, char *msg, char *FeatureString);


// Writes the string s to the Log File
void Write2Log(string LogFileName, string s);

//Parses the Features String (sent by QUEST) and initializes the Features' Map to zeroes
//void InitFeatureMap(string s);

// Receives input from Client about configuration
int ConfigureParameters(int socket, int &Algorithm, double &C, double &Epsilon, 
        int &KernelType, double &KernelParam1, double &KernelParam2, int &Shuffle);

//Manages the input string (given from std) and: 
//  --returns -1 if input was wrong
//  --returns 1 if input was <src><trg> (creates new TrainInstance)
//  --returns 2 if input was <pe> (adds <pe> to existing TrainInstance)
//  --returns 3 if source and/or target sentences are empty
//  --returns 4 if input was <pe> but there wasn't such a TrainInstance
//  --changes c_id to be the current segment id
int ManageInput(string s, int &c_id);

// The Features Map. Dict Form <ID = {1,2,...) : ID (as defined in QUEST - must be defined)>
//std::map<int, int> FeatureDict;
int NoOfFeatures; // should be equal to FeatureDict.size();
// The Features Values. Dict Form <ID = {1,2,...} : Value>
std::map<int, float> FeatureVals;
// The hash-table of the TrainInstances objects
std::map<int, TrainInstancePtr> SegDict;

// The boolean for printing debug messages
int DebugMode = 0;
string LogFileName;
    
struct addrinfo host_info;
struct addrinfo *host_info_list;

//Main function
int main(int argc, char** argv) {
 
    int LearningAlg = 0;
    double SVRC, SVREpsilon, KernelParam1, KernelParam2;
    int KernelType, Shuffle;
    float SofiaC, SofiaEpsilon, Lambda, Eta;
    EtaType Eta_Type;
    int LoadModelFromFile, SaveModelToFile;
    string LoadModelFile, SaveModelFile;
    int UsePort = 0 ;
    string QuestHost, QuestPort;
    string ClientHost, ClientPort; 
    int Clientsocketfd;
    ofstream LogFile, WeightsFile, FeatureFile;
    int PreComputedFeatures;
    //int FeaturesNo;
    int SendLabel;
    int QUESTsocketfd;
    int DoCV = 0;
    int Folds, Limit;
    int StoreWeights = 0;
    int StoreFeatures = 0;
    string ResultsCVFile, WeightsFileName, FeatureFileName;
    int Capacity, KernelType2;
    double s20;
    
    string Epslist, clist, kplist;
                    
    
    string pfile;
    if (argc < 2){
        cout << "Insufficient arguments, please specify path to Properties.config file" << endl;
        return -1;
    }
    else pfile = argv[1];
    cout << pfile << endl;
    
    // Initialize the parameters from the Properties.config file - must be defined.
    if (Initialize(pfile, QuestHost, QuestPort, UsePort, ClientHost, ClientPort, DebugMode, LogFileName, PreComputedFeatures, NoOfFeatures, SendLabel,
            LearningAlg, SVRC, SVREpsilon, KernelType, KernelParam1, KernelParam2, SofiaC, SofiaEpsilon, Capacity, s20, KernelType2,
            Lambda, Eta_Type, Eta, DoCV, Folds, Limit, ResultsCVFile, Epslist, clist, kplist,
            StoreWeights, WeightsFileName, StoreFeatures, FeatureFileName,
            LoadModelFromFile, LoadModelFile, SaveModelToFile, SaveModelFile)){
         
        // Open LogFile and Write Initial Parameters
        if (DebugMode){
            LogFile.open(LogFileName.c_str(), ios::out);
            if (LogFile.is_open()) {
                LogFile << "Initialization Parameters" << endl;

                if (UsePort) LogFile << "Will read from Port " << ClientPort << " in the host " << ClientHost << endl;
                else LogFile << "Reading from stdin-stdout" << endl;
                LogFile << "Algorithm: " << LearningAlg << endl;
                //LogFile << "EpsilonList: " << Epslist << endl; 
                // Write any parameters as you wish
                //LogFile << "SVRC: " << SVRC << endl;
                //LogFile << "SVREPsilon: " << SVREpsilon << endl;
                //LogFile << "LoadModelFromFile: " << LoadModelFromFile << endl;
                //LogFile << "SofiaC: " << SofiaC << endl;
                //LogFile << "Lambda: " << Lambda << endl;
                //LogFile << "Eta: " << Eta << endl;
                //LogFile << "SaveModelFile: " << SaveModelFile << endl;
                //LogFile << "WeightsFile: " << WeightsFileName << endl;
                LogFile << "######################" << endl << endl;
                
                LogFile.close();
            }
            else {
                cout << "ERROR: Couldn't open LogFile. Check the configuration file for correct filepath." << endl;
                cout << "abort..."<< endl;
                return -1;
            }
        }
        // Open weights file if they are to be stored
        char *WeightsFileNameChar;
        if (StoreWeights){
            WeightsFile.open(WeightsFileName.c_str(), ios::out);
            if (WeightsFile.is_open()) {
                WeightsFile << "Weights over Time" << endl;
                WeightsFile.close();
            }   
            else{
                cout << "ERROR: Couldn't open Weights file. Check the configuration file for correct filepath." << endl;
                cout << "abort..." << endl;
                return -1;
            }
            WeightsFileNameChar = new char[WeightsFileName.length() + 1];
            strcpy(WeightsFileNameChar, WeightsFileName.c_str());
        }
        // Open Feature file if they are to be stored
        if (StoreFeatures){
            FeatureFile.open(FeatureFileName.c_str(), ios::out);
            if (!FeatureFile.is_open()){
                cout << "ERROR: Couldn't open the file to store the features. Check the configuration file for correct filepath." << endl;
                cout << "abort..." << endl;
                delete []WeightsFileNameChar;
                return -1;
            }
        }
        
        if (PreComputedFeatures) {
            // Parse Features String and initialize the Features' Map
            // Re-define if you want to use more/other features
            if (DebugMode) {
                stringstream temp;
                //temp << "Feature Map size: " << FeatureDict.size();
                temp << "Number of Features: " << NoOfFeatures;
                Write2Log(LogFileName, temp.str());
            }
         
        }
        else{
            //Initialize Connection with QUEST
            QUESTsocketfd = InitQUESTConnection(QuestHost, QuestPort);
            if (QUESTsocketfd == -1){
                string QuEstError = "ERROR: Couldn't connect to QuEst. Please check if QuEst server is running and communication ports are correct.";
                //if (DebugMode) 
                Write2Log(LogFileName, QuEstError);
                cout << QuEstError << endl;
                cout << "abort..." << endl;
                delete []WeightsFileNameChar;
                return -1;
            }
        }
      
        // Creates a new Interface with the HTER Calculators (or any other metric) 
        TERInterface **TERCalc = initializeTERAdaptors();
        // Creates an interface with the learning algorithms
        LearningInterface **Learn = initializeLearningAdaptors();
        // Initialize all the parameters as defined by the properties file
        Learn[LearningAlg]->SetC(SVRC);
	Learn[LearningAlg]->SetEpsilon(SVREpsilon);
	Learn[LearningAlg]->SetKernelType(KernelType);
	Learn[LearningAlg]->SetKernelParam1(KernelParam1);
	Learn[LearningAlg]->SetKernelParam2(KernelParam2);
	Learn[LearningAlg]->SetC2(SofiaC);
	Learn[LearningAlg]->SetEpsilon2(SofiaEpsilon);
	Learn[LearningAlg]->SetLambda(Lambda);
	Learn[LearningAlg]->SetEtaType(Eta_Type);
	Learn[LearningAlg]->SetEta(Eta);
        Learn[LearningAlg]->SetCapacity(Capacity);
        Learn[LearningAlg]->SetS20(s20);
        Learn[LearningAlg]->SetKernelGP(KernelType2);
        Learn[LearningAlg]->InitMeanStd(NoOfFeatures);
        
        //Initialize the Log mode into the learning class
        Learn[LearningAlg]->InitLog(LogFileName, DebugMode);
        
        // Make sure you load a correct model otherwise it won't work
	if (LoadModelFromFile){
                if (DebugMode){
                        Write2Log(LogFileName, "Load Model");
                        stringstream loadmodel;
                        loadmodel << LoadModelFile;
                        Write2Log(LogFileName, LoadModelFile);
                }
                
                
            
                
                char *lm = strcpy((char*)malloc(LoadModelFile.length()+1), LoadModelFile.c_str());
                try{

                        Learn[LearningAlg]->LoadModelFromFile(lm);
                    
                }
                // This is bad design to catch exceptions, but how else?
                catch (...){
                    Write2Log(LogFileName, "ERROR: Couldn't load the learning model from the specified file. Check the configuration file.");
                    cout << "ERROR: Couldn't load the learning model from the specified file. Check the configuration file." << endl;
                    cout << "abort..." << endl;
                    return -1;
                }
                if (DebugMode) Write2Log(LogFileName, "Loaded the model from the file.");
        }
        
 
    int ServerSocketFd;
 
    
    if (UsePort){
        // Not sure if necessary
        //sleep(2);
        // Initialize connection with Client - will wait for connection
        ServerSocketFd = InitClientConnectionBind(ClientHost, ClientPort);
        if (ServerSocketFd == -1){
            //if (DebugMode) 
            Write2Log(LogFileName, "ERROR: Couldn't bind connection from client. Aborting...");
            cout << "ERROR: Couldn't bind connection from any client." << endl << "abort..." << endl;
            return -1;
        }
        
    }
        
    bool firstTime =  true;
    // Will iterate over sessions until break message "ENDEND" is received 
    while(true){

        if (UsePort){
            // Not sure if necessary
            //sleep(2);
            // Initialize connection with Client - will wait for connection
            Clientsocketfd = InitClientConnectionAccept(ServerSocketFd);
            if (Clientsocketfd == -1){
                //if (DebugMode) 
                Write2Log(LogFileName, "ERROR: Couldn't accept connection from client. Aborting...");
                cout << "ERROR: Couldn't accept connection from any client." << endl << "abort..." << endl;
                return -1;
            }
            
            if (DebugMode) Write2Log(LogFileName, "Client has connected");
            if (DebugMode){
                
                stringstream temp;
                temp << "Clientsocketfd: "<< Clientsocketfd;
                Write2Log(LogFileName, temp.str());

            }
            
            if (DebugMode) Write2Log(LogFileName, "Closing 0/1/2");
            close(0); /* close standard input  */
            close(1); /* close standard output */
            close(2); /* close standard error  */
          
            // Duplicate socket to stdin-out-err
            if( dup(Clientsocketfd) != 0 || dup(Clientsocketfd) != 1 || dup(Clientsocketfd) != 2 ) {
              Write2Log(LogFileName, "Error: error duplicating socket for stdin/stdout/stderr");
              exit(-1);
            }
            else Write2Log(LogFileName, "Socket has been duplicated for stdin/stdout/stderr");
        }
        if (DebugMode) Write2Log(LogFileName, "Done with Initializations...");

  
        
        //Write2Log(LogFileName, "Death here?");
        
        // Check if break message is received
       if (UsePort) 
            if (!ConfigureParameters(Clientsocketfd, LearningAlg, SVRC, SVREpsilon, KernelType, KernelParam1, KernelParam2, Shuffle))
                break;
        
        //The matrices to store the training data for this session.
        onlinesvr::Matrix<double>* FeaturesSet = new onlinesvr::Matrix<double>;
        Vector<double>* Labels = new Vector<double>();
        int currentID;
        
        
        // Receives input infinitely, for future (service) purposes. 
        while(true){
            
            //Write2Log(LogFileName, "Death here 2?");
            std::string input;

            if (DebugMode) Write2Log(LogFileName, "Server: Waiting to receive data...");
            // Receive input
            if (UsePort) {
                
                if(cin.eof()){
                     Write2Log(LogFileName, "Get EOF cin");
                     cin.clear();
                     close(Clientsocketfd);
                     break;
                }
                
                getline(cin, input);
            }
            else getline(std::cin, input);
           
	   
 
            if (DebugMode){
                stringstream temp;
                //temp << "Input: " << input<< " "<< (input.size());
                temp << "Input: " << input;
                Write2Log(LogFileName, temp.str());
            }
            
            // Check if input is break message for the current session
            if (!input.compare("END")) {
                Write2Log(LogFileName, "Received END message so session will end.");
                break;
            }
            currentID = 0;
            
            
            
            // Parse the Input and return its type
            int inpType = ManageInput(input, currentID);

            if (DebugMode){
                stringstream temp;
                //temp << "Input: " << input<< " "<< (input.size());
                temp << "Current ID " << currentID;
                Write2Log(LogFileName, temp.str());
            }
            
            
            if (inpType == 1){
                // Input is <src><trg> pair or <src><trg><feat> tuple

                if (PreComputedFeatures){
                    (*SegDict[currentID]).setSeen(true);
                    if (DebugMode) Write2Log(LogFileName, "Client provided the features");
                    string FeatureString = (*SegDict[currentID]).getFeaturesstr();
                    if (StoreFeatures){
                        FeatureFile << FeatureString << endl;
                        FeatureFile.flush();
                    }
                    if (DebugMode){
                        stringstream temp;
                        temp << "FeatureString: " << FeatureString;
                        Write2Log(LogFileName, temp.str());
                    }
                }
                else{
                    //if (DebugMode) Write2Log(LogFileName, "Asking for the features...");
                    if ( !(*SegDict[currentID]).getSeen()){
                        (*SegDict[currentID]).setSeen(true);
                        input.append("\n");
                        //char *input2 = (char *) (input).c_str();
                        
                        stringstream sendQuEst;
                        sendQuEst << "<seg id=\"" << currentID << "\"><src>" << (*SegDict[currentID]).getSource() << "</src><trg>" << (*SegDict[currentID]).getTarget() << "</trg></seg>\n";
                        //char *input2 = (char *) (sendQuEst.str().c_str());
                        
                        char input2[10000] = {};
                        strcpy(input2, (char *) (sendQuEst.str()).c_str());
                        
                        
                        
                        
                        
                        char FeatureString[10000] = {};
                        

                        // send Request to QUEST server and receive FeatureString
                         
                        try{
                                getFeatureString(QUESTsocketfd, currentID, input2, FeatureString);
                         // This is bad design to catch exceptions, but how else?
                        }catch (...){
                            stringstream temp;
                            temp << "Error: Problem occurs when querying QuEst for the features";
                            Write2Log(LogFileName, temp.str());
                        }
                        
                       //sendQuEst.clear();
                        // Store the features in the feature file
                        if (StoreFeatures){
                            FeatureFile << FeatureString; // << endl;
                            FeatureFile.flush();
                        }
                        if (DebugMode){
                            stringstream temp;
                            temp << "FeatureString: " << FeatureString;
                            Write2Log(LogFileName, temp.str());
                        }
                        //std::string StrF = std::string(FeatureString);
                        // Parse the FeatureString and store features in the TrainInstance
                        try{
                                (*SegDict[currentID]).setFeatures(NoOfFeatures, FeatureString);
                         }catch (...){
                            stringstream temp;
                            temp << "Error: Problem occurs when storing the features in the TrainInstance structure";
                            Write2Log(LogFileName, temp.str());
                        }
                    }else{
                        if (DebugMode){
                            stringstream temp;
                            temp << "The Segment ID "<<currentID <<" is already in memory, features will be not recomputed";
                            Write2Log(LogFileName, temp.str());
                        }
                    }
                }
                
               //check if features are different from empty string
               if((*SegDict[currentID]).getFeatures().empty() == true ){
                   //Feature map is empty, so we assign the default value of 0.15
                   float def = 0.15;
                   (*SegDict[currentID]).setHTER_Prediction(def);
                   if (DebugMode){
                        stringstream temp;
                        temp << "WARNING: Feature vector is empty, so we emit default prediction equal to 0.15";
                        Write2Log(LogFileName, temp.str());
                   }
               }
               else{
                    // Get Prediction from QE Model and store it
                    try{
                        (*SegDict[currentID]).setHTER_Prediction(Learn[LearningAlg]->Predict(FeaturesSet, (*SegDict[currentID]).getFeatures()));
                     }catch (...){
                            stringstream temp;
                            temp << "Error: Problem occurs when producing the prediction";
                            Write2Log(LogFileName, temp.str());
                        }
                    
                    if (DebugMode){
                        stringstream temp;
                        temp << "Predicted a value: " << (*SegDict[currentID]).getHTER_Prediction();
                        Write2Log(LogFileName, temp.str());
                    }
               }
                // Send th prediction to the Client or the output
               if (UsePort){
                    cout << "<seg id=" << currentID << "><pred>" <<(*SegDict[currentID]).getHTER_Prediction() << "</pred></seg>" << endl;
                }
                else{
		    cout << "<seg id=" << currentID << "><pred>" <<(*SegDict[currentID]).getHTER_Prediction() << "</pred></seg>" << endl;
                }
                 
                
                
               // For Matecat: NewPred =  (1 -pred)*100;
            /*   float newP = (((float) 1 ) - (*SegDict[currentID]).getHTER_Prediction()) *((float) 100);
               
                if (UsePort){
                   cout << "<seg id=" << currentID << "><pred>" << newP << "</pred></seg>" << endl;        
                }
                else{
                   cout << "<seg id=" << currentID << "><pred>" << newP << "</pred></seg>" << endl;
                }
              */  

            }
            else if (inpType == 2){
               
                
                // Input is <pe> sentence
               /* if (DebugMode) {
                    stringstream temp;
                    //temp<< currentID <<"\n";
                    //temp<< (*SegDict[currentID]).getTarget() << "\n";
                    temp << "Input was <pe> so now Training will commence.";
                    Write2Log(LogFileName, temp.str());
                }*/
                // Set HTER to the score returned by the HTER adapter
                try{
                        (*SegDict[currentID]).setHTER(TERCalc[0]->execute((*SegDict[currentID]).getPostEdited(), (*SegDict[currentID]).getTarget()));
                 }catch (...){
                    stringstream temp;
                    temp << "Error: Problem occurs when getting the HTER";
                    Write2Log(LogFileName, temp.str());
                }
	//	if (DebugMode) {
          //          stringstream temp;
                    //temp<< currentID <<"\n";
                                        //temp<< (*SegDict[currentID]).getTarget() << "\n";
            //        temp << TERCalc[0]->execute((*SegDict[currentID]).getPostEdited(), (*SegDict[currentID]).getTarget());
            //        Write2Log(LogFileName, temp.str());
            //     }
                    

 
                // Add the training instance to the data matrices
                AddTrainInstance2Vector(currentID, FeaturesSet, Labels);
                
                //if (DebugMode) Write2Log(LogFileName, "Will now train.");
                try{
                    Learn[LearningAlg]->Train(FeaturesSet, Labels, currentID, LoadModelFromFile);
                 }catch (...){
                        stringstream temp;
                        temp << "Error: Problem occurs when training the QE model";
                        Write2Log(LogFileName, temp.str());
                 }
                
                // If the limit is reached for Cross-Validation, perform cross-validation (if needed)
                 if (DoCV & (currentID == Limit)){
               //  if (DoCV & (currentID == (Limit+1))){
                    if (DebugMode) Write2Log(LogFileName, "Now will do X-fold Cross Validation.");
                    // Parameters' lists need to be defined here
                    Vector<double>* EpsilonList = new Vector<double>();
                    Vector<double>* CList = new Vector<double>();
                    Vector<double>* KernelParamList = new Vector<double>();
                    // Add the wanted values to the lists
                    if (LearningAlg == 0){
                        double tempval1 = 0.0;
                        stringstream e(Epslist);
                        while (e >> tempval1){
                            EpsilonList->Add(tempval1);
                            
                            //if (DebugMode){ stringstream temp; temp  << "FirtEpsilon READ FROM PROPERTY FILE: "<< EpsilonList->GetValue(0); Write2Log(LogFileName,temp.str()); }
                             //<< temp.precision(20)
                        }
                        double tempval2 = 0.0;
                        stringstream c(clist);
                        while (c >> tempval2)
                            CList->Add(tempval2);
                        double tempval3 = 0.0;
                        stringstream kp(kplist);
                        while (kp >> tempval3)
                            KernelParamList->Add(tempval3);
                        
                      }
                    else if (LearningAlg == 1){
                        // Initialize epsilon list
                        double tempval1 = 0.0;
                        stringstream e(Epslist);
                        while (e >> tempval1)
                            EpsilonList->Add(tempval1);
                        // Initialize Clist
                        double tempval2 = 0.0;
                        stringstream c(clist);
                        while (c >> tempval2)
                            CList->Add(tempval2);
                        // and for the kernel parameter. At least one value is needed otherwise it will not be defined
                        // it will be ignored for PA (as it doesn't use a kernel)
                        KernelParamList->Add(0.0);
 

                       
                    }
                    else if (LearningAlg == 2){
                        // For OGP, EpsilonList is for the noise variace s20
                        // and Clist is for the Capacity
                        // We use the same functions - names for compactness
         
                        // Initialize epsilon list
                        double tempval1 = 0.0;
                        stringstream e(Epslist);
                        while (e >> tempval1)
                            EpsilonList->Add(tempval1);
                        // Initialize Clist
                        double tempval2 = 0.0;
                        stringstream c(clist);
                        while (c >> tempval2)
                            CList->Add(tempval2);
                        // and for the kernel parameter. At least one value is needed otherwise it will not be defined
                        // it will be ignored for OGP (as it doesn't use a kernel)
                        KernelParamList->Add(0.0);

                    }
                    
                    // Number of Folds
                    int SetNumber = Folds;
                    
                    // The file to put the results
                   // stringstream tmp;
                   // tmp << ResultsCVFile;
                   // char *ResultsFileName = const_cast<char*>((tmp.str()).c_str());
                    
                    char *ResultsFileName = strcpy((char*)malloc(ResultsCVFile.length()+1), ResultsCVFile.c_str());
 
                    // Perform CV
                    if (DebugMode) Write2Log(LogFileName, "Cross-Validation is starting.");
                    Vector<double>* bestParam = CrossValidation(FeaturesSet, Labels, EpsilonList, CList, KernelParamList, KernelType, KernelType2, SetNumber, ResultsFileName, LearningAlg, LoadModelFromFile);
                    if (DebugMode) Write2Log(LogFileName, "Cross-Validation is finished successfully.");
                    
                     // Save the finished model, is needed
                   if (SaveModelToFile){
                        stringstream temp;
                        temp << "File to save in: " << SaveModelFile<<".before";
                        
                        string SaveModelFileBefore = SaveModelFile;
                        
                        SaveModelFileBefore.append(".before");
                        
                        if (DebugMode) Write2Log(LogFileName, temp.str());
                        char *cstr = new char[SaveModelFileBefore.length() + 1];
                        strcpy(cstr, SaveModelFileBefore.c_str());
                        // Save Model to the file
                        Learn[LearningAlg]->SaveModelToFile(cstr);
                        if (DebugMode) Write2Log(LogFileName, "The model before retraining has been saved.");
                    }
                    
                    
                    //ReTrain AQET using the new parameters
                    
                    //*
                    if (DebugMode){ stringstream temp; temp << "Starting Applying the new Parameters"; Write2Log(LogFileName,temp.str()); }
                    
                   
                    //Clean Mean and Std
                    Learn[LearningAlg]->ClearMeanStd();
                    if (DebugMode){ stringstream temp; temp << "Cleaned the mean"; Write2Log(LogFileName,temp.str()); }
                    
                    //Delete the trained learners
                    destroyLearningAdaptors(Learn);
                    if (DebugMode){ stringstream temp; temp << "Destroyed the Old Learner"; Write2Log(LogFileName,temp.str()); }
                    
                    //cleanLearningAdaptors(Learn);
                    
                    //Initialize three empty learners
                    Learn = initializeLearningAdaptors();
                    if (DebugMode){ stringstream temp; temp << "Initialize three empty learners"; Write2Log(LogFileName,temp.str()); }
                    
                    // Initialize the parameters
                    if (LearningAlg == 0){
                        Learn[LearningAlg]->SetC(bestParam->GetValue(1));
                        Learn[LearningAlg]->SetEpsilon(bestParam->GetValue(0));
                        Learn[LearningAlg]->SetKernelType(KernelType);
                        Learn[LearningAlg]->SetKernelParam1(bestParam->GetValue(2));
                        Learn[LearningAlg]->SetKernelParam2(KernelParam2);
                        if (DebugMode){ stringstream temp; temp << "Set the new Parameters for OSVR: C: "<< bestParam->GetValue(1)<< " Epsilon: " << bestParam->GetValue(0) << " KernelPar: "<< bestParam->GetValue(2); Write2Log(LogFileName,temp.str()); }
                    }
                    else if (LearningAlg == 1){
                        Learn[LearningAlg]->SetC2((float) bestParam->GetValue(1));
                        Learn[LearningAlg]->SetEpsilon2((float) bestParam->GetValue(0));
                     	Learn[LearningAlg]->SetLambda(Lambda);
                        Learn[LearningAlg]->SetEtaType(Eta_Type);
                        Learn[LearningAlg]->SetEta(Eta);
                        if (DebugMode){ stringstream temp; temp << "Set the new Parameters for PA: C: "<< bestParam->GetValue(1)<< " Epsilon: " << bestParam->GetValue(0) << " Eta: "<< Eta; Write2Log(LogFileName,temp.str()); }

                    }
                    else if (LearningAlg == 2){
                        Learn[LearningAlg]->SetCapacity((int) bestParam->GetValue(1));
                        Learn[LearningAlg]->SetS20( bestParam->GetValue(0));
                        Learn[LearningAlg]->SetKernelGP(KernelType2);
                        if (DebugMode){ stringstream temp; temp << "Set the new Parameters for OGP: capacity: "<< bestParam->GetValue(1)<< " S20: " << bestParam->GetValue(0); Write2Log(LogFileName,temp.str()); }

                    }
            
                    Learn[LearningAlg]->InitMeanStd(NoOfFeatures);
                     if (DebugMode){ stringstream temp; temp << "Initialized Mean and std"; Write2Log(LogFileName,temp.str()); }
                    
                    //Initialize the Log mode into the learning class
                    Learn[LearningAlg]->InitLog(LogFileName, DebugMode);
                     if (DebugMode){ stringstream temp; temp << "Initialized the Log"; Write2Log(LogFileName,temp.str()); }

                   //Train the model on each point
                   for (int i = 0; i< FeaturesSet->GetLengthRows(); i++){
                        onlinesvr::Matrix<double>* appFeatures = FeaturesSet->ExtractRows(0,i);
                        Vector<double>* appLabels = Labels->Extract(0,i);
                        
                        if (DebugMode){ stringstream temp; temp << "Size Matrix: "<< appFeatures->GetLengthRows();; Write2Log(LogFileName,temp.str()); }
                      
                        //onlinesvr::Matrix<double>* appFeatures = new onlinesvr::Matrix<double>;
                        //appFeatures->AddRowRef(FeaturesSet->GetRowCopy(i));
                        //Vector<double>* appLabels = new Vector<double>();
                        //appLabels->Add(Labels->GetValue(i));
                        
                         
                        //FeaturesSet->AddRowCopy((SetX->GetValue(j))->GetRowRef(k));
                       // Labels->Add((SetY->GetValue(j))->GetValue(k));
                        Learn[LearningAlg]->Train(appFeatures, appLabels, i, LoadModelFromFile);
                        delete appFeatures;
                        delete appLabels;
                         if (DebugMode){ stringstream temp; temp << "Added point n.: "<<i; Write2Log(LogFileName,temp.str()); }
                   }
                    if (DebugMode){ stringstream temp; temp << "Ended Retraining"; Write2Log(LogFileName,temp.str()); }
                   
                     if (SaveModelToFile){
                        stringstream temp;
                        temp << "File to save in: " << SaveModelFile;
                        if (DebugMode) Write2Log(LogFileName, temp.str());
                        char *cstr = new char[SaveModelFile.length() + 1];
                        strcpy(cstr, SaveModelFile.c_str());
                        // Save Model to the file
                        Learn[LearningAlg]->SaveModelToFile(cstr);
                        if (DebugMode) Write2Log(LogFileName, "The model has been saved.");
                    }
                    
                    
                    //*/
                }else{
                    
                    
                    if (DebugMode) Write2Log(LogFileName, "Training is finished successfully.");
                    if (StoreWeights){
                        Learn[LearningAlg]->PrintWeights(WeightsFileNameChar) ;
                        if (DebugMode) Write2Log(LogFileName, "Stored the weights successfully.");
                    }
                    
                }
                /*if (DebugMode) {
                          stringstream temp;
                                   //temp<< currentID <<"\n";
                                                                            //temp<< (*SegDict[currentID]).getTarget() << "\n";
                           temp << "<seg id="<< currentID << "><label>" << (*SegDict[currentID]).getHTER() << "</label></seg>" << endl;
                          Write2Log(LogFileName, temp.str());
                }*/

                // Send the HTER score to the client or the stdout 
                //(client waits for this in order to confirm that training is finished)
                if (SendLabel)
                    if (UsePort){               
                        cout << "<seg id="<< currentID << "><label>" << (*SegDict[currentID]).getHTER() << "</label></seg>" << endl;
                    }
                    else {
                        cout << "<seg id="<< currentID << "><label>" << (*SegDict[currentID]).getHTER() << "</label></seg>" << endl;
                    }
                
                if (DebugMode) {
                          stringstream temp;
                          temp << "Queue Size Before: "<< SegDict.size() << endl;
                          Write2Log(LogFileName, temp.str());
                }
                
                //Remove already performed source-target-pe from the queue if not CV
                if (!DoCV){ 
                        SegDict.erase(currentID);
                }
                if (DebugMode) {
                          stringstream temp;
                          temp << "Queue Size After: "<< SegDict.size() << endl;
                          Write2Log(LogFileName, temp.str());
                }
                    
            }else if (inpType == 3){
               // Input is empty or Source or Target is empty so emits 1, do not compute the features, and do not save the info for this point in memory     
                    if (DebugMode){
                        stringstream temp;
                        temp << "Predicted a value: 1";
                        Write2Log(LogFileName, temp.str());
                    }
                    // Send th prediction to the Client or the output
                    if (UsePort){
                        cout << "<seg id=" << currentID << "><pred>1</pred></seg>" << endl;
                    }
                    else{
                        cout << "<seg id=" << currentID << "><pred>1</pred></seg>" << endl;
                    }
 
                     // Send th prediction to the Client or the output
                    
                    // For MATECAT

                /*    if (UsePort){
                         //char buffer[32];
                        // if (recv(Clientsocketfd, buffer, sizeof(buffer), MSG_PEEK | MSG_DONTWAIT) != 0){
                        try{
                              cout << "<seg id=" << currentID << "><pred>0</pred></seg>" << endl;

                       }catch (...){
                            stringstream temp;
                            temp << "Error: Problem occurs due to client deathl";
                            Write2Log(LogFileName, temp.str());
                        }
                         //}
                    } else cout << "<seg id=" << currentID << "><pred>0</pred></seg>" << endl;
                        */


                  
                   
                    
                    if (DebugMode){
                        stringstream temp;
                        temp << "Socket alive?";
                        Write2Log(LogFileName, temp.str());
                    }
                    
                    
             }else if (inpType == 4){
               // The pe has not a relative pair(source, target) so nothing is done and a default HTER value (1) is sent to the client
                    //if (DebugMode){
                        stringstream temp;
                        temp << "Error: The segment number: " << currentID <<" is not in the queue. Emitted a default HTER value = 1.";
                        Write2Log(LogFileName, temp.str());
                   // }
                    // Send the HTER score to the client or the stdout 
                    //(client waits for this in order to confirm that training is finished)

                    if (SendLabel)
                        if (UsePort){
                            cout << "<seg id="<< currentID << "><label>1</label></seg>" << endl;
                        }
                        else{
                            cout << "<seg id="<< currentID << "><label>1</label></seg>" << endl;
                        }
                    Write2Log(LogFileName, "Death here 3 ?");
                    
             } else{
                // Input was not well structured
                // OR the segment id was not found
                if (UsePort){
                        cout << -1 << endl;
                }else{
                        cout << -1 << endl;  
                }  
                //if (DebugMode) Write2Log(LogFileName, "Error: The input was not structured correctly and will be ignored.");
                //if (DebugMode) {
                    stringstream temp;
                    temp << "Error: Problems occur analyzing the point number: " << currentID;
                    Write2Log(LogFileName, temp.str());
                //}
            }
            
            if (DebugMode) {
                stringstream temp;
                temp << "Queue Size: "<< SegDict.size() << endl;
                Write2Log(LogFileName, temp.str());
            }
        }
    
       // Save the finished model, is needed
    /*    if (SaveModelToFile){
            stringstream temp;
            temp << "File to save in: " << SaveModelFile;
            if (DebugMode) Write2Log(LogFileName, temp.str());
            char *cstr = new char[SaveModelFile.length() + 1];
            strcpy(cstr, SaveModelFile.c_str());
            // Save Model to the file
            Learn[LearningAlg]->SaveModelToFile(cstr);
            if (DebugMode) Write2Log(LogFileName, "The model has been saved.");
        }*/
    }
    
    if (StoreFeatures)
        FeatureFile.close();
    if (StoreWeights)
        WeightsFile.close();
    }
    else {
        //if (DebugMode) 
        Write2Log(LogFileName, "Didn't manage to initialize from the properties file...");
        cout << "ERROR: Didn't manage to initialize from the properties file..." << endl;
        cout << "abort..." << endl;
        return -1;
    }
    return 0;
}


void getFeatureString(int socket, int msgid, char *msg, char *received){

   /* if (DebugMode){
        Write2Log(LogFileName, "Sent: ");
        stringstream temp;
        temp << msg;
        Write2Log(LogFileName, temp.str() );
    }*/
    
    int len = strlen(msg);
    ssize_t bytes_sent = send(socket, msg, len, 0);
    /*if (DebugMode){
        Write2Log(LogFileName, "Sent message to QuEst...");
        stringstream temp;
        temp << bytes_sent;
        Write2Log(LogFileName, temp.str() );
       
    }*/
    //Wait for answer from QUEST
    ssize_t bytes_received = recv(socket, received, 1000, 0);
    
    /*if (DebugMode){
        Write2Log(LogFileName, "Received the features...");
        stringstream temp;
        temp << bytes_received;
        Write2Log(LogFileName, temp.str() );
    }*/
        // If no data arrives, the program will just wait here until some data arrives.
    if (bytes_received == 0)  if (DebugMode) Write2Log(LogFileName, "host shut down.");
    if (bytes_received == -1) if (DebugMode) Write2Log(LogFileName, "receive error!");
    //answer = received;
    //return answer;
}



int InitQUESTConnection(string QuestHost, string QuestPort){
    int status;
    struct sockaddr_un {
        uint8_t sun_length; 
        short sun_family;// = AF_UNIX;
        char sun_path[100];//=null terminated pathname (100 is posix 1.g minimum)
    };
    
    if (DebugMode) Write2Log(LogFileName, "QuEST Init: Setting up the structs...");
    struct addrinfo host_info;
    struct addrinfo *host_info_list;
    memset(&host_info, 0, sizeof host_info);
    host_info.ai_family = AF_UNSPEC;     // IP version not specified. Can be both.
    host_info.ai_socktype = SOCK_STREAM; 
    
    //Get addr info to fill the struct
    status = getaddrinfo(QuestHost.c_str(), QuestPort.c_str(), &host_info, &host_info_list);
    //status = getaddrinfo("localhost", "9999", &host_info, &host_info_list);
    if (status != 0)  { 
       // if (DebugMode) {
                stringstream temp;
                temp << "ERROR: QuEST Init: getaddrinfo error" << gai_strerror(status);
                Write2Log(LogFileName, temp.str());
       // }
        return -1;
    }
    
    //Create a socket
    if (DebugMode) Write2Log(LogFileName, "QuEST Init: Creating a socket...");
    int socketfd ; // The socket descriptor
    socketfd = socket(host_info_list->ai_family, host_info_list->ai_socktype, host_info_list->ai_protocol);
    if (socketfd == -1)  {
        //if (DebugMode) 
        Write2Log(LogFileName, "ERROR: QuEST Init: socket error ");
        return -1;
    }
    
    //Connect now with the QuEst server
    if (DebugMode) Write2Log(LogFileName, "QuEST Init: Connect()ing...");
    status = connect(socketfd, host_info_list->ai_addr, host_info_list->ai_addrlen);
    if (status == -1)  {
        //if (DebugMode) 
        Write2Log(LogFileName, "ERROR: QuEST Init: connect error");
        return -1;
    }
    
    //Send TEST message
    if (DebugMode) Write2Log(LogFileName, "QuEST Init: send()ing TEST message...");
    
 
      
    //Wait for QUEST Init - Needed in the previous version probably not now
    //sleep(10);
    return socketfd;
}



int ManageInput(string s, int &c){
    
    string ErrorMsg1 = "Error: The Input is not structured correctly.";
    

    
    typedef basic_istringstream<char> istringstream;
    float temp;
    std::istringstream iss(s);
    //std::cout << "InitFM: initial string is: " << s << std::endl; 
    std::string sre1 = "(<seg id=)(.*)(><src>)(.*)(</src><trg>)(.*)(</trg></seg>)";
    std::string sre3 = "(<seg id=)(.*)(><src>)(.*)(</src><trg>)(.*)(</trg><feat>)(.*)(</feat></seg>)";
    std::string sre2 = "(<seg id=)(.*)(><pe>)(.*)(</pe></seg>)";
    boost::regex e1, e2, e3;
    boost::cmatch matches;
    
    try{
        e1 = sre1;
    }
    catch (boost::regex_error& e){
        //if (DebugMode) 
        Write2Log(LogFileName, "Error: Not well-structured regular expression for <src><trg>");
    }
    try{
        e2 = sre2;
    }
    catch (boost::regex_error& e){
        //if (DebugMode) 
        Write2Log(LogFileName, "Error: Not well-structured regular expression for <pe>");
    }
    try{
        e3 = sre3;
    }
    catch (boost::regex_error& e){
        //if (DebugMode) 
        Write2Log(LogFileName, "Error: Not well-structured regular expression for <src><trg><feat>");
    }
    
    // If it matches regex e1 (<src><trg>)
    if(s.empty()){
	//empty input
	return 3;
    }
    else if (boost::regex_match(s.c_str(), matches, e1)){
        int id;
        string source, target;
        // Get id, source, target
        for (int i = 1; i < matches.size(); i++){
            string match(matches[i].first, matches[i].second);
            if (i == 2) {
                replaceAll( match, "\"", "");
                replaceAll( match, "\'", "");
                id = atoi(match.c_str());
            }else if (i == 4) source = match;
            else if (i == 6) target = match;
        }
        
        if( source.empty() || target.empty()){
            //if (DebugMode) 
            Write2Log(LogFileName, "Warning: Source or Target Empty");
            return 3;
        }
        
        // If not exists in instances dictionary
        if (SegDict.find(id) == SegDict.end()){
            // Create new instance
            TrainInstance *t = new TrainInstance(id,source,target);
            SegDict.insert(make_pair(id, t));
            (*SegDict[id]).setSeen(false);
        }
        c = id;
        
        if (DebugMode){
                stringstream temp;
                //temp << "Input: " << input<< " "<< (input.size());
                temp << "Current ID in ManageInput " << c << " extracted ID "<< id;
                Write2Log(LogFileName, temp.str());
        }
        
        return 1;
    }
    // If it matches regex e1 (<src><trg><feat>)
    else if (boost::regex_match(s.c_str(), matches, e3)){
        int id;
        string source, target, featstr;
        // Get id, source, target
        for (int i = 1; i < matches.size(); i++){
            string match(matches[i].first, matches[i].second);
            if (i == 2) {
                replaceAll( match, "\"", "");
                replaceAll( match, "\'", "");
                id = atoi(match.c_str());
                
            }
            else if (i == 4) source = match;
            else if (i == 6) target = match;
            else if (i == 8) featstr = match;
        }
        
      
   
        
        // If not exists in instances dictionary
        if (SegDict.find(id) == SegDict.end()){
            // Create new instance
            TrainInstance *t = new TrainInstance(id,source,target);
            SegDict.insert(make_pair(id, t));
            (*SegDict[id]).setSeen(false);
            (*SegDict[id]).setFeatures(NoOfFeatures, featstr);
        }
        c = id;
        return 1;
    }
    
    // if it matches regex e2 (<pe>)
    else if (boost::regex_match(s.c_str(), matches, e2)){
       

        int id;
        string pe;
        // Get id, pe
        for (int i = 1; i < matches.size(); i++){
            string match(matches[i].first, matches[i].second);
            if (i == 2){
                replaceAll( match, "\"", "");
                replaceAll( match, "\'", "");
                id = atoi(match.c_str()); 
            }
            else if (i == 4) pe = match;
        }
        // If segment not found, return error
        if ( SegDict.find(id) == SegDict.end() ) {
            //For some reasons, there is not this segment in the queue, nothing is done
            //if (DebugMode)Write2Log(LogFileName, "Error: This Segment is not in the queue.");
           // return -1;
	   c= id;
           return 4;
        }
        // else insert postedited to the instance
        else {     
            //if (DebugMode) Write2Log(LogFileName, "PE "+pe);

            (*SegDict[id]).setPostEdited(pe);
            (*SegDict[id]).setSeen(true);
            c =id;
            return 2;
        }                                 
    }
    // else, the input is not structured correctly and will be ignored
    else {
        //if (DebugMode) 
        Write2Log(LogFileName, ErrorMsg1);
        return -1;
    }
    
    
  
    
    return 0;

}




int InitClientConnectionBind( string ClientHost, string ClientPort){
    int status;
    struct sockaddr_un {
        uint8_t sun_length; 
        short sun_family;// = AF_UNIX;
        char sun_path[100];//=null terminated pathname (100 is posix 1.g minimum)
    };
    
    if (DebugMode) Write2Log(LogFileName, "Server Init: Setting up the structs...");

    
    memset(&host_info, 0, sizeof host_info);
    host_info.ai_family = AF_UNSPEC;     // IP version not specified. Can be both.
    host_info.ai_socktype = SOCK_STREAM; 
    
    //Get addr info to fill the struct
    status = getaddrinfo(ClientHost.c_str(), ClientPort.c_str(), &host_info, &host_info_list);
    //status = getaddrinfo("localhost", "9998", &host_info, &host_info_list);
    stringstream temp;
    temp << "Server Init: getaddrinfo error" << gai_strerror(status);
    if (status != 0)  {
        if (DebugMode) Write2Log(LogFileName, temp.str()); 
        cout << "ERROR: " << temp.str() << endl;
        return -1;
    }
    
    //Create a socket
    if (DebugMode) Write2Log(LogFileName, "Server Init: Creating a socket...");
    int socketfd ; // The socket descriptor
    socketfd = socket(host_info_list->ai_family, host_info_list->ai_socktype, host_info_list->ai_protocol);
    if (socketfd == -1) {
        //if (DebugMode) 
        Write2Log(LogFileName, "Server Init: socket error, couldn't get socket info.");
        cout << "ERROR: Server Init: Wrong client socket info" << endl;
        return -1;
    }
    
    //Connect now with the client
    if (DebugMode) Write2Log(LogFileName, "Server Init: Bind()ing...");
    int yes = 1;
    status = setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
    status = bind(socketfd, host_info_list->ai_addr, host_info_list->ai_addrlen);
    if (status == -1) {
        //if (DebugMode)  
        Write2Log(LogFileName, "Server Init: connect error: couldn't bind to the port.");
        cout << "ERROR: Server Init couldn't bind to socket." << endl;
        return -1;
    }
    return socketfd;
  
}




int InitClientConnectionAccept( int socketfd ){
    int status;
  
    if (DebugMode){
        Write2Log(LogFileName, "Server Init: Listen()ing for connections..." );
        stringstream temp;
        temp << "SocketFd: "<< socketfd;
        Write2Log(LogFileName, temp.str());
    
    }// Listen to the sochet until a client arrives
    status =  listen(socketfd, 1);
    if (status == -1) {
        //if (DebugMode) 
        Write2Log(LogFileName, "Server: listen error");
        cout << "ERROR: Server stopped listening without accepting client." << endl;
        return -1;
    }
    
    // Accept the new client and return its descriptor
    int new_fd = accept(socketfd, host_info_list->ai_addr, &(host_info_list->ai_addrlen));
    return new_fd;
}



TERInterface **initializeTERAdaptors() {
  TERInterface **array = new TERInterface *[1];
  
  array[0] = new TERAdapter < TerCpp > (new TerCpp(), &TerCpp::getTERScore);
  return array;
}

void Features2Vectors(onlinesvr::Matrix<double>* TrainingSetX, Vector<double>* TrainingSetY){
     for( std::map<int,TrainInstancePtr>::iterator i=SegDict.begin(); i!=SegDict.end(); ++i){
         FeatureVals = (*(*i).second).getFeatures();
         int seg_id = (*(*i).second).getID();
         for( std::map<int,float>::iterator j=FeatureVals.begin(); j!=FeatureVals.end(); ++j){
             TrainingSetX->SetValue(seg_id, (*j).first, (*j).second);
         }
         TrainingSetY->SetValue(seg_id,((*i).second)->getHTER());
     }
}

void AddTrainInstance2Vector(int currentID, onlinesvr::Matrix<double>* TrainingSetX, Vector<double>* TrainingSetY){
    // Get the features of the current segment...
    FeatureVals = SegDict[currentID]->getFeatures();
    
    // ... put it in a vector
    Vector<double>* X = new Vector<double>;
    for( std::map<int,float>::iterator j=FeatureVals.begin(); j!=FeatureVals.end(); ++j){
       X->Add((double)(*j).second);     
    }
    
    // Add it to the training set
    TrainingSetX->AddRowRef(X);
    // Also add its label (result value) to the training labels set
    TrainingSetY->Add(SegDict[currentID]->getHTER());
}

int ConfigureParameters(int socket, int &Algorithm, double &C, double &Epsilon, int &KernelType, 
        double &KernelParam1, double &KernelParam2, int &Shuffle){
    
    // Ideally the input could enable the re-configuration of the parameters
    string input;
   
    
    if(cin.eof()){
        Write2Log(LogFileName, "Get ");
        cin.clear();
        Write2Log(LogFileName, "Received EOF cin message. Will now close client server session.");
        //close(Clientsocketfd);
        return 1;
    }
     getline(cin, input);
     
    if (!input.compare("ENDEND")){
        Write2Log(LogFileName, "Received ENDEND message. Will now close server session.");
        return 0;
    }
    
    if (DebugMode) Write2Log(LogFileName, "Done Configuring Model");
    
    return 1;
    
}

int Initialize(string pfile, string &QuestHost, string &QuestPort, int &UsePort, string &ClientHost, string &ClientPort, int &DebugMode, string &LogFile, int &PreComputedFeatures, int &FeaturesNo, int &SendLabel, int &Algorithm, double &SVRC, double &SVREpsilon, int &KernelType, double &KernelParam1, double &KernelParam2, float &SofiaC, float &SofiaEpsilon, int &Capacity, double &s20, int &KernelType2, float &Lambda, EtaType &Eta_Type, float &Eta, int &DoCV, int &Folds, int &Limit, string &ResultsCVFile, string &epslist, string &clist, string &kplist, int &StoreWeights, string &WeightsFileName, int &StoreFeatures, string &FeatureFileName, int &LoadModelFromFile, string &LoadModelFile, int &SaveModelToFile, string &SaveModelFile){
	
    // Define the name of the properties file here, if you need to change it!
	ifstream Propertiesfile(pfile.c_str());
	string line;
        std::string result;
	
	// Default Values
	Algorithm = 0;
	SVRC = 30;
	SVREpsilon = 0.01;
	KernelType = 100;
	KernelParam1 = 30;
	KernelParam2 = 0;
	SofiaC = 0.1;
	SofiaEpsilon = 0.01;
	Lambda = 0.1;
	Eta_Type = CONSTANT;
	Eta = 0.02;
	LoadModelFromFile = 0;
	SaveModelToFile = 0;
        PreComputedFeatures = 0;
        FeaturesNo = 17; //Baseline features for QuEst
        SendLabel = 0;
        DoCV = 0;
        Capacity = 100;
        s20 = 0.1;
        KernelType2 = 102;        
	
        int linecounter = 0;
	if (Propertiesfile){
            getline(Propertiesfile, line);
            linecounter++;
		while(getline(Propertiesfile,line)){
                    linecounter++;
                    if (!line.empty()){
                        if (line.at(0) == '#')
                            continue;
			else{
                                std::istringstream iss(line);
				if (std::getline(iss, result, '=')){
                                    //cout << result << endl;
                                    if (result == "QuestHost") iss >> QuestHost;
                                    else if (result == "QuestPort") iss >> QuestPort;
                                    else if (result == "UsePort") iss >> UsePort;
                                    else if (result == "ClientHost") iss >> ClientHost;
                                    else if (result == "ClientPort") iss >> ClientPort;
                                    else if (result == "Algorithm") iss >> Algorithm;
                                    else if (result == "PreComputedFeatures") iss >> PreComputedFeatures;
                                    else if (result == "FeaturesNo") iss >> FeaturesNo;
                                    else if (result == "SendLabel") iss >> SendLabel;
                                    else if (result == "SVRC")
                                            iss >> SVRC;
                                    else if (result == "SVREpsilon")
                                            iss >> SVREpsilon;
                                    else if (result == "KernelType")
                                            iss >> KernelType;
                                    else if (result == "KernelParam1")
                                            iss >> KernelParam1;
                                    else if (result == "KernelParam2")
                                            iss >> KernelParam2;
                                    else if (result == "SofiaC")
                                            iss >> SofiaC;
                                    else if (result == "SofiaEpsilon")
                                            iss >> SofiaEpsilon;
                                    else if (result == "Capacity")
                                            iss >> Capacity;
                                    else if (result == "s20")
                                            iss >> s20;
                                    else if (result == "KernelTypeGP")
                                            iss >> KernelType2;
                                    else if (result == "Lambda")
                                            iss >> Lambda;
                                    else if (result == "DebugMode")
                                        iss >> DebugMode;
                                    else if (result == "LogFile"){
                                        string value;
                                        std::getline(iss,value);
                                        LogFile.append(value);
                                    }
                                    else if (result == "Eta_Type")
                                        if (iss.str()=="PEGASOS_ETA")
                                            Eta_Type = PEGASOS_ETA;
                                        else if (iss.str()=="BASIC_ETA")
                                            Eta_Type = BASIC_ETA;
                                        else Eta_Type = CONSTANT;
                                    else if (result == "Eta")
                                            iss >> Eta;
                                    else if (result == "DoCV") iss >> DoCV;
                                    else if (result == "Folds") iss >> Folds;
                                    else if (result == "Limit") iss >> Limit;
                                    else if (result == "ResultsCV"){
                                        string value;
                                        std::getline(iss,value);
                                        ResultsCVFile.append(value);
                                        //if (DebugMode) Write2Log(LogFileName, ResultsCVFile.c_str());
                                    }
                                    else if (result == "EpsilonList"){
                                        string value;
                                        std::getline(iss,value);
                                        epslist.append(value);
                                        //if (DebugMode) Write2Log(LogFileName, ResultsCVFile.c_str());
                                    }
                                    else if (result == "CList"){
                                        string value;
                                        std::getline(iss,value);
                                        clist.append(value);
                                        //if (DebugMode) Write2Log(LogFileName, ResultsCVFile.c_str());
                                    }
                                    else if (result == "KernelParamList"){
                                        string value;
                                        std::getline(iss,value);
                                        kplist.append(value);
                                        //if (DebugMode) Write2Log(LogFileName, ResultsCVFile.c_str());
                                    }
                                    
                                    else if (result == "StoreWeights") iss >> StoreWeights;
                                    else if (result == "WeightsFile"){
                                        string value;
                                        std::getline(iss,value);
                                        WeightsFileName.append(value);
                                    }
                                    else if (result == "StoreFeatures") iss >> StoreFeatures;
                                    else if (result == "FeatureFile"){
                                        string value;
                                        std::getline(iss,value);
                                        FeatureFileName.append(value);
                                    }
                                    else if (result == "LoadModelFromFile")
                                            iss >> LoadModelFromFile;
                                    else if (result == "LoadModelFile"){
                                        string value;
                                        std::getline(iss,value);
                                        LoadModelFile.append(value);
                                    }
                                    else if (result == "SaveModelToFile")
                                            iss >> SaveModelToFile;
                                    else if (result == "SaveModelFile"){
                                        string value;
                                        std::getline(iss,value);
                                        SaveModelFile.append(value);
                                    }
                                    else {
                                            cout << "Wrong input in Properties File in line "<<linecounter <<"." << endl;
                                            return 0;
                                    }
				}
			}
                    }
                }
	}
        else {
            cout << "The properties file could not be found. " << endl;
            return 0;
        }
	return 1;
}


