/* 
 * File:   tools.h
 * Author: anastas
 *
 * Created on July 8, 2013, 11:10 AM
 */

#ifndef __TERCPPTOOLS_H__
#define __TERCPPTOOLS_H__


#include <vector>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <boost/xpressive/xpressive.hpp>
//#include <home/george/anastas/netbeans-7.3.1/boost_1_54_0/boost/xpressive/xpressive.hpp>


using namespace std;

namespace Tools
{
    typedef vector<double> vecDouble;
    typedef vector<char> vecChar;
    typedef vector<int> vecInt;
    typedef vector<float> vecFloat;
    typedef vector<size_t> vecSize_t;
    typedef vector<string> vecString;
    typedef vector<string> alignmentElement;
    typedef vector<alignmentElement> WERalignment;


struct param
{
    bool debugMode;
    string referenceFile;     // path to the resources
    string hypothesisFile;     // path to the configuration files
    string outputFileExtension;
    string outputFileName;
    string referenceSentence;
    string hypothesisSentence;
    double blockSizePercent;
    bool noPunct;
    bool caseOn;
    bool normalize;
    bool tercomLike;
    bool sgmlInputs;
    bool noTxtIds;
    bool printAlignmentsToSTDO;
    bool printAlignmentsToFile;
    bool printDifferenceToHtml;
    bool tokenizedText;
    bool atSentenceLevel;
    bool WER;
    int debugLevel;
};
// param = { false, "","","","" };

// class tools{
// private:
// public:

    string vectorToString ( vector<string> vec );
    string vectorToString ( vector<string> vec, string s );
    vector<string> subVector ( vector<string> vec, int start, int end );
    vector<int> subVector ( vector<int> vec, int start, int end );
    vector<float> subVector ( vector<float> vec, int start, int end );
    vector<string> copyVector ( vector<string> vec );
    vector<int> copyVector ( vector<int> vec );
    vector<float> copyVector ( vector<float> vec );
    vector<string> stringToVector ( string s, string tok );
    vector<int> stringToVectorInt ( string s, string tok );
    vector<float> stringToVectorFloat ( string s, string tok );
    string lowerCase(string str);
    string removePunct(string str);
    string tokenizePunct(string str);
    string removePunctTercom(string str);
    string normalizeStd(string str);
    string printParams(param p);
// };
    param copyParam(param p);
}
#endif