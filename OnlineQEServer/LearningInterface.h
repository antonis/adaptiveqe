/* 
 * File:   LearningInterface.h
 * Author: anastas
 *
 * Created on July 9, 2013, 11:50 AM
 */

#ifndef LEARNINGINTERFACE_H
#define	LEARNINGINTERFACE_H

#include "OnlineSVR.h" // Needed for OnlineSVR
#include "OnlineSVRInterface.h"

#include "Sofiaml.h" // Needed for all Sofiaml Algorithms
#include "SofiaInterface.h"

#include "SOGP.h" // Needed for OnlineGP
#include "OnlineGPInterface.h"

#include <iostream>
#include <map>
#include <cstring>
#include <sstream> 
using namespace std;
using namespace onlinesvr; 

#include "helping.h"
extern int DebugMode;
extern string LogFileName;
extern int NoOfFeatures;

class LearningInterface {
public:
    // 1. Specify the new interface 
    LearningInterface(){}
   // virtual ~LearningInterface(){ if (DebugMode){ stringstream temp; temp << "LearningInterface::~LearningInterface() start (void*):" << (void*)this << endl; Write2Log(LogFileName,temp.str()); } ; delete this; if (DebugMode){ stringstream temp; temp << "LearningInterface::~LearningInterface() end (void*):" << (void*)this << endl; } };
    virtual ~LearningInterface(){};
    //virtual float Predict(std::map<int, float> FeatDict) = 0; 
    virtual float Predict(onlinesvr::Matrix<double>* Feats, std::map<int, float> FeatDict) = 0;
    virtual void Info(){}
    virtual void Clear(){}
    virtual void SaveModelToFile(char* filename){}
    virtual void LoadModelFromFile(char* filename){}
    virtual void Train(onlinesvr::Matrix<double>* Feats, Vector<double>* Labels, int curIt, int LoadModelFromFile){}

    virtual void SetC(double C){}
    virtual double GetC(){}
    virtual void SetEpsilon(double Epsilon){}
    virtual double GetEpsilon(){}
    virtual void SetKernelType(int KernelType){}
    virtual int GetKernelType(){}
    virtual void SetKernelParam1(double KernelParam1){}
    virtual double GetKernelParam1(){}
    virtual void SetKernelParam2(double KernelParam2){}
    virtual double GetKernelParam2(){}
    virtual void SetEtaType(EtaType etatype){}
    virtual EtaType GetEtaType(){}
    virtual void SetEta(float eta){}
    virtual float GetEta(){}
    virtual void SetLambda(float lambda){}
    virtual float GetLambda(){}
    virtual void SetC2(float c){}
    virtual float GetC2(){}
    virtual void SetEpsilon2(float epsilon){}
    virtual float GetEpsilon2(){}
    virtual string GetWeights(){}
    virtual void PrintWeights(char* filename){}
    virtual Vector<double>* Margin (onlinesvr::Matrix<double>* X, Vector<double>* Y){}
    virtual void SetCapacity(int Capacity) {}
    virtual int GetCapacity(){}
    virtual void SetS20(double s20){}
    virtual double GetS20(){}
    virtual void SetKernelGP(int kerneltype2){}
    virtual int GetKernelGP(){}
    virtual void InitMeanStd(int featNo){}
    virtual void ClearMeanStd(){}
    virtual void InitLog(string LogFileName, int DebugMode){}
    
private:
    int i;
};

template <class TYPE>
class LearningAdapter: public LearningInterface {
  public:
    LearningAdapter(TYPE *o, 
            float(TYPE:: *m1)(onlinesvr::Matrix<double>* Feats, std::map<int, float> FeatDict), 
            void(TYPE:: *m2)(),
            void(TYPE:: *m3)(),
            void(TYPE:: *m4)(char* filename), 
            void(TYPE:: *m5)(char* filename), 
            void(TYPE:: *m6)(onlinesvr::Matrix<double>* Feats, Vector<double>* Labels, int CurrentIt, int LoadModelFromFile),
            void(TYPE:: *m7)(double C),
            double(TYPE:: *m8)(),
            void(TYPE:: *m9)(double Epsilon),
            double(TYPE:: *m10)(),
            void(TYPE:: *m11)(int KernelType),
            int(TYPE:: *m12)(),
            void(TYPE:: *m13)(double KernelParam1),
            double(TYPE:: *m14)(),
            void(TYPE:: *m15)(double KernelParam2),
            double(TYPE:: *m16)(),
            void(TYPE:: *m17)(EtaType etatype),
            EtaType(TYPE:: *m18)(),
            void(TYPE:: *m19)(float eta),
            float(TYPE:: *m20)(),
            void(TYPE:: *m21)(float lambda),
            float(TYPE:: *m22)(),
            void(TYPE:: *m23)(float c),
            float(TYPE:: *m24)(),
            void(TYPE:: *m25)(float epsilon),
            float(TYPE:: *m26)(),
            string(TYPE:: *m27)(),
            void(TYPE:: *m28)(char* filename),
            Vector<double>* (TYPE:: *m29)(onlinesvr::Matrix<double>* X, Vector<double>* Y),
            void (TYPE:: *m30)(int capacity),
            int (TYPE:: *m31)(),
            void (TYPE:: *m32)(double s20),
            double (TYPE:: *m33)(),
            void (TYPE:: *m34)(int kerneltype2),
            int (TYPE:: *m35)(),
            void (TYPE:: *m36)(int featNo),
            void (TYPE:: *m37)(),
            void (TYPE:: *m38)(string LogFileName, int DebugMode)
    
    ) {
      object = o;
      predict = m1;
      clear = m2;
      info = m3;
      saveModel = m4;
      loadModel = m5;
      train = m6;
      
      setC = m7;
      getC = m8;
      setEpsilon = m9;
      getEpsilon = m10;
      setKernelType = m11;
      getKernelType = m12;
      setKernelParam1 = m13;
      getKernelParam1 = m14;
      setKernelParam2 = m15;
      getKernelParam2 = m16;
      
      setEtaType = m17;
      getEtaType = m18;
      setEta = m19;
      getEta = m20;
      setLambda = m21;
      getLambda = m22;
      setC2 = m23;
      getC2 = m24;
      setEpsilon2 = m25;
      getEpsilon2 = m26;
      getWeights = m27;
      printWeights = m28;
      margin = m29;
      setCapacity = m30;
      getCapacity = m31;
      sets20 = m32;
      gets20 = m33;
      setKernelGP = m34;
      getKernelGP = m35;
      initMeanStd = m36;
      clearMeanStd = m37;
      initLog = m38;
    }
    
    ~LearningAdapter() {
      if (DebugMode){ stringstream temp; temp << "LearningAdapter::~LearningAdapter() start" << endl; Write2Log(LogFileName,temp.str()); }
      if (DebugMode){ stringstream temp; temp << "LearningAdapter::~LearningAdapter() object (void*):" << (void*) object << endl; Write2Log(LogFileName,temp.str()); }
      delete object;
      if (DebugMode){ stringstream temp; temp << "LearningAdapter::~LearningAdapter() end" << endl; Write2Log(LogFileName,temp.str()); }
    }

    // 4. The adapter/wrapper "maps" the new to the legacy implementation
    float Predict(onlinesvr::Matrix<double>* Feats, std::map<int, float> FeatDict) {  /* the new */
      (object->*predict)(Feats, FeatDict);
    }
    
    void Clear(){
        (object->*clear)();
    }
    void Info(){
        (object->*info)();
    }
    
    void SaveModelToFile(char* filename){
        (object->*saveModel)(filename);
    }
    
    void LoadModelFromFile(char* filename){
        (object->*loadModel)(filename);
    }
    
    void Train(onlinesvr::Matrix<double>* Feats, Vector<double>* Labels, int CurrentIt, int LoadModelFromFile){
        (object->*train)(Feats,Labels, CurrentIt,LoadModelFromFile );
    }
    
   
    
    void SetC(double C){
        (object->*setC)(C);
    }
    double GetC(){
        (object->*getC)();
    }
    void SetEpsilon(double Epsilon){
        (object->*setEpsilon)(Epsilon);
    }
    double GetEpsilon(){
        (object->*getEpsilon)();
    }
    void SetKernelType(int KernelType){
        (object->*setKernelType)(KernelType);
    }
    int GetKernelType(){
        (object->*getKernelType)();
    }
    void SetKernelParam1(double KernelParam1){
        (object->*setKernelParam1)(KernelParam1);
    }
    double GetKernelParam1(){
        (object->*getKernelParam1)();
    }
    void SetKernelParam2(double KernelParam2){
        (object->*setKernelParam2)(KernelParam2);
    }
    double GetKernelParam2(){
        (object->*getKernelParam2)();
    }
        
    void SetEtaType(EtaType etatype){
        (object->*setEtaType)(etatype);
    }
    
    EtaType GetEtaType(){
        (object->*getEtaType)();
    }
    void SetEta(float eta){
        (object->*setEta)(eta);
    }
    float GetEta(){
        (object->*getEta)();
    }
    void SetLambda(float lambda){
        (object->*setLambda)(lambda);
    }
    float GetLambda(){
        (object->*getLambda)();
    }
    void SetC2(float c){
        (object->*setC2)(c);
    }
    float GetC2(){
        (object->*getC2)();
    }
    void SetEpsilon2(float epsilon){
        (object->*setEpsilon2)(epsilon);
    }
    float GetEpsilon2(){
        (object->*getEpsilon2)();
    }
    string GetWeights(){
        (object->*getWeights)();
    }
    void PrintWeights(char* filename){
        (object->*printWeights)(filename);
    }
    
    Vector<double>* Margin (onlinesvr::Matrix<double>* X, Vector<double>* Y){
        (object->*margin)(X,Y);
    }
    void SetCapacity(int capacity) {
        (object->*setCapacity)(capacity);
    }
    
    int GetCapacity(){
        (object->*getCapacity)();
    }
    void SetS20(double s20){
        (object->*sets20)(s20);
    }
    double GetS20(){
        (object->*gets20)();
    }
    void SetKernelGP(int kerneltype2){
        (object->*setKernelGP)(kerneltype2);
    }
    int GetKernelGP(){
        (object->*getKernelGP)();
    }
    void ClearMeanStd(){
        (object->*clearMeanStd)();
    }
    void InitMeanStd(int featNo){
        (object->*initMeanStd)(featNo);
    }
    void InitLog(string LogFileName, int DebugMode){
         (object->*initLog)(LogFileName, DebugMode);
    }

    
  private:
    TYPE *object; // pointer-to-object attribute

    float(TYPE:: *predict)(onlinesvr::Matrix<double>* Feats, std::map<int, float> FeatDict); /* the old */     // ptr-to-member-function attribute
    void(TYPE:: *clear)();
    void(TYPE:: *info)();
    void(TYPE:: *saveModel)(char* filename);
    void(TYPE:: *loadModel)(char* filename);
    void(TYPE:: *train)(onlinesvr::Matrix<double>* Feats, Vector<double>* Labels, int CurrentIt, int LoadModelFromFile);
    void(TYPE:: *setC)(double C);
    double(TYPE:: *getC)();
    void(TYPE:: *setEpsilon)(double Epsilon);
    double(TYPE:: *getEpsilon)();
    void (TYPE:: *setKernelType)(int KernelType);
    int (TYPE:: *getKernelType)();
    void (TYPE:: *setKernelParam1)(double KernelParam1);
    double (TYPE:: *getKernelParam1)();
    void (TYPE:: *setKernelParam2)(double KernelParam2);
    double (TYPE:: *getKernelParam2)();
    
    void (TYPE:: *setEtaType)(EtaType etatype);
    EtaType (TYPE:: *getEtaType)();
    void (TYPE:: *setEta)(float eta);
    float (TYPE:: *getEta)();
    void (TYPE:: *setLambda)(float lambda);
    float (TYPE:: *getLambda)();
    void (TYPE:: *setC2)(float c);
    float (TYPE:: *getC2)();
    void (TYPE:: *setEpsilon2)(float epsilon);
    float (TYPE:: *getEpsilon2)();
    string (TYPE:: *getWeights)();
    void (TYPE:: *printWeights)(char* filename);
    Vector<double>* (TYPE:: *margin)(onlinesvr::Matrix<double>* X, Vector<double>* Y);
    void (TYPE:: *setCapacity)(int capacity);
    int (TYPE:: *getCapacity)();
    void (TYPE:: *sets20)(double s20);
    double (TYPE:: *gets20)();
    void (TYPE:: *setKernelGP)(int kerneltype2);
    int (TYPE:: *getKernelGP)();
    void (TYPE:: *initMeanStd)(int featNo);
    void (TYPE:: *clearMeanStd)();
    void (TYPE:: *initLog)(string LogFileName, int DebugMode);

    
};

void destroyLearningAdaptors(LearningInterface** l) {

    bool DebugMode=true;

    if (DebugMode){ stringstream temp; temp << "Deleting ALL Learn" << endl; Write2Log(LogFileName,temp.str()); }
    if (DebugMode){ stringstream temp; temp << "Deleting ALL Learn l:" << (void*) l << endl; Write2Log(LogFileName,temp.str()); }

    for (int i=0;i<3;++i){
        if (DebugMode){ stringstream temp; temp << "Deleting Learn " << i << " l[i]:" << (void*) l[i] << endl; Write2Log(LogFileName,temp.str()); }
        delete l[i];
        if (DebugMode){ stringstream temp; temp << "Deleted Learn " << i << " l[i]:" << (void*) l[i] << endl; Write2Log(LogFileName,temp.str()); }
    }
    delete []l;
}

void cleanLearningAdaptors(LearningInterface** l) {

    bool DebugMode=true;

    if (DebugMode){ stringstream temp; temp << "Deleting ALL Learn" << endl; Write2Log(LogFileName,temp.str()); }
 

    for (int i=0;i<3;++i){
        if (DebugMode){ stringstream temp; temp << "Deleting Learn " << i << " l[i]:" << (void*) l[i] << endl; Write2Log(LogFileName,temp.str()); }
        delete l[i];
        if (DebugMode){ stringstream temp; temp << "Deleted Learn " << i << " l[i]:" << (void*) l[i] << endl; Write2Log(LogFileName,temp.str()); }
    }
    
}


LearningInterface **initializeLearningAdaptors() {
  if (DebugMode){ stringstream temp; temp << "Creating Learn array" << endl; Write2Log(LogFileName,temp.str()); }
  LearningInterface **array = new LearningInterface *[3];
  if (DebugMode){ stringstream temp; temp << "Created Learn array:" << (void*) array << endl; Write2Log(LogFileName,temp.str()); }

  if (DebugMode){ stringstream temp; temp << "Creating Learn array[0]" << endl; Write2Log(LogFileName,temp.str()); }
  array[0] = new LearningAdapter < OnlineSVRAdapter > 
          (new OnlineSVRAdapter(), 
          &OnlineSVRAdapter::myPredict, 
          &OnlineSVRAdapter::Clear, 
          &OnlineSVRAdapter::ShowInfo, 
          &OnlineSVRAdapter::SaveOnlineSVR,
          &OnlineSVRAdapter::myLoad,
          &OnlineSVRAdapter::myTrain,
          &OnlineSVRAdapter::SetC,
          &OnlineSVRAdapter::GetC,
          &OnlineSVRAdapter::SetEpsilon,
          &OnlineSVRAdapter::GetEpsilon,
          &OnlineSVRAdapter::SetKernelType,
          &OnlineSVRAdapter::GetKernelType,
          &OnlineSVRAdapter::SetKernelParam,
          &OnlineSVRAdapter::GetKernelParam,
          &OnlineSVRAdapter::SetKernelParam2,
          &OnlineSVRAdapter::GetKernelParam2,
          &OnlineSVRAdapter::doNothing6,
          &OnlineSVRAdapter::doNothing8,
          &OnlineSVRAdapter::doNothing7,
          &OnlineSVRAdapter::doNothing2,
          &OnlineSVRAdapter::doNothing7,
          &OnlineSVRAdapter::doNothing2,
          &OnlineSVRAdapter::doNothing7,
          &OnlineSVRAdapter::doNothing2,
          &OnlineSVRAdapter::doNothing7,
          &OnlineSVRAdapter::doNothing2,
          &OnlineSVRAdapter::GetWeightsString,
          &OnlineSVRAdapter::myPrintWeights,
          //&OnlineSVRAdapter::Margin,
          &OnlineSVRAdapter::myMargin,
          &OnlineSVRAdapter::doNothing5,
          &OnlineSVRAdapter::doNothing3,
          &OnlineSVRAdapter::doNothing1,
          &OnlineSVRAdapter::doNothing4,
          &OnlineSVRAdapter::doNothing5,
          &OnlineSVRAdapter::doNothing3,
          &OnlineSVRAdapter::initMeansStDev,
          &OnlineSVRAdapter::clearMeansStDev,
          &OnlineSVRAdapter::initLog
           );
  if (DebugMode){ stringstream temp; temp << "Created Learn array[0]:" << (void*) array[0] << endl; Write2Log(LogFileName,temp.str()); }
  if (DebugMode){ stringstream temp; temp << "Creating Learn array[1]:" << (void*) array << endl; Write2Log(LogFileName,temp.str()); }
  array[1] = new LearningAdapter < SofiaPassiveAggressiveAdapter > 
          (new SofiaPassiveAggressiveAdapter(NoOfFeatures+1), 
          &SofiaPassiveAggressiveAdapter::myPredict,
          &SofiaPassiveAggressiveAdapter::doNothing0, 
          &SofiaPassiveAggressiveAdapter::doNothing0, 
          &SofiaPassiveAggressiveAdapter::mySave,
          &SofiaPassiveAggressiveAdapter::myLoad,
          &SofiaPassiveAggressiveAdapter::myTrain,
          &SofiaPassiveAggressiveAdapter::doNothing1,
          &SofiaPassiveAggressiveAdapter::doNothing4,
          &SofiaPassiveAggressiveAdapter::doNothing1,
          &SofiaPassiveAggressiveAdapter::doNothing4,
          &SofiaPassiveAggressiveAdapter::doNothing5,
          &SofiaPassiveAggressiveAdapter::doNothing3,
          &SofiaPassiveAggressiveAdapter::doNothing1,
          &SofiaPassiveAggressiveAdapter::doNothing4,
          &SofiaPassiveAggressiveAdapter::doNothing1,
          &SofiaPassiveAggressiveAdapter::doNothing4,
          &SofiaPassiveAggressiveAdapter::SetEtaType,
           &SofiaPassiveAggressiveAdapter::GetEtaType,
           &SofiaPassiveAggressiveAdapter::SetEta,
           &SofiaPassiveAggressiveAdapter::GetEta,
           &SofiaPassiveAggressiveAdapter::SetLambda,
           &SofiaPassiveAggressiveAdapter::GetLambda,
           &SofiaPassiveAggressiveAdapter::SetC,
           &SofiaPassiveAggressiveAdapter::GetC,
           &SofiaPassiveAggressiveAdapter::SetEpsilon,
           &SofiaPassiveAggressiveAdapter::GetEpsilon,           
           &SofiaPassiveAggressiveAdapter::myGetWeights,           
           &SofiaPassiveAggressiveAdapter::myPrintWeights,
           &SofiaPassiveAggressiveAdapter::myMargin,
           &SofiaPassiveAggressiveAdapter::doNothing5,
           &SofiaPassiveAggressiveAdapter::doNothing3,
           &SofiaPassiveAggressiveAdapter::doNothing1,
           &SofiaPassiveAggressiveAdapter::doNothing4,
           &SofiaPassiveAggressiveAdapter::doNothing5,
           &SofiaPassiveAggressiveAdapter::doNothing3,  
           &SofiaPassiveAggressiveAdapter::initMeansStDev,
           &SofiaPassiveAggressiveAdapter::clearMeansStDev,
           &SofiaPassiveAggressiveAdapter::initLog
           );
  if (DebugMode){ stringstream temp; temp << "Created Learn array[1]:" << (void*) array[1] << endl; Write2Log(LogFileName,temp.str()); }
  if (DebugMode){ stringstream temp; temp << "Creating Learn array[2]:" << (void*) array << endl; Write2Log(LogFileName,temp.str()); }
  array[2] = new LearningAdapter < OnlineGPAdapter > 
          (new OnlineGPAdapter(), 
          &OnlineGPAdapter::myPredict, 
          &OnlineGPAdapter::doNothing0, 
          &OnlineGPAdapter::doNothing0, 
          &OnlineGPAdapter::mySave,
          &OnlineGPAdapter::myLoad,
          &OnlineGPAdapter::myTrain,
          &OnlineGPAdapter::doNothing1,
          &OnlineGPAdapter::doNothing4,
          &OnlineGPAdapter::doNothing1,
          &OnlineGPAdapter::doNothing4,
          &OnlineGPAdapter::doNothing5,
          &OnlineGPAdapter::doNothing3,
          &OnlineGPAdapter::doNothing1,
          &OnlineGPAdapter::doNothing4,
          &OnlineGPAdapter::doNothing1,
          &OnlineGPAdapter::doNothing4,
          &OnlineGPAdapter::doNothing6,
          &OnlineGPAdapter::doNothing8,
          &OnlineGPAdapter::doNothing7,
          &OnlineGPAdapter::doNothing2,
          &OnlineGPAdapter::doNothing7,
          &OnlineGPAdapter::doNothing2,
          &OnlineGPAdapter::doNothing7,
          &OnlineGPAdapter::doNothing2,
          &OnlineGPAdapter::doNothing7,
          &OnlineGPAdapter::doNothing2,
          &OnlineGPAdapter::myGetWeights,
          &OnlineGPAdapter::myPrintWeights,
          &OnlineGPAdapter::myMargin,
          &OnlineGPAdapter::setCapacity,
          &OnlineGPAdapter::getCapacity,
          &OnlineGPAdapter::setS20,
          &OnlineGPAdapter::getS20,
          &OnlineGPAdapter::setKernel,
          &OnlineGPAdapter::getKernel,
          &OnlineGPAdapter::initMeansStDev,
          &OnlineGPAdapter::clearMeansStDev,
          &OnlineGPAdapter::initLog
    );
  if (DebugMode){ stringstream temp; temp << "Created Learn array[2]:" << (void*) array[2] << endl; Write2Log(LogFileName,temp.str()); }
 
  return array;
}

// Future Call: 





#endif	/* LEARNINGINTERFACE_H */

