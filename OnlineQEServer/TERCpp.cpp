#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fstream>
#include <iostream>
#include <string.h>
#include <vector>
#include "tools.h"
#include "hashMapStringInfos.h"
#include "tercalc.h"
#include "stringInfosHasher.h"
#include "sgmlDocument.h"
#include "multiTxtDocument.h"
#include "xmlStructure.h"
#include "tools.h"
#include "multiEvaluation.h"



using namespace TERCpp;
using namespace std;
using namespace HashMapSpace;
// using namespace boost::xpressive;


void usage();

void readCommandLineArguments ( unsigned int argc, char *argv[] , param & p)
{
    p.debugMode=false;
    p.debugLevel=-1;
    p.caseOn=false;
    p.noPunct=false;
    p.normalize=false;
    p.tercomLike=false;
    p.sgmlInputs=false;
    p.noTxtIds=false;
    p.printAlignmentsToFile = false;
    p.printAlignmentsToSTDO = false;
    p.printDifferenceToHtml = false;
    p.tokenizedText = false; 
    p.atSentenceLevel = false; 
    p.WER=false;
    p.blockSizePercent=0;

    for ( unsigned int i = 1; i < argc; i++ )
    {
        // Command line string
        string s ( argv[i] );
        string infos ("");
        if (i < argc-1)
        {
            infos = argv[i+1];
        }
		
	
        // Configuration files
        if ( s.compare( "-r" ) == 0 )
        {	
		
            p.referenceFile = infos;
        }
	 else if ( s.compare ( "-rSent" ) == 0 )
        {
		
           p.referenceSentence = infos;
		
        }
         else if ( s.compare( "--debugLevel" ) == 0 )
        {
            p.debugLevel = atoi(infos.c_str());
        }
	 else if ( s.compare ( "-hSent" ) == 0 )
        {
            p.hypothesisSentence = infos;
		
        }
         else if ( s.compare ( "-h" ) == 0 )
        {
            p.hypothesisFile = infos;
        }
        else if ( s.compare ( "-o" ) == 0 )
        {
            p.outputFileExtension = infos;
        }
        else if ( s.compare ( "-n" ) == 0 )
        {
            p.outputFileName = infos;
        }
        else if ( s.compare ( "--debugMode" ) == 0 )
        {
            p.debugMode = true;
        }
        else if ( s.compare ( "--noTxtIds" ) == 0 )
        {
            p.noTxtIds = true;
        }
        else if ( s.compare ( "--sgml" ) == 0 )
        {
            p.sgmlInputs = true;
        }
        else if ( s.compare ( "-s" ) == 0 )
        {
            p.caseOn = true;
        }
        else if ( s.compare ( "-P" ) == 0 )
        {
            p.noPunct = true;
        }
        else if ( s.compare ( "-N" ) == 0 )
        {
            p.normalize = true;
        }
        else if ( s.compare ( "--tercom" ) == 0 )
        {
            p.tercomLike = true;
        }
        else if ( s.compare ( "--printAlignmentsToFile" ) == 0 )
        {
            p.printAlignmentsToFile = true;
        }      
	else if ( s.compare ( "--printAlignmentsToSTDO" ) == 0 )
        {
            p.printAlignmentsToSTDO = true;
        }   
	else if ( s.compare ( "--printDifferenceToHtmlToSTDO" ) == 0 )
        {
            p.printDifferenceToHtml = true;
        }
	else if ( s.compare ( "--TokenizedText" ) == 0 )
        {
            p.tokenizedText = true;
        }  
	else if ( s.compare ( "--AtSentenceLevel" ) == 0 )
        {
            p.atSentenceLevel = true;
        }  
        else if ( s.compare ( "--WER" ) == 0 )
        {
            p.WER=true;
        }      
	else if ( s.compare ( "-TerAtBlocks" ) == 0 )
        {
            p.blockSizePercent = atof(infos.c_str());
        }
        else if ( s.compare ( "--help" ) == 0 )
        {
            usage();
        }
        else if ( s.find ( "-" ) == 0 )
        {

	    cerr << "ERROR : tercpp: unknown option :" << s <<endl;
            usage();
        }

	
    }

	//cerr<< p.referenceSentence << "####"<<p.hypothesisSentence<<endl;
}



void usage()
{
// 	cerr<<"tercpp [-N] [-s] [-P] -r ref -h hyp [-a alter_ref] [-b beam_width] [-S trans_span_prefix] [-o out_format -n out_pefix] [-d max_shift_distance] [-M match_cost] [-D delete_cost] [-B substitute_cost] [-I insert_cost] [-T shift_cost]"<<endl;
    cerr<<"Usage : "<<endl<<"\ttercpp [--tercom] [--debugMode] [--debugLevel 0-3] [--noTxtIds] [--printAlignmentsToFile/printAlignmentToSTDO] [--TokenizedText] [--AtSentenceLevel] [--printAlignmentsDifferenceToHtml] [-s] [-P] [--WER] [-TerAtBlocks BlockSizeInPercent] -r ref -h hyp -rSent \"referenceSentence\" -hSent \"hypothesisSentence\" :\n\n\t\t --tercom \t: to use the tercom standart normalization\n\t\t --noTxtIds \t: you don't have to add ids at the end of sentences \n\t\t --debugMode \t: print debug messages \n\t\t --debugLevel \t: print debug messages regarding the level\n\t\t -s \t\t: to be case sensitive\n\t\t -atSentenceLevel \t\t: print the TER score at sentence level\n\t\t -P \t\t: do not take account of punctuation\n\t\t --TokenizedText: the hypothesis and reference files contain tokenized texts\n\t\t -TerAtBlocks \t\t: compute teh TER score for each portion of text of each sentence, the size of the portion is set here passing the percentage (e.g. 0.2, each block contains 20% of the sentence). In STO, it procudes for each sentence: sentence_id SentenceTer Block1Ter Block2Ter ... \n\t\t  --printAlignmentsToFile\t: print alignments information into a separate file\n\t\t --printAlignmentsToSTDO\t: print alignments information into standard output\n\t\t --printDifferenceToHtmlToSTDO\t: print the word difference between the reference and the hypothesis into standard output\n\t\t--WER\t\t: do a simple WER calculation\n\t\t --help \t: print this help message.\n "<<endl;
    exit(0);
// 	System.exit(1);

}





int tercpp ( int argc, char *argv[] )
{
    param myParams;
    readCommandLineArguments ( argc,argv, myParams);
    if (((int)myParams.hypothesisSentence.size()==0) && ((int)myParams.hypothesisFile.size()==0))
    {
        cerr << "ERROR : main : hypothesis file or sentence is not set !" << endl;
        usage();
    }
   if (((int)myParams.referenceSentence.size()==0) && ((int)myParams.referenceFile.size()==0))
    {
        cerr << "ERROR : main : reference file or sentence file is not set !" << endl;
        usage();
    }

	//cerr << myParams.referenceSentence << "###"<< myParams.hypothesisFile << endl;
   if (((int)myParams.referenceSentence.size()!=0) && ((int)myParams.hypothesisFile.size()!=0))
    {
        cerr << "ERROR : main : reference and hypothesis should have the same input format !" << endl;
        usage();
    }
  
   if (((int)myParams.referenceFile.size()!=0) && ((int)myParams.hypothesisSentence.size()!=0))
    {
        cerr << "ERROR : main : reference and hypothesis should have the same input format !" << endl;
        usage();
    } 

    multiEvaluation l_eval(myParams);

    if (!myParams.sgmlInputs)
    {

	if (((int)myParams.referenceSentence.size()!=0) && ((int)myParams.hypothesisSentence.size()!=0))
	{
		l_eval.addReferenceSentence();
		l_eval.setHypothesisSentence();
		l_eval.launchTxtEvaluationAlignToCout();
	}else{
		l_eval.addReferences();
		l_eval.setHypothesis();
		//l_eval.launchTxtEvaluation();
		l_eval.launchTxtEvaluationAlignToCout();
	}
    }
    else
    {
	l_eval.addSGMLReferences();
	l_eval.setSGMLHypothesis();
	l_eval.launchSGMLEvaluation();

    }
    

    return EXIT_SUCCESS;
}
