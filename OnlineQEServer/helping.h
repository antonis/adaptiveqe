/* 
 * File:   helping.h
 * Author: antonis
 *
 * Created on October 5, 2014, 3:08 PM
 */

#include <cstdlib>
#include <iostream>
#include <cstring>      // Needed for memset

#ifndef HELPING_H
#define	HELPING_H

using namespace std;

void Write2Log(string LogFileName, string s);

void replaceAll( string &s, const string &search, const string &replace );

#endif	/* HELPING_H */

