/* 
 * File:   Sofiaml.h
 * Author: anastas
 *
 * Created on July 11, 2013, 5:15 PM
 */

#ifndef SOFIAML_H
#define	SOFIAML_H

#include "OnlineSVR.h" // Needed for OnlineSVR Vectors
#include "SofiamlMethods.h"
#include "SFWeightVector.h"
#include "SFHashWeightVector.h"
//#include <cstdlib>
//#include <cstring>      // Needed for memset

using namespace sofia_ml;

class Sofiaml {
public:
    Sofiaml(LearnerType learner_type, int dimensions);
    Sofiaml(LearnerType learner_type, int dimensions, EtaType eta_type, float lambda, float c, float epsilon);
    ~Sofiaml();
    void createObjects(int dimensions);
    void destroyObjects();

    
    
    /*bool OneLearnerStep(LearnerType learner_type,
		      const SfSparseVector& x,
		      float eta,
		      float c,
		      float lambda,
		      SfWeightVector* w);
    */
    
    void SetLearnerType(LearnerType learner_type);
    LearnerType GetLearnerType();
    void SetEtaType(EtaType etatype);
    EtaType GetEtaType();
    void SetEta(float eta);
    float GetEta();
    float GetEta(EtaType eta_type, float lambda, int i);
    void SetLambda(float lambda);
    float GetLambda();
    void SetC(float c);
    float GetC();
    void SetEpsilon(float epsilon);
    float GetEpsilon();
    std::string GetWeights();
    
    void LoadModelFromFile(const std::string& file_name, SfWeightVector** w);
    void SaveModelToFile(const std::string& file_name, SfWeightVector* w);
    
    //Parameters
    LearnerType learner_type;
    //const SfSparseVector& x;
    EtaType eta_type;
    float eta;
    float c;
    float lambda;
    float epsilon;
    SfWeightVector* w;
    };

#endif	/* SOFIAML_H */

