/*Aux stuff for SOGP*/

#ifndef __SOGP_AUX__
#define __SOGP_AUX__

//Newmat Library and print extension
#define WANT_MATH
#include "../OnlineQEServer/newmat/newmat.h"
#include <stdio.h>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <cstring>

using namespace NEWMAT;

//--------------------------------------------------------
//These could be in a library?  In newmat?  Reads should autodetect?
void printRV(RowVector rv,FILE *fp,const char *name=NULL,bool ascii=false);
void readRV(RowVector &rv,FILE *fp,const char *name=NULL,bool ascii=false);
void printCV(ColumnVector cv,FILE *fp,const char *name=NULL,bool ascii=false);
void readCV(ColumnVector &cv,FILE *fp,const char *name=NULL,bool ascii=false);
void printMatrix(NEWMAT::Matrix m,FILE *fp,const char *name=NULL,bool ascii=false);
void readMatrix(NEWMAT::Matrix &m,FILE *fp,const char *name=NULL,bool ascii=false);
//--------------------------------------------------------
 void Write2Log2(string LogFileName, string s);
//Kernels
//Known Kernels..This could be done better..Reflection?
enum KERNEL{RBF,POL,LIN};

//A kernel is the function, and it's parameters
class SOGPKernel{
 public:
  virtual double kernel(const ColumnVector& a, const ColumnVector& b)=0;
  virtual ReturnMatrix kernelM(const ColumnVector& in, const NEWMAT::Matrix &BV);
  virtual double kstar(const ColumnVector& in);
  virtual double kstar();
  virtual void printTo(FILE *fp,bool ascii=false){
    printf("Kernel Writer %d not written\n",m_type);
  }
  virtual void readFrom(FILE *fp,bool ascii=false){
    printf("Kernel Reader %d not written\n",m_type);
  }
  KERNEL m_type;
};

class RBFKernel: public SOGPKernel{
 public:
  double kernel(const ColumnVector &a, const ColumnVector &b);
  void printTo(FILE *fp,bool ascii = true){
    fprintf(fp,"A %lf\n",A);printRV(widths,fp,"widths",ascii);
  }
  void readFrom(FILE *fp,bool ascii=true){
    fscanf(fp,"A %lf\n",&A);readRV(widths,fp,"widths",ascii);
  }
  RBFKernel(){
    init(.1);
  }
  RBFKernel(double w){
    init(w);
  }
  RBFKernel(RowVector w){
    init(w);
  }
  //Must call this explicitly...shouldn't need to
  void setA(double nA){
    A=nA;
  }
 private:
  double A;//Should likely never change, but just in case
  RowVector widths;//Stored as 1/w
  void init(double w){
    RowVector foo(1);foo(1)=w;init(foo);
  }
  void init(RowVector w){//This is the base case
    m_type=RBF;widths=w;A=1;
    for(int i=1;i<=widths.Ncols();i++)//Invert the widths
      widths(i)=1.0/widths(i);
  }
};

class POLKernel: public SOGPKernel{
 public:
  double kernel(const ColumnVector &a, const ColumnVector &b);
  POLKernel(){
    init(1);
  }
  POLKernel(double s){
    init(s);
  }
  POLKernel(RowVector s){
    init(s);
  }
  void printTo(FILE *fp,bool ascii=true){
    printRV(scales,fp,"scales",ascii);
  }
  void readFrom(FILE *fp,bool ascii=true){
    readRV(scales,fp,"scales",ascii);
  }
 private:
  RowVector scales;
  void init(double s){
    RowVector foo(1);foo(1)=s;init(foo);
  }
  void init(RowVector s){
    m_type=POL;scales=s;
  }
};

class LINKernel: public SOGPKernel{
 public:
  double kernel(const ColumnVector &a, const ColumnVector &b);
  LINKernel(){
    init(1);
  }
  LINKernel(double s){
    init(s);
  }
  LINKernel(RowVector s){
    init(s);
  }
  void printTo(FILE *fp,bool ascii=false){
    printRV(scales,fp,"scales",ascii);
  }
  void readFrom(FILE *fp,bool ascii=false){
    readRV(scales,fp,"scales",ascii);
  }
 private:
  RowVector scales;
  void init(double s){
    RowVector foo(1);foo(1)=s;init(foo);
  }
  void init(RowVector s){
    m_type=LIN;scales=s;
  }
};

//SOGP Parameters
class SOGPParams{
 public:
  //should params be private and 'set'?
  int capacity;
  double s20;  //Should be a vector?
  SOGPKernel *m_kernel;//Cannot edit kernel once made.

  void printTo(FILE *fp,bool ascii=false){
    fprintf(fp,"capacity %d, s20 %lf\n",capacity,s20);
    fprintf(fp,"kernel %d ",m_kernel->m_type);
    m_kernel->printTo(fp,ascii);
  };
  void readFrom(FILE *fp,bool ascii=false){
    fscanf(fp,"capacity %d, s20 %lf\n",&capacity,&s20);
    int temp;
    fscanf(fp,"kernel %d ",&temp);
    switch(temp){
      //This should be better?
    case RBF: m_kernel=new RBFKernel();break;
    case POL: m_kernel=new POLKernel();break;
    case LIN: m_kernel=new LINKernel();break;
    default: printf("SOGPParams readFrom: Unknown Kernel! %d\n",temp);
    }
    m_kernel->readFrom(fp,ascii);
  }
  SOGPParams(){
    setDefs();
  }
  SOGPParams(SOGPKernel *kern){
    setDefs();
    delete m_kernel;
    switch(kern->m_type){
      //And this
    case RBF: m_kernel=new RBFKernel(*((RBFKernel *)kern));break;
    case POL: m_kernel=new POLKernel(*((POLKernel *)kern));break;
    case LIN: m_kernel=new LINKernel(*((LINKernel *)kern));break;
    }
  }
  private:
  void setDefs(){
    capacity=1000;
    s20=0.1;
    m_kernel=new RBFKernel;
  }
};

#endif