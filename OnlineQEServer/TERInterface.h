/* 
 * File:   TERInterface.h
 * Author: anastas
 *
 * Created on July 8, 2013, 4:50 PM
 */

#ifndef TERINTERFACE_H
#define	TERINTERFACE_H

#include <iostream>
#include <fstream>
#include <string.h>
#include <vector>
#include "tools.h"
#include "hashMapStringInfos.h"
#include "tercalc.h"
#include "stringInfosHasher.h"
#include "sgmlDocument.h"
#include "multiTxtDocument.h"
#include "xmlStructure.h"
#include "tools.h"
#include "multiEvaluation.h"

using namespace TERCpp;
using namespace std;
using namespace HashMapSpace;


class TERInterface {
public:
    // 1. Specify the new interface
    virtual ~TERInterface(){}
    virtual float execute(string ref, string hyp) = 0; 
};

template <class TYPE>
class TERAdapter: public TERInterface {
  public:
    TERAdapter(TYPE *o, float(TYPE:: *m)(string ref, string hyp)) {
      object = o;
      method = m;
    }
    ~TERAdapter() {
      delete object;
    }

    // 4. The adapter/wrapper "maps" the new to the legacy implementation
    float execute(string ref, string hyp) {  /* the new */
      (object->*method)(ref, hyp);
    }
  private:
    TYPE *object; // ptr-to-object attribute

    float(TYPE:: *method)(string ref, string hyp); /* the old */     // ptr-to-member-function attribute
};


// The old: three totally incompatible classes
// no common base class,


class TerCpp {
public:
    ~TerCpp(){
        //cout << "TerCpp::dtor" << endl;
    }
    float getTERScore(string ref, string hyp){
        param p;
        
        // Init Parameters
        p.debugMode=false;
        p.debugLevel=-1;
        p.caseOn=false;
        p.noPunct=false;
        p.normalize=false;
        p.tercomLike=false;
        p.sgmlInputs=false;
        p.noTxtIds=false;
        p.printAlignmentsToFile = false;
        p.printAlignmentsToSTDO = false;
        p.printDifferenceToHtml = false;
        p.tokenizedText = false; 
        p.atSentenceLevel = true; 
        p.WER=false;
        p.blockSizePercent=0;
        
        // Set Parameters
        p.referenceSentence = ref;
        p.hypothesisSentence = hyp;
        p.noTxtIds = true;
        p.caseOn = true;
        //p.noPunct = true;
        p.tokenizedText = true;
        
        
     
       // ofstream LogFile;
       // LogFile.open("QE.log", ios::app);
       // if (LogFile.is_open()) {
       //   LogFile << "SUCAAAAA " << ref << " " << hyp << endl;
       //   LogFile.close();
       //  } 
        
        multiEvaluation l_eval(p);
        float ans;
        
        if (!p.sgmlInputs)
        {

           if (((int)p.referenceSentence.size()!=0) && ((int)p.hypothesisSentence.size()!=0))
          
          {

                    l_eval.addReferenceSentence();
                    l_eval.setHypothesisSentence();
                    ans = l_eval.launchTxtEvaluationAlignToCout();
           }else{
               ans = 1;
           }
            //        l_eval.addReferences();
           //         l_eval.setHypothesis();
                    
            //        ans = l_eval.launchTxtEvaluationAlignToCout();
           // }
        }
        else
        {
            l_eval.addSGMLReferences();
            l_eval.setSGMLHypothesis();
            l_eval.launchSGMLEvaluation();

        }
        
        return ans;

    
    }
};

#endif	/* TERINTERFACE_H */

