/* 
 * File:   multiEvaluation.cpp
 * Author: anastas
 * 
 * Created on July 8, 2013, 10:56 AM
 */

#include "multiEvaluation.h"

#include <math.h>
#include <sstream>

// #include <iostream>
// #include <boost/filesystem/fstream.hpp>
// #include <boost/archive/xml_oarchive.hpp>
// #include <boost/archive/xml_iarchive.hpp>
// #include <boost/serialization/nvp.hpp>

// helper functions to allow us to load and save sandwiches to/from xml


namespace TERCpp
{

    multiEvaluation::multiEvaluation()
    {
        evalParameters.debugMode = false;
        evalParameters.caseOn = false;
        evalParameters.noPunct = false;
        evalParameters.normalize = false;
        evalParameters.tercomLike = false;
        evalParameters.sgmlInputs = false;
        evalParameters.noTxtIds = false;
// 	referencesTxt=new multiTxtDocument();
// 	hypothesisTxt=new documentStructure();
    }

    multiEvaluation::multiEvaluation ( param p )
    {
        evalParameters.debugMode = false;
        evalParameters.caseOn = false;
        evalParameters.noPunct = false;
        evalParameters.normalize = false;
        evalParameters.tercomLike = false;
        evalParameters.sgmlInputs = false;
        evalParameters.noTxtIds = false;
	
        evalParameters = Tools::copyParam ( p );
// 	referencesTxt=new multiTxtDocument();
// 	hypothesisTxt=new documentStructure();
    }

    void multiEvaluation::addReferences()
    {
        referencesTxt.loadRefFiles ( evalParameters );
    }

    void multiEvaluation::addReferenceSentence()
    {
	//cout <<" ADD REF" << evalParameters.referenceSentence << endl;
        referencesTxt.loadRefSentence ( evalParameters );
    }

// void multiEvaluation::addReferences(vector< string > vecRefecrences)
// {
//     for (int i=0; i< (int) vecRefecrences.size(); i++)
//     {
//         referencesTxt.loadFile(vecRefecrences.at(i));
//     }
// }


    void multiEvaluation::setHypothesis()
    {
        multiTxtDocument l_multiTxtTmp;
        l_multiTxtTmp.loadHypFile ( evalParameters );
        hypothesisTxt = (*(l_multiTxtTmp.getDocument ( "0" )));
    }

   void multiEvaluation::setHypothesisSentence()
    {
        multiTxtDocument l_multiTxtTmp;
        l_multiTxtTmp.loadHypSentence ( evalParameters );
        hypothesisTxt = (*(l_multiTxtTmp.getDocument ( "0" )));
    }


    void multiEvaluation::setParameters ( param p )
    {
        evalParameters = Tools::copyParam ( p );
    }

    void multiEvaluation::launchTxtEvaluation()
    {
	if (evalParameters.debugMode)
	{
		    cerr <<"DEBUG tercpp : multiEvaluation::launchTxtEvaluation : before testing references and hypothesis size  "<<endl<<"END DEBUG"<<endl;
	}
	  
        if ( referencesTxt.getSize() == 0 )
        {
            cerr << "ERROR : multiEvaluation::launchTxtEvaluation : there is no references" << endl;
            exit ( 0 );
        }
        if ( hypothesisTxt.getSize() == 0 )
        {
            cerr << "ERROR : multiEvaluation::launchTxtEvaluation : there is no hypothesis" << endl;
            exit ( 0 );
        }
	if (evalParameters.debugMode)
	{
		    cerr <<"DEBUG tercpp : multiEvaluation::launchTxtEvaluation : testing references and hypothesis size  "<<endl<<" number of references : "<<  referencesTxt.getSize()<<endl; 
		    vector <string> s =referencesTxt.getListDocuments();
		    cerr << " avaiable ids : ";
		    for (vector <string>::iterator iterS=s.begin(); iterS!=s.end(); iterS++)
		    {
			cerr << " " << (*iterS);
		    }
		    cerr << endl;
		    for (vector <string>::iterator iterSBis=s.begin(); iterSBis!=s.end(); iterSBis++)
		    {
			cerr << " reference : "+(*iterSBis)+";  size : "<<  (referencesTxt.getDocument((*iterSBis)))->getSize() << endl;
		    }
		    cerr << " hypothesis size : "<<  hypothesisTxt.getSize() << endl<<"END DEBUG"<<endl;
	}
	  
        int incDocRefences = 0;
        stringstream l_stream;
        vector<float> editsResults;
        vector<float> wordsResults;
        int tot_ins = 0;
        int tot_del = 0;
        int tot_sub = 0;
        int tot_sft = 0;
        int tot_wsf = 0;
        float tot_err = 0;
        float tot_wds = 0;
//         vector<stringInfosHasher> setOfHypothesis = hashHypothesis.getHashMap();
	ofstream outputAlignments;
        if (evalParameters.printAlignmentsToFile)
	{
	    outputAlignments.open( ( evalParameters.hypothesisFile + ".alignments" ).c_str() );
	}
	ofstream outputSum ( ( evalParameters.hypothesisFile + ".output.sum.log" ).c_str() );
        outputSum << "Hypothesis File: " + evalParameters.hypothesisFile + "\nReference File: " + evalParameters.referenceFile + "\n" + "Ave-Reference File: " << endl;
        char outputCharBuffer[200];
        sprintf ( outputCharBuffer, "%19s | %4s | %4s | %4s | %4s | %4s | %6s | %8s | %8s", "Sent Id", "Ins", "Del", "Sub", "Shft", "WdSh", "NumEr", "AvNumWd", "TER");
        outputSum << outputCharBuffer << endl;
        outputSum << "-------------------------------------------------------------------------------------" << endl;
	vector <string> referenceList =referencesTxt.getListDocuments();
	for (vector <string>::iterator referenceListIter=referenceList.begin(); referenceListIter!=referenceList.end(); referenceListIter++)
	{
// 	    cerr << " " << (*referenceListIter);
            documentStructure l_reference = (*(referencesTxt.getDocument ( (*referenceListIter) )));
            evaluate ( l_reference, hypothesisTxt );
//             evaluate ( l_reference);
	}

//         for ( incDocRefences = 0; incDocRefences < referencesTxt.getSize();incDocRefences++ )
//         {
//             l_stream.str ( "" );
//             l_stream << incDocRefences;
//         }
        for ( vector<segmentStructure>::iterator segHypIt = hypothesisTxt.getSegments()->begin(); segHypIt != hypothesisTxt.getSegments()->end(); segHypIt++ )
        {
            terAlignment l_result = segHypIt->getAlignment();
	    string bestDocId = segHypIt->getBestDocId();
	    string l_id=segHypIt->getSegId();
            editsResults.push_back(l_result.numEdits);
            wordsResults.push_back(l_result.averageWords);
            l_result.scoreDetails();
            tot_ins += l_result.numIns;
            tot_del += l_result.numDel;
            tot_sub += l_result.numSub;
            tot_sft += l_result.numSft;
            tot_wsf += l_result.numWsf;
            tot_err += l_result.numEdits;
            tot_wds += l_result.averageWords;

            char outputCharBufferTmp[200];
            sprintf(outputCharBufferTmp, "%19s | %4d | %4d | %4d | %4d | %4d | %6.1f | %8.3f | %8.3f",(l_id+":"+bestDocId).c_str(), l_result.numIns, l_result.numDel, l_result.numSub, l_result.numSft, l_result.numWsf, l_result.numEdits, l_result.averageWords, l_result.scoreAv()*100.0);
            outputSum<< outputCharBufferTmp<<endl;

            if (evalParameters.debugMode)
            {
                cerr <<"DEBUG tercpp : multiEvaluation::launchTxtEvaluation : Evaluation "<<endl<< l_result.toString() <<endl<<"END DEBUG"<<endl;
            }
	    if (evalParameters.printAlignmentsToFile)
	    {
		
		outputAlignments << l_id<<endl;
		outputAlignments << "REF:" << "\t"<<vectorToString(l_result.ref," ")<<endl;
		outputAlignments << "HYP:"<< "\t"<<vectorToString(l_result.hyp," ")<<endl;
		outputAlignments << "HYP AFTER SHIFT:"<< "\t"<<vectorToString(l_result.aftershift," ")<<endl;
		outputAlignments << "ALIG:"<< "\t"<<l_result.printAlignments();//<<endl;
		outputAlignments << " ||| " << l_result.printAllShifts();
// 		outputAlignments << "ALIG:"<< "\t"<<l_result.toString()<<"\t"<<endl;
		outputAlignments << endl;
	    }else if (evalParameters.printAlignmentsToSTDO){
	    	cout << l_id<<endl;
		cout << "REF:" << "\t"<<vectorToString(l_result.ref," ")<<endl;
		cout << "HYP:"<< "\t"<<vectorToString(l_result.hyp," ")<<endl;
		cout << "HYP AFTER SHIFT:"<< "\t"<<vectorToString(l_result.aftershift," ")<<endl;
		cout << "ALIG:"<< "\t"<<l_result.printAlignments();//<<endl;
		cout << " ||| " << l_result.printAllShifts();
// 		outputAlignments << "ALIG:"<< "\t"<<l_result.toString()<<"\t"<<endl;
		cout << endl;

	    }


        }

	if (evalParameters.WER)
	{
	    cout << "Total WER: " << scoreTER ( editsResults, wordsResults );
	}
	else
	{
	    //cout << "Total TER: " << scoreTER ( editsResults, wordsResults );
	}
	char outputCharBufferTmp[200];
        outputSum << "-------------------------------------------------------------------------------------" << endl;
        sprintf ( outputCharBufferTmp, "%19s | %4d | %4d | %4d | %4d | %4d | %6.1f | %8.3f | %8.3f", "TOTAL", tot_ins, tot_del, tot_sub, tot_sft, tot_wsf, tot_err, tot_wds, tot_err*100.0 / tot_wds );
        outputSum << outputCharBufferTmp << endl;
        outputSum.close();
        
        //return scoreTER (editsResults, wordsResults);

    }




  float multiEvaluation::launchTxtEvaluationAlignToCout()
    {
 	
	if (evalParameters.debugMode)
	{
		    cerr <<"DEBUG tercpp : multiEvaluation::launchTxtEvaluation : before testing references and hypothesis size  "<<endl<<"END DEBUG"<<endl;
	}
	  
        if ( referencesTxt.getSize() == 0 )
        {
            cerr << "ERROR : multiEvaluation::launchTxtEvaluation : there is no references" << endl;
            exit ( 0 );
        }
        if ( hypothesisTxt.getSize() == 0 )
        {
            cerr << "ERROR : multiEvaluation::launchTxtEvaluation : there is no hypothesis" << endl;
            exit ( 0 );
        }
	if (evalParameters.debugMode)
	{
		    cerr <<"DEBUG tercpp : multiEvaluation::launchTxtEvaluation : testing references and hypothesis size  "<<endl<<" number of references : "<<  referencesTxt.getSize()<<endl; 
		    vector <string> s =referencesTxt.getListDocuments();
		    cerr << " avaiable ids : ";
		    for (vector <string>::iterator iterS=s.begin(); iterS!=s.end(); iterS++)
		    {
			cerr << " " << (*iterS);
		    }
		    cerr << endl;
		    for (vector <string>::iterator iterSBis=s.begin(); iterSBis!=s.end(); iterSBis++)
		    {
			cerr << " reference : "+(*iterSBis)+";  size : "<<  (referencesTxt.getDocument((*iterSBis)))->getSize() << endl;
		    }
		    cerr << " hypothesis size : "<<  hypothesisTxt.getSize() << endl<<"END DEBUG"<<endl;
	}

	if(evalParameters.blockSizePercent != 0)
	{	//	cout << l_id<<endl;

		//cout<<  "#Sentence Id#" << "\t"<<  "#Sentence TER#"<<"\t"<<"#Nr. Words in common#"<<"\t"<< "#Nr. Token in the Reference#" <<"\t"<< "#Reference Block Size#"<<"\t"<<"#(Nr. Words in common)/Ref. Size#" <<"\t"<<"#Nr. Token in the MT output#" <<"\t"<< "#MT Output Block Size#" <<"\t"<<"#(Nr. Words in common)/MT. Size#"<<"\t"<< "#Block TER#" <<endl;
		cout<<  "#Sentence Id#" << "\t"<<  "#Sentence TER#"<<"\t"<<"#Nr. Words in common#"<<"\t"<< "#Nr. Token in the Reference#" <<"\t"<< "#Reference Block Size#"<<"\t"<<"#(Nr. Words in common)/Ref. Size#" <<"\t"<<"#Nr. Token in the MT output#" <<"\t"<< "#MT Output Block Size#" <<"\t"<<"#(Nr. Words in common)/MT. Size#" <<endl;
	}

	  
        int incDocRefences = 0;
        stringstream l_stream;
        vector<float> editsResults;
        vector<float> wordsResults;
        int tot_ins = 0;
        int tot_del = 0;
        int tot_sub = 0;
        int tot_sft = 0;
        int tot_wsf = 0;
        float tot_err = 0;
        float tot_wds = 0;
//         vector<stringInfosHasher> setOfHypothesis = hashHypothesis.getHashMap();
	ofstream outputAlignments;
     //   if (evalParameters.printAlignments)
//	{
//	    outputAlignments.open( ( evalParameters.hypothesisFile + ".alignments" ).c_str() );
//	}
	ofstream outputSum ( ( evalParameters.hypothesisFile + ".output.sum.log" ).c_str() );
        outputSum << "Hypothesis File: " + evalParameters.hypothesisFile + "\nReference File: " + evalParameters.referenceFile + "\n" + "Ave-Reference File: " << endl;
        char outputCharBuffer[200];
        sprintf ( outputCharBuffer, "%19s | %4s | %4s | %4s | %4s | %4s | %6s | %8s | %8s", "Sent Id", "Ins", "Del", "Sub", "Shft", "WdSh", "NumEr", "AvNumWd", "TER");
        outputSum << outputCharBuffer << endl;
        outputSum << "-------------------------------------------------------------------------------------" << endl;
	vector <string> referenceList =referencesTxt.getListDocuments();

	//cerr << "SIZE: "<<referenceList.size()<<endl;
	for (vector <string>::iterator referenceListIter=referenceList.begin(); referenceListIter!=referenceList.end(); referenceListIter++)
	{
// 	    cerr << " " << (*referenceListIter);
            documentStructure l_reference = (*(referencesTxt.getDocument ( (*referenceListIter) )));
            evaluate ( l_reference, hypothesisTxt );
//             evaluate ( l_reference);
	}

//         for ( incDocRefences = 0; incDocRefences < referencesTxt.getSize();incDocRefences++ )
//         {
//             l_stream.str ( "" );
//             l_stream << incDocRefences;
//         }
        for ( vector<segmentStructure>::iterator segHypIt = hypothesisTxt.getSegments()->begin(); segHypIt != hypothesisTxt.getSegments()->end(); segHypIt++ )
        {
            terAlignment l_result = segHypIt->getAlignment();
	    
	    string bestDocId = segHypIt->getBestDocId();
	    string l_id=segHypIt->getSegId();
            editsResults.push_back(l_result.numEdits);
            wordsResults.push_back(l_result.averageWords);
            l_result.scoreDetails();
            tot_ins += l_result.numIns;
            tot_del += l_result.numDel;
            tot_sub += l_result.numSub;
            tot_sft += l_result.numSft;
            tot_wsf += l_result.numWsf;
            tot_err += l_result.numEdits;
            tot_wds += l_result.averageWords;

	    vector<terShift> shiftInfo = l_result.getAllShifts();
	    string align = l_result.printAlignments();
            char outputCharBufferTmp[200];
            sprintf(outputCharBufferTmp, "%19s | %4d | %4d | %4d | %4d | %4d | %6.1f | %8.3f | %8.3f",(l_id+":"+bestDocId).c_str(), l_result.numIns, l_result.numDel, l_result.numSub, l_result.numSft, l_result.numWsf, l_result.numEdits, l_result.averageWords, l_result.scoreAv()*100.0);
            outputSum<< outputCharBufferTmp<<endl;

	

	    if (evalParameters.atSentenceLevel)
            {
		//cout << "id:\t" << l_id << "\t"<<  l_result.score() << endl;
	    }

            if (evalParameters.debugMode)
            {
                cerr <<"DEBUG tercpp : multiEvaluation::launchTxtEvaluation : Evaluation "<<endl<< l_result.toString() <<endl<<"END DEBUG"<<endl;
            }
	    if (evalParameters.printAlignmentsToSTDO)
	    {
		
	//	cout << l_id<<endl;
		cout << "REF:" << "\t"<<vectorToString(l_result.ref," ")<<endl;
		cout << "HYP:"<< "\t"<<vectorToString(l_result.hyp," ")<<endl;
		cout << "HYP AFTER SHIFT:"<< "\t"<<vectorToString(l_result.aftershift," ")<<endl;
		cout << "ALIG:"<< "\t"<<l_result.printAlignments();//<<endl;
		cout << " ||| " << l_result.printAllShifts();
// 		outputAlignments << "ALIG:"<< "\t"<<l_result.toString()<<"\t"<<endl;
		cout << endl;

		cout << "REFERENCE => SHIFTED HYPOYHESIS" <<endl;

		cout<< "A:##Begin##" << endl;
	
		int idx1=0, idx2=0;
		string finalOutput="";
		vector<string> shiftedHyp, ref_A;
		int count =0;
		cout<<"Align Length: "<< (int)align.length()<<endl;
		for (int i=0; i<(int)align.length(); i++)
		{
			cout<<"Align: "<< align[i]<<endl;
			if(align.at(i) == 'A')
                        {
				
                                cout<<"A: "<<l_result.ref[idx1]<<" => "<< l_result.aftershift[idx2]<<"\n";
				/*bool found = false;
				for (int jj=0; jj < ( int ) shiftInfo.size(); jj++ )
				{	int m = (int)shiftInfo[jj].newloc;
					//cout << i<< " NewLoc: "<<m<< endl;
					if(l_result.ref[idx1] == vectorToString(shiftInfo[jj].shifted ) && (((m-4) <count) && ((m+4) >count)) )
					{
						cout << count<< " NewLoc: "<<m<< endl;
						finalOutput = finalOutput+ " " +"<span style=\"color:green\">"+ l_result.ref[idx1]+  "</span>" ;
						found = true;	
					}
				}	
				if(found == false)
				{
					finalOutput = finalOutput+ " " + l_result.ref[idx1];
				}*/
				//finalOutput = finalOutput+ " " + l_result.ref[idx1];
                                //align[ref_A[idx1]]=shiftedHyp[idx2];
				idx1 ++;
				idx2 ++;
				count++;
                        }
			if( align.at(i) == 'S')
			{
                                cout<<"S: "<<l_result.ref[idx1]<<" => "<< l_result.aftershift[idx2]<<"\n";
				//finalOutput = finalOutput +" <strike><span style=\"color:red;\">" +l_result.aftershift[idx2]  +"</span></strike>"+" <span style=\"color:blue\">" +l_result.ref[idx1]+ "</span>" ;
				//finalOutput = finalOutput +" " + l_result.ref[idx1]+"("+l_result.aftershift[idx2]+")";
				/*if((i < align.length()) && (align.at(i+1) != 'S'))
				{
					finalOutput = finalOutput +")";
				}*/
                                //align[ref_A[idx1]]=shiftedHyp[idx2];
				idx1 ++;
				idx2 ++;
				count++;
                        }
                        if(align.at(i) == 'I')
                        {	//in the shifted hyp but not in the ref
				cout<<"I: "<<"NULL => "<< l_result.aftershift[idx2]<<"\n";
				/*if((i >0)  && (align.at(i-1) != 'I'))
				{
					finalOutput = finalOutput +" (";
				}*/
			
				//finalOutput = finalOutput +" <strike><span style=\"color:red;\">" +l_result.aftershift[idx2]  +"</span></strike>";
				//finalOutput = finalOutput+" (" +l_result.aftershift[idx2]+")";
				/*if((i < align.length())  && (align.at(i+1) != 'I'))
				{
					finalOutput = finalOutput +")";
				}*/
				//align["NULL"]=shiftedHyp[idx2];
				idx2 ++;
				count++;
                        }
                        if(align.at(i) == 'D')
                        {	
				// in the ref but not in the shifted hyp
				 cout<<"D: "<<l_result.ref[idx1]<<" => NULL"<<endl;
				//finalOutput = finalOutput +" <span style=\"color:orange\">" +l_result.ref[idx1]+ "</span>"; 
				//finalOutput = finalOutput +" " + l_result.ref[idx1];
				//align[ref_A[idx1]]="NULL";
				idx1 ++;
				count++;
                        }
			
			
		}
		//cout<< "DiffViwe: "+finalOutput <<endl; 
		cout<< "A:##End##" << endl;
	    }else if (evalParameters.printAlignmentsToFile)
	    {
		ofstream outputAlignments;
       
		outputAlignments.open( ( "output.alignments" ) );
				//outputAlignments << l_id<<endl;
		outputAlignments << "REF:" << "\t"<<vectorToString(l_result.ref," ")<<endl;
		outputAlignments << "HYP:"<< "\t"<<vectorToString(l_result.hyp," ")<<endl;
		outputAlignments << "HYP AFTER SHIFT:"<< "\t"<<vectorToString(l_result.aftershift," ")<<endl;
		outputAlignments << "ALIG:"<< "\t"<<l_result.printAlignments();//<<endl;
		outputAlignments << " ||| " << l_result.printAllShifts();
// 		outputAlignments << "ALIG:"<< "\t"<<l_result.toString()<<"\t"<<endl;
		outputAlignments << endl;


		int idx1=0, idx2=0;
		string finalOutput="";
		vector<string> shiftedHyp, ref_A;
		int count =0;
		outputAlignments<<"Align Length: "<< (int)align.length()<<endl;
		for (int i=0; i<(int)align.length(); i++)
		{
			//cout<<"Align: "<< align[i]<<endl;
			if(align.at(i) == 'A')
                        {
                               outputAlignments<<"A: "<<l_result.ref[idx1]<<" => "<< l_result.aftershift[idx2]<<"\n";
				/*bool found = false;
				for (int jj=0; jj < ( int ) shiftInfo.size(); jj++ )
				{	int m = (int)shiftInfo[jj].newloc;
					//cout << i<< " NewLoc: "<<m<< endl;
					if(l_result.ref[idx1] == vectorToString(shiftInfo[jj].shifted ) && (((m-4) <count) && ((m+4) >count)) )
					{
						cout << count<< " NewLoc: "<<m<< endl;
						finalOutput = finalOutput+ " " +"<span style=\"color:green\">"+ l_result.ref[idx1]+  "</span>" ;
						found = true;	
					}
				}	
				if(found == false)
				{
					finalOutput = finalOutput+ " " + l_result.ref[idx1];
				}*/
				//finalOutput = finalOutput+ " " + l_result.ref[idx1];
                                //align[ref_A[idx1]]=shiftedHyp[idx2];
				idx1 ++;
				idx2 ++;
				count++;
                        }
			if( align.at(i) == 'S')
			{
                                outputAlignments<<"S: "<<l_result.ref[idx1]<<" => "<< l_result.aftershift[idx2]<<"\n";
				//finalOutput = finalOutput +" <strike><span style=\"color:red;\">" +l_result.aftershift[idx2]  +"</span></strike>"+" <span style=\"color:blue\">" +l_result.ref[idx1]+ "</span>" ;
				//finalOutput = finalOutput +" " + l_result.ref[idx1]+"("+l_result.aftershift[idx2]+")";
				/*if((i < align.length()) && (align.at(i+1) != 'S'))
				{
					finalOutput = finalOutput +")";
				}*/
                                //align[ref_A[idx1]]=shiftedHyp[idx2];
				idx1 ++;
				idx2 ++;
				count++;
                        }
                        if(align.at(i) == 'I')
                        {	//in the shifted hyp but not in the ref
				outputAlignments<<"I: "<<"NULL => "<< l_result.aftershift[idx2]<<"\n";
				/*if((i >0)  && (align.at(i-1) != 'I'))
				{
					finalOutput = finalOutput +" (";
				}*/
			
				//finalOutput = finalOutput +" <strike><span style=\"color:red;\">" +l_result.aftershift[idx2]  +"</span></strike>";
				//finalOutput = finalOutput+" (" +l_result.aftershift[idx2]+")";
				/*if((i < align.length())  && (align.at(i+1) != 'I'))
				{
					finalOutput = finalOutput +")";
				}*/
				//align["NULL"]=shiftedHyp[idx2];
				idx2 ++;
				count++;
                        }
                        if(align.at(i) == 'D')
                        {	
				// in the ref but not in the shifted hyp
				 outputAlignments<<"D: "<<l_result.ref[idx1]<<" => NULL"<<endl;
				//finalOutput = finalOutput +" <span style=\"color:orange\">" +l_result.ref[idx1]+ "</span>"; 
				//finalOutput = finalOutput +" " + l_result.ref[idx1];
				//align[ref_A[idx1]]="NULL";
				idx1 ++;
				count++;
                        }
			
			
		}
		
		
	    }


	    if(evalParameters.printDifferenceToHtml)
	    {	//	cout << l_id<<endl;
	

		//string align = l_result.printAlignments();
	
		int idx1=0, idx2=0;
		string finalOutput="";
		vector<string> shiftedHyp, ref_A;
		int count =0;

		for (int i=0; i<(int)align.length(); i++)
		{
			
			if(align.at(i) == 'A')
                        {
                               
				bool found = false;
				for (int jj=0; jj < ( int ) shiftInfo.size(); jj++ )
				{	int m = (int)shiftInfo[jj].newloc;
					//cout << i<< " NewLoc: "<<m<< endl;
					//if(l_result.ref[idx1] == vectorToString(shiftInfo[jj].shifted ) && (((m-4) <count) && ((m+4) >count)) )
					//for (int ii=0; ii<(shiftInfo[jj].shifted).size(); ii++)
    					//	cout<<"#"<<(shiftInfo[jj].shifted).at(ii)<<endl;
					
					//if((vectorToString(shiftInfo[jj].shifted).find(l_result.ref[idx1]) != string::npos ) && (((m-4) <count) && ((m+4) >count)) )
					if(( std::find((shiftInfo[jj].shifted).begin(), (shiftInfo[jj].shifted).end(), l_result.ref[idx1]) != (shiftInfo[jj].shifted).end()) && (((m-4) <count) && ((m+4) >count)) )
					//std::find(v.begin(), v.end(), x) != v.end()
					{
						//cout << count<< " NewLoc: "<<m<< endl;
						finalOutput = finalOutput+ " " +"<span style=\"color:green\">"+ l_result.ref[idx1]+  "</span>" ;
						found = true;	
					}
				}	
				if(found == false)
				{
					finalOutput = finalOutput+ " " + l_result.ref[idx1];
				}
				//finalOutput = finalOutput+ " " + l_result.ref[idx1];
                                //align[ref_A[idx1]]=shiftedHyp[idx2];
				idx1 ++;
				idx2 ++;
				count++;
                        }
			if( align.at(i) == 'S')
			{
                               
				finalOutput = finalOutput +" <strike><span style=\"color:red;\">" +l_result.aftershift[idx2]  +"</span></strike>"+" <span style=\"color:blue\">" +l_result.ref[idx1]+ "</span>" ;
				//finalOutput = finalOutput +" " + l_result.ref[idx1]+"("+l_result.aftershift[idx2]+")";
				/*if((i < align.length()) && (align.at(i+1) != 'S'))
				{
					finalOutput = finalOutput +")";
				}*/
                                //align[ref_A[idx1]]=shiftedHyp[idx2];
				idx1 ++;
				idx2 ++;
				count++;
                        }
                        if(align.at(i) == 'I')
                        {	//in the shifted hyp but not in the ref
				
				/*if((i >0)  && (align.at(i-1) != 'I'))
				{
					finalOutput = finalOutput +" (";
				}*/
			
				finalOutput = finalOutput +" <strike><span style=\"color:red;\">" +l_result.aftershift[idx2]  +"</span></strike>";
				//finalOutput = finalOutput+" (" +l_result.aftershift[idx2]+")";
				/*if((i < align.length())  && (align.at(i+1) != 'I'))
				{
					finalOutput = finalOutput +")";
				}*/
				//align["NULL"]=shiftedHyp[idx2];
				idx2 ++;
				count++;
                        }
                        if(align.at(i) == 'D')
                        {	
				// in the ref but not in the shifted hyp
				
				finalOutput = finalOutput +" <span style=\"color:orange\">" +l_result.ref[idx1]+ "</span>"; 
				//finalOutput = finalOutput +" " + l_result.ref[idx1];
				//align[ref_A[idx1]]="NULL";
				idx1 ++;
				count++;
                        }
			
			
		}
		//cout<< "DiffViwe: "+finalOutput <<endl; 
		cout<< "DiffViwe: "+finalOutput <<endl;
	}
	
	
	if(evalParameters.blockSizePercent != 0)
	{	//	cout << l_id<<endl;

		//cout<<  "Sentence Id" << "\t"<<  "Sentence TER"<<"\t"<< "# Token in the Reference" <<"\t"<< "Reference Block Size" <<"\t"<<"# Token in the MT output" <<"\t"<< "MT Output Block Size" <<"\t"<< "Block TER" <<endl;
		//string align = l_result.printAlignments();
		//cout << l_id<<endl;
		int commonWords= 0;

		//int countAlign=1;
		int idx1=0, idx2=0;
		//vector<string> shiftedHyp, ref_A;
		int count =0;
		string output="";
		string alignClean = replaceinString(align," " ,"");
		//cout << "Aligned Clean SIZE: " << alignClean.length() << endl;		
		
		int alignSize = alignClean.length();
		//if(alignSize < 25){
		//cout << l_result.hyp.size() << endl;
		if(l_result.hyp.size() <0 )
		//if(l_result.hyp.size() <25 )
		{
			//cout << "Shifted sentence too short for computing HTER at BLOCK level"<< endl;
		}
		else
		{
					
			vector<string> alignFound;

			
			//cout << "Shifted sentence of the right length for computing HTER at BLOCK level"<< endl;
			int blockSize = floor((double)alignSize * evalParameters.blockSizePercent);
			int numberBlocks = (int)ceil(((double)alignSize)/((double)blockSize));

			int blockSizeRef = floor((double)l_result.ref.size() * evalParameters.blockSizePercent);
		
			//cout << "Sentence length: " << alignSize<< " Block Size: "<< blockSize << " Number of blocks: "<< numberBlocks<< " Block Size Ref: "<< blockSizeRef << endl;
			//cout << "Number of Blocks: " << numberBlocks << endl;
			for (int bk=0; bk<numberBlocks; bk++)
			{	
				string block ="";
				//cout << "BlockId: "<<bk<<endl;
				double countErr=0;
				for(int i=bk*blockSize; i<((bk+1)*blockSize); i++){
							
					if(i >= alignSize)
						break;
					//cout << "Count id: "<<i<< " Value: " << alignClean.at(i)  <<endl;
					if(alignClean.at(i) == 'A')
					{	
						commonWords++;
					        bool found = false;
						for (int jj=0; jj < ( int ) shiftInfo.size(); jj++ )
						{	
							
							//int m = (int)shiftInfo[jj].newloc;
							/*cout << i<< " NewLoc: "<<m<< " Count: " << count << endl;
							for (int iiI=0; iiI<(shiftInfo[jj].shifted).size(); iiI++)
		    							cout<<"#"<<(shiftInfo[jj].shifted).at(iiI);
							cout<<endl; */
							//if(l_result.ref[idx1] == vectorToString(shiftInfo[jj].shifted ) && (((m-3) <count) && ((m+3) >count)) )
							/*if(( std::find((shiftInfo[jj].shifted).begin(), (shiftInfo[jj].shifted).end(), l_result.ref[idx1]) != (shiftInfo[jj].shifted).end())) && (((m-4) <count) && ((m+4) >count)) )
								for (int ii=0; ii<(shiftInfo[jj].shifted).size(); ii++)
		    							cout<<"#"<<(shiftInfo[jj].shifted).at(ii)<<endl; */
							//cout << "### " << l_result.ref[idx1] << endl; 
							//if((vectorToString(shiftInfo[jj].shifted).find(l_result.ref[idx1]) != string::npos ) && (((m-4) <count) && ((m+4) >count)) )
							
							if (idx1 < l_result.ref.size()){ 
								if(( std::find(alignFound.begin(), alignFound.end(), l_result.ref[idx1]) !=  alignFound.end())){
									//cout << "Here2"  <<endl;
									break;
								}
								else
								{	//cout << "Here3"  <<endl;
									//cout<< " Word in sentence: " << l_result.ref[idx1]  <<endl;
									if(( std::find((shiftInfo[jj].shifted).begin(), (shiftInfo[jj].shifted).end(), l_result.ref[idx1]) != (shiftInfo[jj].shifted).end())) //&& (( std::find(alignFound.begin(), alignFound.end(), l_result.ref[idx1]) ==  alignFound.end()))) 
		//&& (((m-4) <count) && ((m+4) >count)) )
		//(( std::find(alignFound.begin(), alignFound.end(), l_result.ref[idx1]) !=  alignFound.end()) == false)) 
									//&& (((m-4) <count) && ((m+4) >count)) )
									//std::find(v.begin(), v.end(), x) != v.end()
									{
										//cout << *(std::find(alignFound.begin(), alignFound.end(), l_result.ref[idx1])) << endl;
										//cout << count<< " NewLoc: "<<m<< endl;
										//finalOutput = finalOutput+ " " +"<span style=\"color:green\">"+ l_result.ref[idx1]+  "</span>" ;
										//cout <<"MATCHED WORD: "<< l_result.ref[idx1] << endl;
										//cout << "AlignSize: "<< alignFound.size()  << endl;
									
										//if(( std::find(alignFound.begin(), alignFound.end(), l_result.ref[idx1]) ==  alignFound.end()))
										alignFound.push_back(l_result.ref[idx1] );
										//cout << "##AlignSize: "<< alignFound.size() << " Elem: " << alignFound.at(alignFound.size()-1) << " Word in sentence: " << l_result.ref[idx1]  <<endl;
										found= true;
								
										break;
						
							
									}
								}
							}	
						}

						if(found == false)
						{	
							//cout << "Assigned Value: A"   <<endl;
							block = block+ 'A';	
						}else
						{
							//cout << "Here4"  <<endl;
							//cout << "Assigned Value: H"   <<endl;
							block = block+ 'H';
							//cout <<"block: " << block <<" block Size: "<<(block.size())<<endl;
							//cout << "block Pos size: "<< block.at(block.size()-1)   << endl;	
							if((block.size() > 1) && (block.at(block.size()-2)  != 'H'))
							{
								countErr++;
							
							}
							else if(block.size() ==1)
							{
								countErr++;
							}
							/*if(block.size() != 1)
								cout << "block Pos size: "<< block.at(block.size()) << "block Pos size - 1: "<< block.at(block.size())   << endl;
							if((block.size() > 1) && (block.at(block.size())  != 'H'))	*/
							//countAlign++;
							//for (int zz=0; zz<alignFound.size(); zz++)
		    							//cout<<"# Elem in AlignFound "<<alignFound.at(zz)<< " Pos: " <<zz<< endl; 
						}	
						idx1 ++;
						idx2 ++;	
						count++;
						//cout << "End A"<< endl;
					}
					if( alignClean.at(i) == 'S')
					{
					       //cout << "Assigned Value: S"   <<endl;
						block = block+ 'S';
						countErr++;
						idx1 ++;
						idx2 ++;
						count++;
						//cout << "End S"<< endl;
					}
					if(alignClean.at(i) == 'I')
					{	
						//cout << "Assigned Value: I"   <<endl;
						block = block+ 'I';
						countErr++;
						idx1 ++;
						idx2 ++;
						//cout << "End I"<< endl;
						count++;
					}
					if(alignClean.at(i) == 'D')
					{	
						//cout << "Assigned Value: D"   <<endl;
						block = block+ 'D';
						countErr++;
						idx1 ++;
						idx2 ++;
						count++;
						//cout << "End D"<< endl;
					} 
					//cout << "CountErr: "<<countErr << endl;
					//cout << "Partial Block: " << block << endl;
				}
				double terBlock = countErr/blockSizeRef;
				ostringstream os;
				os << terBlock;
				string str = os.str();
				output = output+str+"\t";
				//cout << "Block: " <<block << endl;
				//cout << "Errors: " << countErr <<" TER Block: " <<  terBlock << endl;
			}
			//cout << "Schianto qui?"<< endl;
			//cout << "All Blocks:\t" << output<<  endl;
		//cout << "REF:" << "\t"<<vectorToString(l_result.ref," ")<<endl;
		//cout << "HYP:"<< "\t"<<vectorToString(l_result.hyp," ")<<endl;

			//extract words in common
			
			
			//cout << "TER:\t"<<  l_result.scoreAv() << " " <<l_result.numEdits << " " << l_result.averageWords <<endl;
			//cout << "REF:" << "\t"<<vectorToString(l_result.ref," ")<<endl;
			//cout << "HYP:"<< "\t"<<vectorToString(l_result.hyp," ")<<endl;
			double comWordOverMTSize = ((double)commonWords/(double)l_result.hyp.size());
			double comWordOverRefSize = ((double)commonWords/(double)l_result.ref.size());
			//cout<<  l_id << "\t"<<  l_result.score()<< "\t"<< commonWords << "\t"<< l_result.ref.size() <<"\t"<< blockSizeRef <<  "\t"<<comWordOverRefSize << "\t"<<l_result.hyp.size() << "\t"<< blockSize  << "\t"<< comWordOverMTSize <<"\t"<<output <<endl;
			cout<<  l_id << "\t"<<  l_result.score()<< "\t"<< commonWords << "\t"<< l_result.ref.size() <<"\t"<< blockSizeRef <<  "\t"<<comWordOverRefSize << "\t"<<l_result.hyp.size() << "\t"<< blockSize  << "\t"<< comWordOverMTSize  <<endl;
		}	

	}
        }
	
	if ((!evalParameters.atSentenceLevel) &&(!(evalParameters.blockSizePercent != 0))){
		if (evalParameters.WER)
		{
		    cout << "Total WER: " << scoreTER ( editsResults, wordsResults );
		}
		else 
		{
		    //cout << "Total TER: " << scoreTER ( editsResults, wordsResults );
		}
	}
	char outputCharBufferTmp[200];
        outputSum << "-------------------------------------------------------------------------------------" << endl;
        sprintf ( outputCharBufferTmp, "%19s | %4d | %4d | %4d | %4d | %4d | %6.1f | %8.3f | %8.3f", "TOTAL", tot_ins, tot_del, tot_sub, tot_sft, tot_wsf, tot_err, tot_wds, tot_err*100.0 / tot_wds );
        outputSum << outputCharBufferTmp << endl;
        outputSum.close();
        
        return scoreTER(editsResults,wordsResults);

    }




	string multiEvaluation::replaceinString(string str, string tofind, string toreplace)
	{
		size_t position = 0;
		for ( position = str.find(tofind); position != string::npos; position = str.find(tofind,position) )
		{
			str.replace(position ,1, toreplace);
		}
		return(str);
	}







    void multiEvaluation::evaluate ( documentStructure& docStructReference, documentStructure& docStructhypothesis )
    {
	if (docStructReference.getSize() != docStructhypothesis.getSize())
	{
		cerr << "ERROR :  multiEvaluation::evaluate size of hypothesis and size of reference differ : " << endl << "Hypothesis size: " << docStructhypothesis.getSize() << endl << "Reference size: "<< docStructReference.getSize() <<endl << "Exit Here !!!" <<endl;
		exit(1);
	}
	if (evalParameters.debugMode)
	{
		    cerr <<"DEBUG tercpp : multiEvaluation::evaluate : launching evaluate on  "<<endl<<" references size : "<<  docStructReference.getSize() << endl << " hypothesis size : "<<  docStructhypothesis.getSize() << endl<<"END DEBUG"<<endl;
	}
	if (evalParameters.debugMode)
	{
		    cerr <<"DEBUG tercpp : multiEvaluation::evaluate : testing hypothesis "<<endl;
		    cerr <<" segId : "<<  docStructhypothesis.getSegments()->at(0).getSegId() << endl<<"END DEBUG"<<endl;
	}

        for ( vector<segmentStructure>::iterator segHypIt = docStructhypothesis.getSegments()->begin(); segHypIt != docStructhypothesis.getSegments()->end(); segHypIt++ )
        {
// 	  cerr << "************************************************************************************************************************************************************************************** 1 " << (docStructhypothesis.getSegments()->at(0)).toString()<<endl;
            terCalc * l_evalTER = new terCalc();
// 	  cerr << "************************************************************************************************************************************************************************************** 2"<<endl;
// 	  (*segHypIt).getSegId() ;
// 	  cerr << "************************************************************************************************************************************************************************************** 3"<<endl;
            segmentStructure * l_segRef = docStructReference.getSegment ( segHypIt->getSegId() );
// 	  cerr << "************************************************************************************************************************************************************************************** 4"<<endl;
// 	    exit(0);
	    terAlignment l_result;
	    if (evalParameters.WER)
	    {
		l_result = l_evalTER->WERCalculation ( segHypIt->getContent(), l_segRef->getContent());
	    }
	    else
	    {
		l_result = l_evalTER->TER ( segHypIt->getContent(), l_segRef->getContent());
	    }
// 	    terAlignment l_result = l_evalTER->TER ( segHypIt->getContent(), l_segRef->getContent());

		//cerr << "Hypothesis : "<< vectorToString(segHypIt->getContent())<<endl;
		//	cerr << "Reference : "<<	vectorToString(l_segRef->getContent())<<endl;
	    l_result.averageWords = l_segRef->getAverageLength();
	    if (l_result.averageWords==0.0)
	    {
		cerr << "ERROR : tercpp : multiEvaluation::evaluate : averageWords is equal to zero" <<endl;
		exit(0);
	    }
            l_segRef->setAlignment ( l_result );
	    if (evalParameters.debugMode)
	    {
			cerr <<"DEBUG tercpp : multiEvaluation::evaluate : testing   "<<endl<<"reference : "<<  l_segRef->getSegId() <<endl;
			cerr << "Hypothesis : "<< vectorToString(segHypIt->getContent())<<endl;
			cerr << "Reference : "<<	vectorToString(l_segRef->getContent())<<endl;
// 			cerr << "BestDoc Id : "<<  l_segRef->getBestDocId() <<endl;
			cerr << "numEdits : "<< l_result.numEdits  <<endl;
			cerr << "averageWords : "<< l_result.averageWords  <<endl;
			cerr << "score : "<<  l_result.scoreAv()  <<endl;
			cerr << "terAlignment.toString :" << l_result.toString()<<endl;
			cerr << "END DEBUG"<<endl<<endl;
	    }
	    if ((segHypIt->getAlignment().numWords == 0) && (segHypIt->getAlignment().numEdits == 0 ))
	    {
                segHypIt->setAlignment ( l_result );
                segHypIt->setBestDocId ( docStructReference.getDocId() );
	    }
	    else if ( l_result.scoreAv() < segHypIt->getAlignment().scoreAv() )
            {
                segHypIt->setAlignment ( l_result );
                segHypIt->setBestDocId ( docStructReference.getDocId() );
            }
	    if (evalParameters.debugMode)
	    {
			cerr << "DEBUG tercpp : multiEvaluation::evaluate : testing   "<<endl;
			cerr << "hypothesis : "<<  segHypIt->getSegId() <<endl;
			cerr << "hypothesis score : "<<  segHypIt->getAlignment().scoreAv() <<endl;
// 			cerr << "BestDoc Id : "<<  segHypIt->getBestDocId() <<endl;
			cerr << "new score : "<<  l_result.scoreAv()  <<endl;
			cerr << "new BestDoc Id : "<< docStructReference.getDocId()  <<endl;
			cerr << "Best Alignements : "<< l_result.printAlignments() <<endl;
			cerr << "END DEBUG"<<endl<<endl;
	    }
	    delete l_evalTER;
        }
	if (evalParameters.debugMode)
	{
		    cerr <<"DEBUG tercpp : multiEvaluation::evaluate :    "<<endl<<"End of function"<<endl<<"END DEBUG"<<endl;
	}
// 	for (incSegHypothesis=0; incSegHypothesis< getSize();incSegHypothesis++)
// 	{
// 	  docStructhypothesis->getSegments()
// 	}
    }

    float multiEvaluation::scoreTER ( vector<float> numEdits, vector<float> numWords )
    {
        vector<float>::iterator editsIt = numEdits.begin();
        vector<float>::iterator wordsIt = numWords.begin();
        if ( numWords.size() != numEdits.size() )
        {
            cerr << "ERROR : tercpp:score, diffrent size of hyp and ref" << endl;
            exit ( 0 );
        }

        double editsCount = 0.0;
        double wordsCount = 0.0;
        while ( editsIt != numEdits.end() )
        {
            editsCount += ( *editsIt );
            wordsCount += ( *wordsIt );
            editsIt++;
            wordsIt++;
        }
        float output;

        if ( ( wordsCount <= 0.0 ) && ( editsCount > 0.0 ) )
        {
            output =  1.0; // << " (" << editsCount << "/" << wordsCount << ")" << endl;
        }
        else
            if ( wordsCount <= 0.0 )
            {
                output =  0.0; // << " (" << editsCount << "/" << wordsCount << ")" << endl;
            }
            else
            {
//       return editsCount/wordsCount;
                output =  editsCount / wordsCount; // << " (" << editsCount << "/" << wordsCount << ")" << endl;
            }
        return output; //.str();
    }

    void multiEvaluation::launchSGMLEvaluation()
    {
	if (evalParameters.debugMode)
	{
		    cerr <<"DEBUG tercpp : multiEvaluation::launchSGMLEvaluation : before testing references and hypothesis size  "<<endl<<"END DEBUG"<<endl;
	}
	  
        if ( referencesSGML.getSize() == 0 )
        {
            cerr << "ERROR : multiEvaluation::launchSGMLEvaluation : there is no references" << endl;
            exit ( 0 );
        }
        if ( hypothesisSGML.getSize() == 0 )
        {
            cerr << "ERROR : multiEvaluation::launchSGMLEvaluation : there is no hypothesis" << endl;
            exit ( 0 );
        }
	if (evalParameters.debugMode)
	{
		    cerr <<"DEBUG tercpp : multiEvaluation::launchSGMLEvaluation : testing references and hypothesis size  "<<endl<<" references size : "<<  referencesSGML.getSize() << endl << " hypothesis size : "<<  hypothesisSGML.getSize() << endl<<"END DEBUG"<<endl;
	}
	  
        int incDocRefences = 0;
        int incDocHypothesis = 0;
        stringstream l_stream;
        vector<float> editsResults;
        vector<float> wordsResults;
        int tot_ins = 0;
        int tot_del = 0;
        int tot_sub = 0;
        int tot_sft = 0;
        int tot_wsf = 0;
        float tot_err = 0;
        float tot_wds = 0;
//         vector<stringInfosHasher> setOfHypothesis = hashHypothesis.getHashMap();
        ofstream outputSum ( ( evalParameters.hypothesisFile + ".output.sum.log" ).c_str() );
        outputSum << "Hypothesis File: " + evalParameters.hypothesisFile + "\nReference File: " + evalParameters.referenceFile + "\n" + "Ave-Reference File: " << endl;
        char outputCharBuffer[200];
        sprintf ( outputCharBuffer, "%19s | %4s | %4s | %4s | %4s | %4s | %6s | %8s | %8s", "Sent Id", "Ins", "Del", "Sub", "Shft", "WdSh", "NumEr", "AvNumWd", "TER");
        outputSum << outputCharBuffer << endl;
        outputSum << "-------------------------------------------------------------------------------------" << endl;
        for ( incDocHypothesis = 0; incDocHypothesis < hypothesisSGML.getSize();incDocHypothesis++ )
        {
	    documentStructure l_hypothesis = (*(hypothesisSGML.getDocument ( incDocHypothesis)));
	    vector<documentStructure> l_reference = (referencesSGML.getDocumentCollection ( l_hypothesis.getDocId() ));
	    
	    for ( incDocRefences = 0; incDocRefences < (int)l_reference.size();incDocRefences++ )
	    {
		
// 		cerr << "******************************************************** DEB : "<< incDocRefences <<endl;
		documentStructure l_l_references = l_reference.at(incDocRefences);
		evaluate ( l_l_references, l_hypothesis );
// 		cerr << "******************************************************** FIN : "<< incDocRefences <<endl;
	    }
	    for ( vector<segmentStructure>::iterator segHypIt = l_hypothesis.getSegments()->begin(); segHypIt != l_hypothesis.getSegments()->end(); segHypIt++ )
	    {
		terAlignment l_result = segHypIt->getAlignment();
		string bestDocId = segHypIt->getBestDocId();
		string l_id=segHypIt->getSegId();
		editsResults.push_back(l_result.numEdits);
		wordsResults.push_back(l_result.averageWords);
		l_result.scoreDetails();
		tot_ins += l_result.numIns;
		tot_del += l_result.numDel;
		tot_sub += l_result.numSub;
		tot_sft += l_result.numSft;
		tot_wsf += l_result.numWsf;
		tot_err += l_result.numEdits;
		tot_wds += l_result.averageWords;

		char outputCharBufferTmp[200];
		sprintf(outputCharBufferTmp, "%19s | %4d | %4d | %4d | %4d | %4d | %6.1f | %8.3f | %8.3f",(l_id+":"+bestDocId).c_str(), l_result.numIns, l_result.numDel, l_result.numSub, l_result.numSft, l_result.numWsf, l_result.numEdits, l_result.averageWords, l_result.scoreAv()*100.0);
		outputSum<< outputCharBufferTmp<<endl;

// 		if (evalParameters.debugMode)
// 		{
// 		    cerr <<"DEBUG tercpp : multiEvaluation::launchSGMLEvaluation : Evaluation "<<endl<< l_result.toString() <<endl<<"END DEBUG"<<endl;
// 		}

	    }
	    
	}
//         for ( incDocRefences = 0; incDocRefences < referencesSGML.getSize();incDocRefences++ )
//         {
//             l_stream.str ( "" );
//             l_stream << incDocRefences;
//             documentStructure l_reference = (*(referencesSGML.getDocument ( l_stream.str() )));
//             evaluate ( l_reference, hypothesisSGML );
//         }

	if (evalParameters.WER)
	{
	    cout << "Total WER: " << scoreTER ( editsResults, wordsResults );
	}
	else
	{
	    cout << "Total TER: " << scoreTER ( editsResults, wordsResults );
	}
	char outputCharBufferTmp[200];
        outputSum << "-------------------------------------------------------------------------------------" << endl;
        sprintf ( outputCharBufferTmp, "%19s | %4d | %4d | %4d | %4d | %4d | %6.1f | %8.3f | %8.3f", "TOTAL", tot_ins, tot_del, tot_sub, tot_sft, tot_wsf, tot_err, tot_wds, tot_err*100.0 / tot_wds );
        outputSum << outputCharBufferTmp << endl;
        outputSum.close();


    }
void multiEvaluation::addSGMLReferences()
{
      if (evalParameters.debugMode)
      {
	  cerr <<"DEBUG tercpp : multiEvaluation::addSGMLReferences "<<endl<<"END DEBUG"<<endl;
      }
      xmlStructure refStruct;
      referencesSGML.setParams(evalParameters);
      refStruct.xmlParams=copyParam(evalParameters);
      referencesSGML=refStruct.dump_to_SGMLDocument(evalParameters.referenceFile);
      referencesSGML.setAverageLength();
      if (evalParameters.debugMode)
      {
	  cerr <<"DEBUG tercpp : multiEvaluation::addSGMLReferences Reference Content :"<<endl;
	  cerr << referencesSGML.toString()<<endl<<"END DEBUG"<<endl;
      }
      
}
void multiEvaluation::setSGMLHypothesis()
{
      if (evalParameters.debugMode)
      {
	  cerr <<"DEBUG tercpp : multiEvaluation::setSGMLHypothesis "<<endl<<"END DEBUG"<<endl;
      }
//       SGMLDocument sgmlHyp;
      xmlStructure hypStruct;
      hypStruct.xmlParams=copyParam(evalParameters);
      hypStruct.xmlParams.tercomLike=false;
      hypothesisSGML.setParams(evalParameters);
      hypothesisSGML=hypStruct.dump_to_SGMLDocument(evalParameters.hypothesisFile);
      if (evalParameters.debugMode)
      {
	  cerr <<"DEBUG tercpp : multiEvaluation::setSGMLHypothesis Hypothesis Content :"<<endl;
	  cerr << hypothesisSGML.toString()<<endl<<"END DEBUG"<<endl;
	  cerr <<"DEBUG tercpp : LOAD FINISHED "<<endl<<"END DEBUG"<<endl;
      }
//       hypothesisSGML=(*(sgmlHyp.getFirstDocument()));
}

}