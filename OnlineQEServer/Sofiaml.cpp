/* 
 * File:   Sofiaml.cpp
 * Author: anastas
 * 
 * Created on July 11, 2013, 5:15 PM
 */

#include "Sofiaml.h"
#include <assert.h>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

#include "helping.h"
extern int DebugMode;
extern string LogFileName;

Sofiaml::Sofiaml(LearnerType learner_type, int dimensions) {
     if (DebugMode){ stringstream temp; temp << "Sofiaml::Sofiaml(LearnerType learner_type, int dimensions) start\n"; Write2Log(LogFileName, temp.str()); }
    this->learner_type = learner_type;
    this->eta_type = CONSTANT;
    this->lambda = 0.1;
    this->c = 0.01;
    this->epsilon = 0.01;
    this->createObjects(dimensions);
     if (DebugMode){ stringstream temp; temp << "Sofiaml::Sofiaml(LearnerType learner_type, int dimensions) end\n"; Write2Log(LogFileName, temp.str()); }
}

Sofiaml::Sofiaml(LearnerType learner_type, int dimensions, EtaType etatype, float lambda, float c, float epsilon){
    if (DebugMode){ stringstream temp; temp << "Sofiaml::Sofiaml(LearnerType learner_type, int dimensions, EtaType etatype, float lambda, float c, float epsilon) end\n"; Write2Log(LogFileName, temp.str()); }
    this->learner_type = learner_type;
    this->eta_type = etatype;
    this->lambda = lambda;
    this->c = c;          
    this->epsilon = epsilon;
    this->createObjects(dimensions);
    if (DebugMode){ stringstream temp; temp << "Sofiaml::Sofiaml(LearnerType learner_type, int dimensions, EtaType etatype, float lambda, float c, float epsilon) end\n"; Write2Log(LogFileName, temp.str()); }
}

void Sofiaml::createObjects(int dimensions){
    this->w = new SfWeightVector(dimensions);
}

void Sofiaml::destroyObjects(){
    delete this->w;
}

void Sofiaml::SetLearnerType(LearnerType learner_type){
    this->learner_type = learner_type;
}

LearnerType Sofiaml::GetLearnerType(){
    return this->learner_type;
}

void Sofiaml::SetEtaType(EtaType etatype){
    this->eta_type = etatype;
}

EtaType Sofiaml::GetEtaType(){
    return this->eta_type;
}
 
void Sofiaml::SetEta(float eta){
    this->eta = eta;
}

float Sofiaml::GetEta(){
    return this->eta;
}

float Sofiaml::GetEta(EtaType eta_type, float lambda, int i) {
    switch (eta_type) {
    case BASIC_ETA:
      return 10.0 / (i + 10.0);
      break;
    case PEGASOS_ETA:
      return 1.0 / (lambda * i);
      break;
    case CONSTANT:
      return 0.02;
      break;
    default:
      std::cerr << "EtaType " << eta_type << " not supported." << std::endl;
      exit(0);
    }
    std::cerr << "Error in GetEta, we should never get here." << std::endl;
    return 0;
}

void Sofiaml::SetLambda(float lambda){
    this->lambda = lambda;
}

float Sofiaml::GetLambda(){
    return this->lambda;
}

void Sofiaml::SetC(float c){
    this->c = c;
}
 
float Sofiaml::GetC(){
    return this->c;
}

void Sofiaml::SetEpsilon(float epsilon){
    this->epsilon = epsilon;
}

float Sofiaml::GetEpsilon(){
    return this->epsilon;
}

std::string Sofiaml::GetWeights(){
    std::stringstream temp;
    int i;
    for (i=0; i < this->w->GetDimensions(); i++)
	temp << this->w->ValueOf(i) << " ";
    temp << std::endl;
    std::string str;
    str = temp.str();
    return str;
}

//void Sofiaml::doNothing(char* filename){
    
//}

Sofiaml::~Sofiaml() {
    if (DebugMode){ stringstream temp; temp << "Sofiaml::~Sofiaml() start\n"; Write2Log(LogFileName, temp.str()); }
    this->destroyObjects();
    if (DebugMode){ stringstream temp; temp << "Sofiaml::~Sofiaml() end\n"; Write2Log(LogFileName, temp.str()); }
 
}

void Sofiaml::SaveModelToFile(const std::string& file_name, SfWeightVector* w) {
  std::fstream model_stream;
  model_stream.open(file_name.c_str(), std::fstream::out);
  if (!model_stream) {
    std::cerr << "Error opening model output file " << file_name << std::endl;
    exit(1);
  }
  std::cerr << "Writing model to: " << file_name << std::endl;
  model_stream << w->AsString() << std::endl;
  model_stream.close();
  std::cerr << "   Done." << std::endl;
}

void Sofiaml::LoadModelFromFile(const std::string& file_name, SfWeightVector** w) {
  if (*w != NULL) {
    delete *w;
  }

  std::fstream model_stream;
  model_stream.open(file_name.c_str(), std::fstream::in);
  if (!model_stream) {
    std::cerr << "Error opening model input file " << file_name << std::endl;
    exit(1);
  }

  std::cerr << "Reading model from: " << file_name << std::endl;
  std::string model_string;
  std::getline(model_stream, model_string);
  model_stream.close();
  std::cerr << "   Done." << std::endl;

  *w = new SfWeightVector(model_string);
  assert(*w != NULL);
}

