/* 
 * File:   TrainInstance.h
 * Author: anastas
 *
 * Created on July 4, 2013, 2:21 PM
 */

#include <string>
#include <map>
#include <utility>

#ifndef TRAININSTANCE_H
#define	TRAININSTANCE_H

//Template for the map table for the features
template < class Key,                                     // map::key_type
           class T,                                       // map::mapped_type
           class Compare = std::less<Key>,                     // map::key_compare
           class Alloc = std::allocator<std::pair<const Key,T> >    // map::allocator_type
> class map;


class TrainInstance {
public:
    TrainInstance(int id, std::string source, std::string target);
    //TrainInstance(const TrainInstance& orig);
    virtual ~TrainInstance();
    
    void setID(int i);
    int getID();
    
    void setSource(std::string s);
    std::string getSource();
    
    void setTarget(std::string s);
    std::string getTarget();
    
    void Write2Log(std::string LogFileName, std::string s);
    
    //void setFeatures(std::string FeatureString);
    void setFeatures(int FeatureNo, std::string Featuretring);
    //void setFeatures(std::map<int, float> FeatureVals);
    std::map<int, float> getFeatures();
    std::string getFeaturesstr();
    //std::string printFeatures();
    
    void setHTER_Prediction(float pred);
    float getHTER_Prediction();
    
    void setPostEditorID(int a);
    int getPostEditorID();
    
    void setPostEdited(std::string s);
    std::string getPostEdited();
    
    void setHTER(float hter);
    float getHTER();
    
    void setSeen(bool s);
    bool getSeen();
    
private:
    int id;
    std::string src;
    std::string trg;
    std::map<int, float> FeatureVals;
    float hterPrediction;
    int PostEditorID;
    std::string pe;
    float HTER;
    bool seen;
};

typedef TrainInstance * TrainInstancePtr;

#endif	/* TRAININSTANCE_H */

