/* 
 * File:   tinyxmlerror.h
 * Author: anastas
 *
 * Created on July 8, 2013, 11:09 AM
 */

#ifndef TINYXMLERROR_H
#define	TINYXMLERROR_H

class tinyxmlerror {
public:
    tinyxmlerror();
    tinyxmlerror(const tinyxmlerror& orig);
    virtual ~tinyxmlerror();
private:

};

#endif	/* TINYXMLERROR_H */

