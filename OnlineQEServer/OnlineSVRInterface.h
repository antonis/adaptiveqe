/* 
 * File:   OnlineSVRInterface.h
 * Author: anastas
 *
 * Created on July 17, 2013, 10:42 AM
 */

#ifndef ONLINESVRINTERFACE_H
#define	ONLINESVRINTERFACE_H

#include "OnlineSVR.h" // Needed for OnlineSVR
#include "Sofiaml.h" // Needed for all Sofiaml Algorithms
#include "SofiaInterface.h"
#include <iostream>
#include <map>
#include <cstring>
#include <sstream> 
using namespace std;
using namespace onlinesvr; 

extern int DebugMode;
extern string LogFileName;

class OnlineSVRAdapter : public OnlineSVR {
public:
    OnlineSVRAdapter():OnlineSVR(){
	if (DebugMode){ stringstream temp; temp << "OnlineSVRAdapter::OnlineSVRAdapter()" << endl; Write2Log(LogFileName,temp.str()); }
}
    //OnlineSVRAdapter(const OnlineSVRAdapter& orig);

    ~OnlineSVRAdapter() {
	if (DebugMode){ stringstream temp; temp << "OnlineSVRAdapter::~OnlineSVRAdapter()" << endl; Write2Log(LogFileName,temp.str()); }
} 

//    virtual ~OnlineSVRAdapter() { if (DebugMode){ stringstream temp; temp << "OnlineSVRAdapter::~OnlineSVRAdapter()" << endl; Write2Log(LogFileName,temp.str()); } } 
    
    Vector<double>* means;// = new Vector<double>();
    Vector<double>* stdev;// = new Vector<double>();
    int FeaturesNo;
    int NoOfPreComputed;
    int CurrentInstances;
    
/*
    string LogFileName; 
    int DebugMode;
   */
 
    void initLog(string LogF, int DebugM){
        LogFileName = LogF;
        DebugMode = DebugM;
    }

    void clearMeansStDev(){
        delete means;
        delete stdev;
    }
 
    void initMeansStDev(int featureNo){
        means = new Vector<double>();
        stdev = new Vector<double>();
        FeaturesNo = featureNo;
        NoOfPreComputed = 0;
        CurrentInstances = 0;
        for (int i=0; i<featureNo; i++){
            means->Add(0.0);
            stdev->Add(0.0);
        }
    }
    
   float myPredict(Matrix<double>* Feats, std::map<int, float> FeatDict){
        //Matrix<double>* TestX = new Matrix<double>();
        Vector<double>* TestX = new Vector<double>();
        for( std::map<int,float>::iterator j=FeatDict.begin(); j!=FeatDict.end(); ++j){
             TestX->Add((*j).second);
        }
        
        // Normalise the Features - Is it NEEDED?!? yes
        if (Feats->GetLengthRows() > 0){
            //cout << "I'm in!!" << endl;
            for (int i = 0; i < TestX->GetLength(); i++){
                if (stdev->GetValue(i) != 0){
                    double Val = (TestX->Values[i] - means->GetValue(i))/(stdev->GetValue(i));
                    //double Val = (TestX->Values[i] - means->GetValue(i))/(3*stdev->GetValue(i));
                    TestX->SetValue(i,Val);                    
                }
                else
                    TestX->SetValue(i,0.0);
            }
            
        }
        
        float ans = this->OnlineSVR::Predict(TestX);
        if (ans < 0.0) ans = 0.0;
        else if (ans > 1.0) ans = 1.0;
        stringstream temp;
        if(DebugMode){
            temp << "OnlineSVR:: Predict:: Predicted value: " << ans;
            Write2Log(LogFileName, temp.str());
            temp.str( std::string() );
            temp.clear();
        }
        return ans;
    }

    
 
    
    void myTrain(Matrix<double>* Feats, Vector<double>* Labels, int CurrentIt, int LoadModelFromFile){
        stringstream temp;
        if(DebugMode){
            temp << "OnlineSVR:: Train:: Got in here ";
            Write2Log(LogFileName, temp.str());
            temp.str( std::string() );
            temp.clear();
        }
        int dim = Feats->GetLengthCols();
        this->CurrentInstances = Labels->GetLength() + NoOfPreComputed;
        int N = CurrentInstances;
        
        if(DebugMode){
            stringstream temp4;
            temp4 << "OnlineSVR:: Train:: Found N = " << N << " and dim = " << dim;
            Write2Log(LogFileName, temp4.str());
        
        }
        // Compute Normalisation parameters for each row
        if(LoadModelFromFile == 0){
            if (N == 1)  {
                
                stringstream te;
                if(DebugMode){
                  Write2Log(LogFileName, "OnlineSVR:: Train:: Will instantiate means and stdev with the first instance");
                }
                for (int i=0; i<dim; i++){
                    // What is the reason of using this method GetColCopy?
                    //Vector<double>* Temp = Feats->GetColCopy(i);
                    stdev->SetValue(i, 0.0);
                    //means->SetValue(i, Temp->GetValue(i));
                    means->SetValue(i, Feats->GetValue(0,i));
                    te<< means->GetValue(i)<<" ";
                    //te<< Feats->GetValue(0, i)<< " ";
                }
                if(DebugMode){
                        Write2Log(LogFileName, te.str());
                }
            } 
            else{
                stringstream tempST;
                stringstream tempM;
                stringstream oldST;
                stringstream oldM;
                //tempST<< N<<" ";
                for(int kk =0; kk < means->GetLength(); kk++){
                    oldM << means->GetValue(kk)<< " ";
                }
                if(DebugMode){
                 Write2Log(LogFileName, oldM.str());
                }

                for(int kk =0; kk < means->GetLength(); kk++){
                    oldST << stdev->GetValue(kk)<< " ";
                }
                if(DebugMode){
                    Write2Log(LogFileName, oldST.str());

                    Write2Log(LogFileName, "OnlineSVR:: Train:: Updating means and stdevs for the features");
                }
                for (int i = 0; i < dim; i++){
                    double newmean = ((N-1)*means->GetValue(i) + Feats->GetValue(N-1,i))/((double) N);
                    double newvar = ((((double)N)-2)/(((double) N)-1)) *(stdev->GetValue(i) * stdev->GetValue(i)) + (1.0/((double) N)) * (Feats->GetValue(N-1, i) -  means->GetValue(i)) * (Feats->GetValue(N-1, i) -  means->GetValue(i));

                    stdev->SetValue(i, sqrt(newvar));
                    means->SetValue(i, newmean);
                    tempST << sqrt(newvar) << " ";
                    tempM << newmean << " ";

                }
                if(DebugMode){
                        Write2Log(LogFileName, tempM.str());
                        Write2Log(LogFileName, tempST.str());
                }
            }
        }
        else{
            stringstream tempST;
            stringstream tempM;
            stringstream oldST;
            stringstream oldM;
            //tempST<< "Old Features: ";
            for(int kk =0; kk < means->GetLength(); kk++){
                oldM << means->GetValue(kk)<< " ";
            }
            
            if(DebugMode){
                Write2Log(LogFileName, oldM.str());
            }
            for(int kk =0; kk < means->GetLength(); kk++){
               oldST << stdev->GetValue(kk)<< " ";
            }
            if(DebugMode){
                Write2Log(LogFileName, oldST.str());
            
            Write2Log(LogFileName, "OnlineSVR:: Train:: Updating means and stdevs for the features");
            }
            for (int i = 0; i < dim; i++){
                double newmean = ((N-1)*means->GetValue(i) + Feats->GetValue(Labels->GetLength()-1,i))/((double) N);
                double newvar = ((((double)N)-2)/(((double) N)-1)) *(stdev->GetValue(i) * stdev->GetValue(i)) + (1.0/((double) N)) * (Feats->GetValue((Labels->GetLength()-1), i) -  means->GetValue(i)) * (Feats->GetValue((Labels->GetLength()-1), i) -  means->GetValue(i));
                stdev->SetValue(i, sqrt(newvar));
                means->SetValue(i, newmean);
                tempST << sqrt(newvar) << " ";
                tempM << newmean << " ";
                //tempM << Feats->GetValue(N-2, i) << " ";
            }
            if(DebugMode){
                Write2Log(LogFileName, tempM.str());
                Write2Log(LogFileName, tempST.str());
            }
        }
        
        //Normalisation with Mean and Variance
        stringstream tempNormF;
        double Label = Labels->GetValue(Labels->GetLength()-1);
        Vector<double>* TrainX = Feats->GetRowCopy(Feats->GetLengthRows()-1);
        for (int i = 0; i < TrainX->GetLength(); i++){
            if (stdev->GetValue(i) != 0)
                //TrainX->SetValue(i, (TrainX->Values[i]- means->GetValue(i))/(3*stdev->GetValue(i)));
                TrainX->SetValue(i, (TrainX->Values[i]- means->GetValue(i))/(stdev->GetValue(i)));
            else
                TrainX->SetValue(i,0.0);
            tempNormF << TrainX->GetValue(i)<< " ";
        }
        if(DebugMode){
            Write2Log(LogFileName, tempNormF.str());
            temp << "OnlineSVR:: Train:: Normalised correctly ";
            Write2Log(LogFileName, temp.str());
            temp.str( std::string() );
            temp.clear();
        }
        
        this->OnlineSVR::Train(TrainX, Label);
        delete TrainX;
        //this->OnlineSVR::Train(Feats, Labels);
    }
    
    void myLoad(char* filename){
        this->LoadOnlineSVR(filename);
        int Dimension = this->X->GetLengthCols();
        FeaturesNo = Dimension;
        NoOfPreComputed = this->X->GetLengthRows();
        for (int i = 0; i < Dimension; i++){
            Vector<double>* Temp = X->GetColCopy(i);
            stdev->SetValue(i, Temp->StDev());
            means->SetValue(i, Temp->Mean());
            delete Temp;
        }
        
    }
    
    
    
    Vector<double>* myMargin(onlinesvr::Matrix<double>* Feats, Vector<double>* Labels){
        int dim = Feats->GetLengthCols();
        int N = Feats->GetLengthRows();
         Vector<double> Answer(N);
        //Normalisation with Mean and Variance

          if(DebugMode){
                stringstream t;
                t<< "Matrix dimension ";
                t << N << " "<< dim;
                t << "\n";
                Write2Log(LogFileName, t.str());
                t.clear();
         }
         
         
        for (int j = 0; j<N; j++){
                double Label = Labels->GetValue(j);
                Vector<double>* TestX = Feats->GetRowCopy(j);
                
                if(DebugMode){
                        stringstream t;
                        t<< "Row: ";
                        t << j <<endl;
                        
                        Write2Log(LogFileName, t.str());
                        t.clear();
                }
                
                stringstream tempNormF;
                tempNormF << "OnlineSVR:: MyMargin:: Normalised ";
                for (int i = 0; i < TestX->GetLength(); i++){
                    if (stdev->GetValue(i) != 0)
                            //TrainX->SetValue(i, (TrainX->Values[i]- means->GetValue(i))/(3*stdev->GetValue(i)));
                            TestX->SetValue(i, (TestX->Values[i]- means->GetValue(i))/(stdev->GetValue(i)));
                    else{
                            TestX->SetValue(i,0.0);
                            
                    }
                    tempNormF << TestX->GetValue(i)<< " ";
                }
                
                if(DebugMode){
                        tempNormF << "\n";
                        Write2Log(LogFileName, tempNormF.str());
                        tempNormF.clear();
                }
                
                float ans = this->OnlineSVR::Predict(TestX);
                if (ans < 0.0) ans = 0.0;
                else if (ans > 1.0) ans = 1.0;
                
                stringstream temp;
                temp << "OnlineSVR:: MyMargin:: Prediction and Margin: ";
                temp<< ans <<" ";
                
                double m = ans - Label;
                Answer.AddAt(m, j);
                
                temp << m<< " ";
                if(DebugMode){
                    
                    temp<<"\n";
                    Write2Log(LogFileName, temp.str());
                    
                    temp.clear();
                }
                //delete temp;
                //delete tempNormF;
                delete TestX; 
  
        }
        
          
        return Answer.Clone();
    }
    
    
    void myPrintWeights(char* WeightsFileName){
        //int i;
        //for (i=1; i < this->w->GetDimensions(); i++)
        //        temp << this->w->ValueOf(i) << " ";
        //temp << std::endl;
        //std::string str;
        //str = temp.str();
        //return str;
        //WeightsFileName = "../Weights/Exp1.weights";
        ofstream WeightsFile;
        WeightsFile.open(WeightsFileName, ios::app);
        if (WeightsFile.is_open()) {
            WeightsFile << " problem " << endl;
        }
        WeightsFile.close();
    }//void StoreWeights(char *filename){
        
    //}
    
    void doNothing0(){}
    void doNothing1(double i){}
    void doNothing5(int i){}
    void doNothing6(EtaType e){}
    void doNothing7(float f){}
    float doNothing2(){}
    int doNothing3(){}
    double doNothing4(){}
    EtaType doNothing8(){}
    float doNothing9(Matrix<double>* Feats, std::map<int, float> FeatDict){}

    
  /*  void Write2Log(string LogFileName, string s){
        ofstream LogFile;
        LogFile.open(LogFileName.c_str(), ios::app);
        if (LogFile.is_open()) {
            LogFile <<  s << endl;
            LogFile.close();
        }
    } */

private:

};



#endif	/* ONLINESVRINTERFACE_H */

