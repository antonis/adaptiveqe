/* 
 * File:   SofiaInterface.h
 * Author: anastas
 *
 * Created on July 17, 2013, 10:15 AM
 */

#ifndef SOFIAINTERFACE_H
#define	SOFIAINTERFACE_H

#include "OnlineSVR.h" // Needed for OnlineSVR
#include "Sofiaml.h" // Needed for all Sofiaml Algorithms
#include <iostream>
#include <map>
#include <cstring>
#include <sstream> 
#include "helping.h"
using namespace std;
using namespace onlinesvr; 

extern int DebugMode;
extern string LogFileName;

class SofiaPassiveAggressiveAdapter : public Sofiaml {
public:
    SofiaPassiveAggressiveAdapter(int dimensions) : Sofiaml(PASSIVE_AGGRESSIVE,dimensions){
        if(DebugMode){ 	stringstream temp; temp << "SofiaPassiveAggressiveAdapter::SofiaPassiveAggressiveAdapter()\n"; Write2Log(LogFileName, temp.str()); }
    }
    //SofiaPegasosAdapter(int dimensions) : Sofiaml(SGD_SVM,dimensions){}
    //virtual ~SofiaPassiveAggressiveAdapter(){}
    ~SofiaPassiveAggressiveAdapter(){
        if(DebugMode){ 	stringstream temp; temp << "SofiaPassiveAggressiveAdapter::~SofiaPassiveAggressiveAdapter()\n"; Write2Log(LogFileName, temp.str()); }
    }
    
    Vector<double>* means;// = new Vector<double>();
    Vector<double>* stdev;// = new Vector<double>();
    int FeaturesNo;
    int NoOfPreComputed;
    int CurrentInstances;
    
/*    
    string LogFileName; 
    int DebugMode;
*/    
    void initLog(string LogF, int DebugM){
      LogFileName = LogF;
      DebugMode = DebugM;      
    }
   
    void clearMeansStDev(){
        delete means;
        delete stdev;
    }
 
    void initMeansStDev(int featureNo){
        FeaturesNo = featureNo;
        NoOfPreComputed = 0;
        CurrentInstances = 0;
        means = new Vector<double>();
        stdev = new Vector<double>();
        for (int i=0; i<featureNo; i++){
            means->Add(0.0);
            stdev->Add(0.0);
        }
    }

    
    void myTrain(Matrix<double>* Feats, Vector<double>* Labels, int currentIt, int LoadModelFromFile){
         stringstream temp;
        if(DebugMode){
               
                temp << "PA:: Train:: Got in here ";
                Write2Log(LogFileName, temp.str());
                temp.str( std::string() );
                temp.clear();
        }
        int dim = Feats->GetLengthCols();
        this->CurrentInstances = Labels->GetLength() + NoOfPreComputed;
                
        int N = CurrentInstances;
        if(DebugMode){
                stringstream temp4;
                temp4 << "PA:: Train:: Found N = " << N << " and dim = " << dim;
                //Write2Log("./QE.log", temp4.str());
                Write2Log(LogFileName, temp4.str());
        }
        // Compute Normalisation parameters for each row
        // if LoadModelFromFile == 0: we are in the classic training situation where there is not a pre-trained model
        if(LoadModelFromFile == 0){
                if (N == 1)  {
                    stringstream te;
                    if(DebugMode){
                        
                        Write2Log(LogFileName, "PA:: Train:: Will instantiate means and stdev with the first instance");
                    }
                    for (int i=0; i<dim; i++){
                        // What is the reason of using this method GetColCopy?
                        //Vector<double>* Temp = Feats->GetColCopy(i);
                        stdev->SetValue(i, 0.0);
                        //means->SetValue(i, Temp->GetValue(i));
                        means->SetValue(i, Feats->GetValue(0,i));
                        te<< means->GetValue(i)<<" ";
                        //te<< Feats->GetValue(0, i)<< " ";
                    }
                    if(DebugMode){
                        Write2Log(LogFileName, te.str());
                    }
                } 
                 else{
                     stringstream tempST;
                     stringstream tempM;
                     stringstream oldST;
                     stringstream oldM;
                     //tempST<< N<<" ";
                     for(int kk =0; kk < means->GetLength(); kk++){

                        oldM << means->GetValue(kk)<< " ";
                     }
                     if(DebugMode){
                        Write2Log(LogFileName, oldM.str());
                     }
                     for(int kk =0; kk < means->GetLength(); kk++){

                        oldST << stdev->GetValue(kk)<< " ";
                     }
                     if(DebugMode){
                        Write2Log(LogFileName, oldST.str());

                        Write2Log(LogFileName, "PA:: Train:: Updating means and stdevs for the features");
                     }
                    for (int i = 0; i < dim; i++){
                        double newmean = ((N-1)*means->GetValue(i) + Feats->GetValue(N-1,i))/((double) N);
                        //Old wrong way to compute mean and std
                        //double newmean = (means->GetValue(i) +( Feats->GetValue(N-1,i) - means->GetValue(i))/((double) N));
                       // double newvar = ((N-1)*stdev->GetValue(i) + N*(means->GetValue(i) - newmean)*(means->GetValue(i) - newmean) + (Feats->GetValue(N-1, i) - newmean)*(Feats->GetValue(N-1, i) - newmean))/((double) N);

                        //double newvar = ((N-1)*(stdev->GetValue(i)) + N*(means->GetValue(i) - newmean)*(means->GetValue(i) - newmean) + (Feats->GetValue(N-1, i) - newmean)*(Feats->GetValue(N-1, i) - newmean))/((double) N);

                        double newvar = ((((double)N)-2)/(((double) N)-1)) *(stdev->GetValue(i) * stdev->GetValue(i)) + (1.0/((double) N)) * (Feats->GetValue(N-1, i) -  means->GetValue(i)) * (Feats->GetValue(N-1, i) -  means->GetValue(i));

                        stdev->SetValue(i, sqrt(newvar));
                        means->SetValue(i, newmean);
                        tempST << sqrt(newvar) << " ";
                        tempM << newmean << " ";
                        //tempM << Feats->GetValue(N-2, i) << " ";
                    }
                    if(DebugMode){
                         Write2Log(LogFileName, tempM.str());

                        Write2Log(LogFileName, tempST.str());
                    }
                 }
        }else{
            //TO BE DONE!!!
            // LoadModelFromFile == 1 so we are training over a pre-trained model. This means that the initial N is not equal to 1 but is the sum of the points used to pre-trained the model plus 1. 
            //So what we need to do:
            // 1) change the information into the saved model, it should contain the total number of point used to build the model
            // 2) Load this new information
            // 3) Update the value of N to N = NumberPoints used in the past
            // 4) test that the final mean and std are similar to those computed concatenating the data used to pre-trained the model and the new points
            
            // If we have the updated value of N, I guess we do not need the if for  N==1 and we can run the same code as above
                stringstream tempST;
                stringstream tempM;
                stringstream oldST;
                stringstream oldM;
                //tempST<< "Old Features: ";
                for(int kk =0; kk < means->GetLength(); kk++){

                   oldM << means->GetValue(kk)<< " ";
                }
                if(DebugMode){
                        Write2Log(LogFileName, oldM.str());
                }
                for(int kk =0; kk < means->GetLength(); kk++){

                   oldST << stdev->GetValue(kk)<< " ";
                }
                if(DebugMode){
                        Write2Log(LogFileName, oldST.str());

                        Write2Log(LogFileName, "PA:: Train:: Updating means and stdevs for the features");
                }
               for (int i = 0; i < dim; i++){

                   double newmean = ((N-1)*means->GetValue(i) + Feats->GetValue(Labels->GetLength()-1,i))/((double) N);
                  // double newmean = ((N-1)*means->GetValue(i) + Feats->GetValue(N-1,i))/((double) N);
                   //Old wrong way to compute mean and std
                   //double newmean = (means->GetValue(i) +( Feats->GetValue(N-1,i) - means->GetValue(i))/((double) N));
                  // double newvar = ((N-1)*stdev->GetValue(i) + N*(means->GetValue(i) - newmean)*(means->GetValue(i) - newmean) + (Feats->GetValue(N-1, i) - newmean)*(Feats->GetValue(N-1, i) - newmean))/((double) N);

                   //double newvar = ((N-1)*(stdev->GetValue(i)) + N*(means->GetValue(i) - newmean)*(means->GetValue(i) - newmean) + (Feats->GetValue(N-1, i) - newmean)*(Feats->GetValue(N-1, i) - newmean))/((double) N);

                   //double newvar = ((((double)N)-1)/((double) N)) *(stdev->GetValue(i) * stdev->GetValue(i)) + (1/((double) N)) * (Feats->GetValue(N-1, i) -  means->GetValue(i)) * (Feats->GetValue(N-1, i) -  means->GetValue(i));

                   double newvar = ((((double)N)-2)/(((double) N)-1)) *(stdev->GetValue(i) * stdev->GetValue(i)) + (1.0/((double) N)) * (Feats->GetValue((Labels->GetLength()-1), i) -  means->GetValue(i)) * (Feats->GetValue((Labels->GetLength()-1), i) -  means->GetValue(i));


                   stdev->SetValue(i, sqrt(newvar));
                   means->SetValue(i, newmean);
                   tempST << sqrt(newvar) << " ";
                   tempM << newmean << " ";
                   //tempM << Feats->GetValue(N-2, i) << " ";
               }
                if(DebugMode){
                        Write2Log(LogFileName, tempM.str());

                         Write2Log(LogFileName, tempST.str());
                }
 
        }
    
        
         

        //if (Feats->GetLengthRows() > 3){
        if (Feats->GetLengthRows() > 0){
            temp << "PA:: Train:: instances = " << Feats->GetLengthRows();
            if(DebugMode){
                Write2Log(LogFileName, temp.str());
                temp.str( std::string() );
                temp.clear();
            }
            float Label = Labels->GetValue(Labels->GetLength()-1);
            temp << "PA:: Train:: Got Labels ";
            if(DebugMode){
                Write2Log(LogFileName, temp.str());
                temp.str( std::string() );
                temp.clear();
            }
            //cout << "the label to train is " << Label << endl;

            //Normalisation with Mean and Variance
             stringstream tempNormF;
            Vector<double>* TrainX = Feats->GetRowCopy(Feats->GetLengthRows()-1);
            for (int i = 0; i < TrainX->GetLength(); i++){
                if (stdev->GetValue(i) != 0)
                    //TrainX->SetValue(i, (TrainX->Values[i]- means->GetValue(i))/(3*stdev->GetValue(i)));
                    TrainX->SetValue(i, (TrainX->Values[i]- means->GetValue(i))/(stdev->GetValue(i)));
                else
                    TrainX->SetValue(i,0.0);
                tempNormF << TrainX->GetValue(i)<< " ";
                /*Vector<double>* Temp = Feats->GetColCopy(i);
                    if (Temp->Variance() != 0){
                        double Val = (TrainX->Values[i] - Temp->Mean())/(3*sqrt(Temp->Variance()));
                        TrainX->SetValue(i,Val);
                    }
                    else
                        TrainX->SetValue(i,0.0);
                 */ 
            }
            if(DebugMode){
                Write2Log(LogFileName, tempNormF.str());
                temp << "PA:: Train:: Normalised correctly ";
                Write2Log(LogFileName, temp.str());
                temp.str( std::string() );
                temp.clear();
            }

            string InString = Vect2SparseString(TrainX, Label);
            //cout << "Let's see the InString" << endl << InString << endl;

            SfDataSet trainingData = new SfDataSet(false);
            trainingData.AddVector(InString);
            const SfSparseVector& x = trainingData.VectorAt(0);

            //this->eta = GetEta(this->eta_type, this->lambda, currentIt);
            this->eta = GetEta(this->eta_type, this->lambda, this->CurrentInstances);
            OneLearnerStep(this->learner_type, x, this->eta, this->c, this->lambda, this->epsilon, this->w);
            if(DebugMode){
                temp << "PA:: Train:: Trained";
                Write2Log(LogFileName, temp.str());
                temp.str( std::string() );
                temp.clear();
            }
            delete TrainX, trainingData;
        }
 
    }
    

    
        
    float myPredict(Matrix<double>* Feats, std::map<int, float> FeatDict){
        stringstream temp;
        if(DebugMode){
            temp << "PA:: Predict:: Got in here ";
            Write2Log(LogFileName, temp.str());
            temp.str( std::string() );
            temp.clear();
        }
        Vector<double>* TestX = new Vector<double>();
        for( std::map<int,float>::iterator j=FeatDict.begin(); j!=FeatDict.end(); ++j){
             TestX->Add((*j).second);
        }
        // Normalise the Features
        if (Feats->GetLengthRows() > 0){
            //cout << "I'm in!!" << endl;
            for (int i = 0; i < TestX->GetLength(); i++){
                    //Vector<double>* Temp = Feats->GetColCopy(i);
                if (stdev->GetValue(i) != 0){
                    double Val = (TestX->Values[i] - means->GetValue(i))/(stdev->GetValue(i));
                    //double Val = (TestX->Values[i] - means->GetValue(i))/(3*stdev->GetValue(i));
                    TestX->SetValue(i,Val);                    
                }
                else
                    TestX->SetValue(i,0.0);
            }
            
        }
        
        string InString = Vect2SparseString(TestX, 1.0);
        if(DebugMode){
            temp << "PA:: Predict:: Transformed the input to SF Sparse string";
            Write2Log(LogFileName, temp.str());
            temp.str( std::string() );
            temp.clear();
        }
        
        SfDataSet testData = new SfDataSet(false);
        testData.AddVector(InString);
        const SfSparseVector& x = testData.VectorAt(0);
        temp << "PA:: Predict:: Got Vector from sparse string";
        if(DebugMode){
            Write2Log(LogFileName, temp.str());
            temp.str( std::string() );
            temp.clear();
        }
        
        //const SfSparseVector& x = new SfSparseVector(Vect2SparseString(TestX, 0));
        
       // float pred = w->InnerProduct(x, 0.1);
         float pred = w->InnerProduct(x);
        
        if (pred < 0.0) pred = 0.0;
        else if (pred > 1.0) pred = 1.0;
        if(DebugMode){
            temp << "PA:: Predict:: Predicted. Value: " << pred;
            Write2Log(LogFileName, temp.str());
            temp.str( std::string() );
            temp.clear();
        }
        
        delete TestX, testData;
        return pred;
        
    }
    
    float myPredict(Matrix<double>* Feats, Vector<double>* TestX){
        stringstream temp;
        if(DebugMode){
            temp << "PA:: Predict:: Got in here ";
            Write2Log(LogFileName, temp.str());
            temp.str( std::string() );
            temp.clear();
        }
        
        // Get a clone of the TestX vector, just in case
        Vector<double>* TestX2 = new Vector<double>();
        for (int i=0; i<TestX->GetLength(); i++)
            TestX2->Add(TestX->GetValue(i));
        // Or maybe the line inder is better way?
        //Vector<double>* TestX2 = TestX->Clone()
        
        
        
        // Normalise the Features
        if (Feats->GetLengthRows() > 0){
            //cout << "I'm in!!" << endl;
            for (int i = 0; i < TestX2->GetLength(); i++){
                    //Vector<double>* Temp = Feats->GetColCopy(i);
                if (stdev->GetValue(i) != 0){
                    double Val = (TestX2->Values[i] - means->GetValue(i))/(stdev->GetValue(i));
                    //double Val = (TestX->Values[i] - means->GetValue(i))/(3*stdev->GetValue(i));
                    TestX2->SetValue(i,Val);                    
                }
                else
                    TestX2->SetValue(i,0.0);
            }
            
        }
        
        string InString = Vect2SparseString(TestX2, 1.0);
        
        if(DebugMode){
            temp << "PA:: Predict:: Transformed the input to SF Sparse string";
            Write2Log(LogFileName, temp.str());
            temp.str( std::string() );
            temp.clear();
        }
        
        SfDataSet testData = new SfDataSet(false);
        testData.AddVector(InString);
        const SfSparseVector& x = testData.VectorAt(0);
        
        if(DebugMode){
            temp << "PA:: Predict:: Got Vector from sparse string";
            Write2Log(LogFileName, temp.str());
            temp.str( std::string() );
            temp.clear();
        }
        //const SfSparseVector& x = new SfSparseVector(Vect2SparseString(TestX, 0));
        
        //float pred = w->InnerProduct(x, 0.1);
        float pred = w->InnerProduct(x);
       
        
        if (pred < 0.0) pred = 0.0;
        else if (pred > 1.0) pred = 1.0;
        
        if(DebugMode){
            temp << "PA:: Predict:: Predicted. Value: " << pred;
            Write2Log(LogFileName, temp.str());
            temp.str( std::string() );
            temp.clear();
        }
        
        //TestX2->Clear();
        
        delete TestX2, testData;
        return pred;
        
    }
    
    Vector<double>* myMargin(Matrix<double>* X, Vector<double>* Y){
        Vector<double>* V = new Vector<double>(X->GetLengthRows());
        
        Matrix<double>* Feats = new Matrix<double>();
        //Feats = X->Clone();
        for (int i=0; i<X->GetLengthRows(); i++) {
            //Feats->AddRowRefAt(X->GetRowCopy(i),i);
            //Feats->AddRowCopyAt(X->GetRowCopy(i),i);
            Feats->AddRowCopyAt(X->GetRowRef(i),i);
            //std::map<int, float> FeatDict;
            //for (int j=0; j<X->GetLengthCols(); j++){
            //    float Feat = X->GetValue(i,j);
            //    FeatDict.insert(make_pair(j++, Feat));
            //}
            //V->Add((double) this->myPredict(Feats,FeatDict));
            //V->Add((double) this->myPredict(Feats,X->GetRowRef(i)));
           // V->Add((double) this->myPredict(Feats,X->GetRowCopy(i)));
            V->Add((double) this->myPredict(Feats,X->GetRowRef(i)));
            
	}
        V->SubtractVector(Y);
        Feats->Clear();
        delete Feats;
        return V;
    }
    
    
    void myLoad(char* filename){
         LoadModelFromFile2(filename, &(this->w));
    }
    void LoadModelFromFile2(const std::string& file_name, SfWeightVector** w) {
          if (*w != NULL) {
            delete *w;
          }
          if (means != NULL) {
            delete means;
          }
          if (stdev != NULL) {
            delete stdev;
          }
        
         
          stringstream temp;

          std::fstream model_stream;
          model_stream.open(file_name.c_str(), std::fstream::in);
          
          
          
          if (!model_stream) {
              if(DebugMode){
                    temp << "PA:: Error opening model input file " << file_name;
                   Write2Log(LogFileName, temp.str());
              }
            exit(1);
          }

    
          
          temp << "Reading model from: " << file_name;
          std::string model_string, feats_string, noInstances_string, means_string, stdevs_string;
          std::getline(model_stream, model_string);
          std::getline(model_stream, feats_string);
          std::getline(model_stream, noInstances_string);
          std::getline(model_stream, means_string);
          std::getline(model_stream, stdevs_string);
          model_stream.close();
         
           
          *w = new SfWeightVector(model_string);
          
          stringstream ftemp(feats_string);
          int fn;
          ftemp >> fn;
          this->FeaturesNo = fn;
          
          stringstream insttemp(noInstances_string);
          int inst;
          insttemp >> inst;
          this->NoOfPreComputed = inst;
          // Needed?
          this->CurrentInstances = 0;
          
          
          means = new Vector<double>(FeaturesNo);
          stdev = new Vector<double>(FeaturesNo);
          
          stringstream mtemp(means_string);
          stringstream stdtemp(stdevs_string);
          
         /* Write2Log("./QE.log", "HERE3");
          Write2Log("./QE.log", mtemp.str());
          Write2Log("./QE.log", stdtemp.str());*/
          
        
          for (int i = 0; i<FeaturesNo; i++){
              double m,s;
              mtemp >> m;
              stdtemp >> s;
              
             /* std::ostringstream strs;
                //strs << m;
              strs << means->GetLength();
                std::string Mstr = strs.str();
              Write2Log("./QE.log", Mstr);
              
              std::ostringstream strb;
                //strb << s;
               strb << stdev->GetLength();
                std::string Sstr = strb.str();
              Write2Log("./QE.log", Sstr);
              */
              
              //Write2Log("./QE.log", stdtemp.str());
              //means->SetValue(i,m);
              means->AddAt(m,i);
              stdev->AddAt(s,i);
              //stdev->SetValue(i,s);
 
          }
          
          assert(*w != NULL);
          temp << "PA:: Done.";
          if(DebugMode){
                Write2Log(LogFileName, temp.str());
          }
         
        }

    
     void mySave(char* filename){
         SaveModelToFile2(filename, this->w);
    }
    void SaveModelToFile2(const std::string& file_name, SfWeightVector* w) {
        stringstream temp;
        std::fstream model_stream;
        model_stream.open(file_name.c_str(), std::fstream::out);
        if (!model_stream) {
            if(DebugMode){
                temp << "PA:: Error opening model output file " << file_name << std::endl;
                Write2Log(LogFileName, temp.str());
                temp.str( std::string() );
                temp.clear();
            }
            exit(1);
        }
        /* Format for PA models:
         * w1 w2 w3 ... (weights)
         * fn (features Number)
         * ci (current number of instances)
         * m1 m2 m3 ... (means)
         * sd1 sd2 sd3 ... (standard deviations)
         */
        temp << "PA:: Writing model to: " << file_name ;
        model_stream << w->AsString() << std::endl;
        model_stream << FeaturesNo << std::endl;
        model_stream << CurrentInstances << std::endl;
        for (int i = 0; i< means->GetLength(); i++)
            model_stream << means->GetValue(i) << " ";
        model_stream << std::endl;
        for (int i = 0; i< stdev->GetLength(); i++)
            model_stream << stdev->GetValue(i) << " ";
        model_stream << std::endl;
        
        model_stream.close();
        
        if(DebugMode){
            temp << "PA:: Save to file Done.";
            Write2Log(LogFileName, temp.str());
        }
    }

     
    string myGetWeights(){
        std::stringstream temp;
        //int i;
        //for (i=1; i < this->w->GetDimensions(); i++)
        //        temp << this->w->ValueOf(i) << " ";
        //temp << std::endl;
        //std::string str;
        //str = temp.str();
        //return str;
        char* WeightsFileName = "../Weights/Exp1.weights";
        ofstream WeightsFile;
        WeightsFile.open(WeightsFileName, ios::app);
        if (WeightsFile.is_open()) {
            WeightsFile << this->w->AsString() << endl;
        }
        WeightsFile.close();
        return this->w->AsString();
    }
    
    void myPrintWeights(char* WeightsFileName){
       std::stringstream temp;
        //WeightsFileName = "../Weights/Exp1.weights";
        ofstream WeightsFile;
        WeightsFile.open(WeightsFileName, ios::app);
        if (WeightsFile.is_open()) {
            WeightsFile << this->w->AsString() << endl;
        }
        WeightsFile.close();
    }
     
     void doNothing0(){}
     void doNothing1(double i){}
     void doNothing5(int i){}
     float doNothing2(){}
     int doNothing3(){}
     double doNothing4(){}
     float doNothing6(std::map<int, float> FeatDict){}
    
    string Vect2SparseString(Vector<double>* Feats, float Label){
        std::stringstream temp;
        char* temp1;
        char* temp2;
        int i;
        temp << Label << " ";
        
        //cout << "I'll be printing the string all the way... " << endl;
        
        for(int i=0; i<Feats->GetLength(); i++){
           
            temp << " " << (i+1) << ":" << Feats->Values[i] ;
            //cout << temp.str() << endl;
            
        }
        
        temp << "\n";
        return temp.str();        
    }
 /*   void Write2Log(string LogFileName, string s){
        ofstream LogFile;
        LogFile.open(LogFileName.c_str(), ios::app);
        if (LogFile.is_open()) {
            LogFile <<  s << endl;
            LogFile.close();
        }
    }*/
    
private:
        
};

/*class SofiaPegasosAdapter : public Sofiaml {
public:
    SofiaPegasosAdapter(int dimensions) : Sofiaml(PEGASOS,dimensions){}
    //SofiaPegasosAdapter(int dimensions) : Sofiaml(SGD_SVM,dimensions){}
    virtual ~SofiaPegasosAdapter(){}
    
    void myTrain(Matrix<double>* Feats, Vector<double>* Labels, int currentIt){
        
        if (Feats->GetLengthRows() > 3){
            //std::cout << "I think I'll train now!" << endl;
            float Label = Labels->GetValue(Labels->GetLength()-1);
            //cout << "the label to train is " << Label << endl;

            Vector<double>* TrainX = Feats->GetRowCopy(Feats->GetLengthRows()-1);
            for (int i = 0; i < TrainX->GetLength(); i++){
                Vector<double>* Temp = Feats->GetColCopy(i);
                //std::cout << "The temp is" << endl;
                //Temp->Print();
                    if (Temp->Variance() != 0){
                        //std::cout << "Value = " << TrainX->Values[i] << endl << "Mean = " << Temp->Mean() << endl << "Variance = " << Temp->Variance() << endl;
                        //std::cout << "SQRT Var = " << sqrt(Temp->Variance()) << endl;
                        double Val = (TrainX->Values[i] - Temp->Mean())/(sqrt(Temp->Variance()));
                        //std::cout << "Val = " << Val << endl;
                        TrainX->SetValue(i,Val);
                        //std::cout << "TrainX = " << endl;
                        //TrainX->Print();
                    }
                    else
                        TrainX->SetValue(i,0.0);                
            }
            
            //std::cout << "TrainX is:" << endl;
            //TrainX->Print();

            string InString = Vect2SparseString(TrainX, Label);
            //cout << "Let's see the InString" << endl << InString << endl;

            SfDataSet trainingData = new SfDataSet(false);
            trainingData.AddVector(InString);
            const SfSparseVector& x = trainingData.VectorAt(0);
            
            //std::cout << "The training Data is:" << endl <<  trainingData.AsString() << endl;
            //string mtmp;
            //getline(std::cin, mtmp);

            this->eta = GetEta(this->eta_type, this->lambda, currentIt);
            OneLearnerStep(this->learner_type, x, this->eta, this->c, this->lambda, this->epsilon, this->w);
        }
        else if (Feats->GetLengthRows() == 3){
            //std::cout << "I'm not ready to train yet..." << endl;
            for(int j=0; j<3; j++){
                float Label = Labels->GetValue(Labels->GetLength()-(j+1));
                //cout << "the label to train is " << Label << endl;

                Vector<double>* TrainX = Feats->GetRowCopy(Feats->GetLengthRows()-(j+1));
                for (int i = 0; i < TrainX->GetLength(); i++){
                    Vector<double>* Temp = Feats->GetColCopy(i);
                    if (Temp->Variance() != 0){
                        double Val = (TrainX->Values[i] - Temp->Mean())/(sqrt(Temp->Variance()));
                        TrainX->SetValue(i,Val);
                    }
                    else
                        TrainX->SetValue(i,0.0);
                    //TrainX->SetValue(i,(TrainX->Values[i] - Temp->Mean())/(sqrt(Temp->Variance())));
                }
                
                string InString = Vect2SparseString(TrainX, Label);
                //cout << "Let's see the InString" << endl << InString << endl;

                SfDataSet trainingData = new SfDataSet(false);
                trainingData.AddVector(InString);
                const SfSparseVector& x = trainingData.VectorAt(0);
               

                //std::cout << "The training Data is:" << endl << trainingData.AsString() << endl;
                //string mtmp;
                //getline(std::cin, mtmp);
                
                this->eta = GetEta(this->eta_type, this->lambda, currentIt);
                OneLearnerStep(this->learner_type, x, this->eta, this->c, this->lambda, this->epsilon, this->w);
            }
            
        }
    }
    

    
        
    float myPredict(Matrix<double>* Feats, std::map<int, float> FeatDict){
        Vector<double>* TestX = new Vector<double>();
        for( std::map<int,float>::iterator j=FeatDict.begin(); j!=FeatDict.end(); ++j){
             TestX->Add((*j).second);
        }
        //cout << "I will print the Test Vector, just to make sure..." << endl;
        //TestX->Print();
        
        //Feats->Print();
        
        if (Feats->GetLengthRows() > 2){
            //cout << "I'm in!!" << endl;
            for (int i = 0; i < TestX->GetLength(); i++){
                    Vector<double>* Temp = Feats->GetColCopy(i);
                    if (Temp->Variance() != 0){
                        double Val = (TestX->Values[i] - Temp->Mean())/(sqrt(Temp->Variance()));
                        TestX->SetValue(i,Val);                    
                    }
                    else
                        TestX->SetValue(i,0.0);
                    //TestX->SetValue(i,(TestX->Values[i] - Temp->Mean())/(sqrt(Temp->Variance())));
            }
            //TestX->Print();
        }
        string InString = Vect2SparseString(TestX, 1.0);
        //cout << "Let's see the InString" << endl << InString << endl;
        
        SfDataSet testData = new SfDataSet(false);
        testData.AddVector(InString);
        //std::cout << "Predict Vector = " << endl << (testData.VectorAt(0)).AsString();
        const SfSparseVector& x = testData.VectorAt(0);
        //const SfSparseVector& x = new SfSparseVector(Vect2SparseString(TestX, 0));
        float pred = w->InnerProduct(x, 0.1);
        if (pred < 0.0) return 0.0;
        else if (pred > 1.0) return 1.0;
        else return pred;
        //return  w->InnerProduct(x);//SingleSvmPrediction(x,w);
    }
    
    
    void myLoad(char* filename){
         Sofiaml::LoadModelFromFile(filename, &(this->w));
    }
    
     void mySave(char* filename){
         Sofiaml::SaveModelToFile(filename, this->w);
    }
     string myGetWeights(){
        std::stringstream temp;
        char* WeightsFileName = "../Weights/Exp1.weights";
        ofstream WeightsFile;
        WeightsFile.open(WeightsFileName, ios::app);
        if (WeightsFile.is_open()) {
            WeightsFile << this->w->AsString() << endl;
        }
        WeightsFile.close();
        return this->w->AsString();
    }
    
    void myPrintWeights(char* WeightsFileName){
       std::stringstream temp;
        //WeightsFileName = "../Weights/Exp1.weights";
        ofstream WeightsFile;
        WeightsFile.open(WeightsFileName, ios::app);
        if (WeightsFile.is_open()) {
            WeightsFile << this->w->AsString() << endl;
        }
        WeightsFile.close();
    }
     
     void doNothing0(){}
     void doNothing1(double i){}
     void doNothing5(int i){}
     float doNothing2(){}
     int doNothing3(){}
     double doNothing4(){}
     float doNothing6(std::map<int, float> FeatDict){}
    
    string Vect2SparseString(Vector<double>* Feats, float Label){
        std::stringstream temp;
        char* temp1;
        char* temp2;
        int i;
        temp << Label << " ";
        
        //cout << "I'll be printing the string all the way... " << endl;
        
        for(int i=0; i<Feats->GetLength(); i++){
           
            temp << " " << (i+1) << ":" << Feats->Values[i] ;
            //cout << temp.str() << endl;
            
        }
        
        temp << "\n";
        return temp.str();        
    }
    
private:
        
};

class SofiaSgdSvmAdapter : public Sofiaml {
public:
    SofiaSgdSvmAdapter(int dimensions) : Sofiaml(SGD_SVM,dimensions){}
    //SofiaPegasosAdapter(int dimensions) : Sofiaml(SGD_SVM,dimensions){}
    virtual ~SofiaSgdSvmAdapter(){}
    
    void myTrain(Matrix<double>* Feats, Vector<double>* Labels, int currentIt){
        if (Feats->GetLengthRows() > 3){
            float Label = Labels->GetValue(Labels->GetLength()-1);
            //cout << "the label to train is " << Label << endl;

            Vector<double>* TrainX = Feats->GetRowCopy(Feats->GetLengthRows()-1);
            for (int i = 0; i < TrainX->GetLength(); i++){
                Vector<double>* Temp = Feats->GetColCopy(i);
                    if (Temp->Variance() != 0){
                        double Val = (TrainX->Values[i] - Temp->Mean())/(3*sqrt(Temp->Variance()));
                        TrainX->SetValue(i,Val);
                    }
                    else
                        TrainX->SetValue(i,0.0);
            }

            string InString = Vect2SparseString(TrainX, Label);
            //cout << "Let's see the InString" << endl << InString << endl;

            SfDataSet trainingData = new SfDataSet(false);
            trainingData.AddVector(InString);
            const SfSparseVector& x = trainingData.VectorAt(0);

            this->eta = GetEta(this->eta_type, this->lambda, currentIt);
            OneLearnerStep(this->learner_type, x, this->eta, this->c, this->lambda, this->epsilon, this->w);
        }
        else if (Feats->GetLengthRows() == 3){
            for(int j=0; j<3; j++){
                float Label = Labels->GetValue(Labels->GetLength()-(j+1));
                //cout << "the label to train is " << Label << endl;

                Vector<double>* TrainX = Feats->GetRowCopy(Feats->GetLengthRows()-(j+1));
                for (int i = 0; i < TrainX->GetLength(); i++){
                    Vector<double>* Temp = Feats->GetColCopy(i);
                    if (Temp->Variance() != 0){
                        double Val = (TrainX->Values[i] - Temp->Mean())/(3*sqrt(Temp->Variance()));
                        TrainX->SetValue(i,Val);
                    }
                    else
                        TrainX->SetValue(i,0.0);
                    //TrainX->SetValue(i,(TrainX->Values[i] - Temp->Mean())/(sqrt(Temp->Variance())));
                }

                string InString = Vect2SparseString(TrainX, Label);
                //cout << "Let's see the InString" << endl << InString << endl;

                SfDataSet trainingData = new SfDataSet(false);
                trainingData.AddVector(InString);
                const SfSparseVector& x = trainingData.VectorAt(0);

                this->eta = GetEta(this->eta_type, this->lambda, currentIt);
                OneLearnerStep(this->learner_type, x, this->eta, this->c, this->lambda, this->epsilon, this->w);
            }
            
        }
    }
    

    
        
    float myPredict(Matrix<double>* Feats, std::map<int, float> FeatDict){
        Vector<double>* TestX = new Vector<double>();
        for( std::map<int,float>::iterator j=FeatDict.begin(); j!=FeatDict.end(); ++j){
             TestX->Add((*j).second);
        }
        //cout << "I will print the Test Vector, just to make sure..." << endl;
        //TestX->Print();
        
        Feats->Print();
        
        if (Feats->GetLengthRows() > 2){
            //cout << "I'm in!!" << endl;
            for (int i = 0; i < TestX->GetLength(); i++){
                    Vector<double>* Temp = Feats->GetColCopy(i);
                    if (Temp->Variance() != 0){
                        double Val = (TestX->Values[i] - Temp->Mean())/(3*sqrt(Temp->Variance()));
                        TestX->SetValue(i,Val);                    
                    }
                    else
                        TestX->SetValue(i,0.0);
                    //TestX->SetValue(i,(TestX->Values[i] - Temp->Mean())/(sqrt(Temp->Variance())));
            }
            //TestX->Print();
        }
        string InString = Vect2SparseString(TestX, 1.0);
        //cout << "Let's see the InString" << endl << InString << endl;
        
        SfDataSet testData = new SfDataSet(false);
        testData.AddVector(InString);
        const SfSparseVector& x = testData.VectorAt(0);
        //const SfSparseVector& x = new SfSparseVector(Vect2SparseString(TestX, 0));
        
        float pred = w->InnerProduct(x, 0.1);
        if (pred < 0.0) return 0.0;
        else if (pred > 1.0) return 1.0;
        else return pred;
        //return  w->InnerProduct(x);//SingleSvmPrediction(x,w);
    }
    
    void myLoad(char* filename){
         Sofiaml::LoadModelFromFile(filename, &(this->w));
    }
    
     void mySave(char* filename){
         Sofiaml::SaveModelToFile(filename, this->w);
    }
     string myGetWeights(){
        std::stringstream temp;
        //int i;
        //for (i=1; i < this->w->GetDimensions(); i++)
        //        temp << this->w->ValueOf(i) << " ";
        //temp << std::endl;
        //std::string str;
        //str = temp.str();
        //return str;
        char* WeightsFileName = "../Weights/Exp1.weights";
        ofstream WeightsFile;
        WeightsFile.open(WeightsFileName, ios::app);
        if (WeightsFile.is_open()) {
            WeightsFile << this->w->AsString() << endl;
        }
        WeightsFile.close();
        return this->w->AsString();
    }
    
    void myPrintWeights(char* WeightsFileName){
       std::stringstream temp;
        //WeightsFileName = "../Weights/Exp1.weights";
        ofstream WeightsFile;
        WeightsFile.open(WeightsFileName, ios::app);
        if (WeightsFile.is_open()) {
            WeightsFile << this->w->AsString() << endl;
        }
        WeightsFile.close();
    }
     
     void doNothing0(){}
     void doNothing1(double i){}
     void doNothing5(int i){}
     float doNothing2(){}
     int doNothing3(){}
     double doNothing4(){}
     float doNothing6(std::map<int, float> FeatDict){}
    
    string Vect2SparseString(Vector<double>* Feats, float Label){
        std::stringstream temp;
        char* temp1;
        char* temp2;
        int i;
        temp << Label << " ";
        
        //cout << "I'll be printing the string all the way... " << endl;
        
        for(int i=0; i<Feats->GetLength(); i++){
           
            temp << " " << (i+1) << ":" << Feats->Values[i] ;
            //cout << temp.str() << endl;
            
        }
        
        temp << "\n";
        return temp.str();        
    }
    
private:
        
};


class SofiaMarginPerceptronAdapter : public Sofiaml {
public:
    SofiaMarginPerceptronAdapter(int dimensions) : Sofiaml(MARGIN_PERCEPTRON,dimensions){}
    //SofiaPegasosAdapter(int dimensions) : Sofiaml(SGD_SVM,dimensions){}
    virtual ~SofiaMarginPerceptronAdapter(){}
    
    void myTrain(Matrix<double>* Feats, Vector<double>* Labels, int currentIt){
        if (currentIt > 3){
            float Label = Labels->GetValue(Labels->GetLength()-1);
            //cout << "the label to train is " << Label << endl;

            Vector<double>* TrainX = Feats->GetRowCopy(Feats->GetLengthRows()-1);
            for (int i = 0; i < TrainX->GetLength(); i++){
                Vector<double>* Temp = Feats->GetColCopy(i);
                if (Temp->Variance() != 0){
                    double Val = (TrainX->Values[i] - Temp->Mean())/(sqrt(Temp->Variance()));
                    TrainX->SetValue(i,Val);
                }
                else
                    TrainX->SetValue(i,0.0);
            }

            string InString = Vect2SparseString(TrainX, Label);
            //cout << "Let's see the InString" << endl << InString << endl;

            SfDataSet trainingData = new SfDataSet(false);
            trainingData.AddVector(InString);
            const SfSparseVector& x = trainingData.VectorAt(0);

            this->eta = GetEta(this->eta_type, this->lambda, currentIt);
            OneLearnerStep(this->learner_type, x, this->eta, this->c, this->lambda, this->epsilon, this->w);
        }
        else if (currentIt == 3){
            for(int j=0; j<3; j++){
                float Label = Labels->GetValue(Labels->GetLength()-(j+1));
                //cout << "the label to train is " << Label << endl;

                Vector<double>* TrainX = Feats->GetRowCopy(Feats->GetLengthRows()-(j+1));
                for (int i = 0; i < TrainX->GetLength(); i++){
                    Vector<double>* Temp = Feats->GetColCopy(i);
                    if (Temp->Variance() != 0){
                        double Val = (TrainX->Values[i] - Temp->Mean())/(sqrt(Temp->Variance()));
                        TrainX->SetValue(i,Val);
                    }
                    else
                        TrainX->SetValue(i,0.0);
                    //TrainX->SetValue(i,(TrainX->Values[i] - Temp->Mean())/(sqrt(Temp->Variance())));
                }

                string InString = Vect2SparseString(TrainX, Label);
                //cout << "Let's see the InString" << endl << InString << endl;

                SfDataSet trainingData = new SfDataSet(false);
                trainingData.AddVector(InString);
                const SfSparseVector& x = trainingData.VectorAt(0);

                this->eta = GetEta(this->eta_type, this->lambda, currentIt);
                OneLearnerStep(this->learner_type, x, this->eta, this->c, this->lambda, this->epsilon, this->w);
            }
            
        }
    }
    

    
        
    float myPredict(Matrix<double>* Feats, std::map<int, float> FeatDict){
        Vector<double>* TestX = new Vector<double>();
        for( std::map<int,float>::iterator j=FeatDict.begin(); j!=FeatDict.end(); ++j){
             TestX->Add((*j).second);
        }
        //cout << "I will print the Test Vector, just to make sure..." << endl;
        //TestX->Print();
        
        Feats->Print();
        
        if (Feats->GetLengthRows() > 2){
            //cout << "I'm in!!" << endl;
            for (int i = 0; i < TestX->GetLength(); i++){
                    Vector<double>* Temp = Feats->GetColCopy(i);
                    if (Temp->Variance() != 0){
                        double Val = (TestX->Values[i] - Temp->Mean())/(sqrt(Temp->Variance()));
                        TestX->SetValue(i,Val);                    
                    }
                    else
                        TestX->SetValue(i,0.0);
                    //TestX->SetValue(i,(TestX->Values[i] - Temp->Mean())/(sqrt(Temp->Variance())));
            }
            TestX->Print();
        }
        string InString = Vect2SparseString(TestX, 1.0);
        //cout << "Let's see the InString" << endl << InString << endl;
        
        SfDataSet testData = new SfDataSet(false);
        testData.AddVector(InString);
        const SfSparseVector& x = testData.VectorAt(0);
        //const SfSparseVector& x = new SfSparseVector(Vect2SparseString(TestX, 0));
        
        return  w->InnerProduct(x);//SingleSvmPrediction(x,w);
    }
    
    void myLoad(char* filename){
         Sofiaml::LoadModelFromFile(filename, &(this->w));
    }
    
     void mySave(char* filename){
         Sofiaml::SaveModelToFile(filename, this->w);
    }
     string myGetWeights(){
        std::stringstream temp;
        //int i;
        //for (i=1; i < this->w->GetDimensions(); i++)
        //        temp << this->w->ValueOf(i) << " ";
        //temp << std::endl;
        //std::string str;
        //str = temp.str();
        //return str;
        char* WeightsFileName = "../Weights/Exp1.weights";
        ofstream WeightsFile;
        WeightsFile.open(WeightsFileName, ios::app);
        if (WeightsFile.is_open()) {
            WeightsFile << this->w->AsString() << endl;
        }
        WeightsFile.close();
        return this->w->AsString();
    }
    
    void myPrintWeights(char* WeightsFileName){
       std::stringstream temp;
        //WeightsFileName = "../Weights/Exp1.weights";
        ofstream WeightsFile;
        WeightsFile.open(WeightsFileName, ios::app);
        if (WeightsFile.is_open()) {
            WeightsFile << this->w->AsString() << endl;
        }
        WeightsFile.close();
    }
     void doNothing0(){}
     void doNothing1(double i){}
     void doNothing5(int i){}
     float doNothing2(){}
     int doNothing3(){}
     double doNothing4(){}
     float doNothing6(std::map<int, float> FeatDict){}
    
    string Vect2SparseString(Vector<double>* Feats, float Label){
        std::stringstream temp;
        char* temp1;
        char* temp2;
        int i;
        temp << Label << " ";
        
        //cout << "I'll be printing the string all the way... " << endl;
        
        for(int i=0; i<Feats->GetLength(); i++){
           
            temp << " " << (i+1) << ":" << Feats->Values[i] ;
            //cout << temp.str() << endl;
            
        }
        
        temp << "\n";
        return temp.str();        
    }
    
private:
        
};
*/

#endif	/* SOFIAINTERFACE_H */

