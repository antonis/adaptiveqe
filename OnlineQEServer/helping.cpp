/* 
 * File:   helping.h
 * Author: antonis
 *
 * Created on October 5, 2014, 3:08 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cstring>      // Needed for memset

#include "helping.h"

using namespace std;

void Write2Log(string LogFileName, string s){
    ofstream LogFile;
    LogFile.open(LogFileName.c_str(), ios::app);
    if (LogFile.is_open()) {
        LogFile << s << endl;
        LogFile.close();
    }
}


void replaceAll( string &s, const string &search, const string &replace ) {
    for( size_t pos = 0; ; pos += replace.length() ) {
        // Locate the substring to replace
        pos = s.find( search, pos );
        if( pos == string::npos ) break;
        // Replace by erasing and inserting
        s.erase( pos, search.length() );
        s.insert( pos, replace );
    }
}

