/* 
 * File:   crossvalidation.h
 * Author: antonis
 *
 * Created on October 5, 2014, 2:54 PM
 */

#include "TrainInstance.h"
#include "TERInterface.h"
#include "LearningInterface.h"
#include "helping.h"


#ifndef CROSSVALIDATION_H
#define	CROSSVALIDATION_H

extern int DebugMode;
extern string LogFileName;
extern int NoOfFeatures;

// Perform grid-search over the parameters' lists and cross-validation
void CrossValidation (onlinesvr::Matrix<double>* TrainingSetX, Vector<double>* TrainingSetY, Vector<double>* EpsilonList, Vector<double>* CList, Vector<double>* KernelParamList, int SetNumber, char* ResultsFileName, int LoadModelFromFile);
//void CrossValidation (onlinesvr::Matrix<double>* TrainingSetX, Vector<double>* TrainingSetY, Vector<double>* EpsilonList, Vector<double>* CList, Vector<double>* KernelParamList, int SetNumber, char* ResultsFileName, int Algorithm, int LoadModelFromFile);
Vector<double>* CrossValidation (onlinesvr::Matrix<double>* TrainingSetX, Vector<double>* TrainingSetY, Vector<double>* EpsilonList, Vector<double>* CList, Vector<double>* KernelParamList, double KernelTypeSVR, double KernelTypeOGP, int SetNumber, char* ResultsFileName, int Algorithm, int LoadModelFromFile);
double CrossValidation (Vector<onlinesvr::Matrix<double>*>* SetX, Vector<Vector<double>*>* SetY, double Epsilon, double C, double KernelParam, int LoadModelFromFile);
double CrossValidation (Vector<onlinesvr::Matrix<double>*>* SetX, Vector<Vector<double>*>* SetY, double Epsilon, double C, double KernelParam, double KernelTypeSVR, double KernelTypeOGP, int Algorithm, int LoadModelFromFile);


void CrossValidation (onlinesvr::Matrix<double>* TrainingSetX, Vector<double>* TrainingSetY, Vector<double>* EpsilonList, Vector<double>* CList, Vector<double>* KernelParamList, int SetNumber, char* ResultsFileName, int LoadModelFromFile)
{
        if (DebugMode){ stringstream temp2; temp2 <<"void CrossValidation (....., int LoadModelFromFile)\n"; Write2Log(LogFileName, temp2.str()); }
        int i, j, k;

        // Partition of the training set
        int SamplesPerSet = static_cast<int>(TrainingSetX->GetLengthRows()/SetNumber);

        // Build the Sets
        Vector<onlinesvr::Matrix<double>*>* SetX = new Vector<onlinesvr::Matrix<double>*>();
        Vector<Vector<double>*>* SetY = new Vector<Vector<double>*>();		
        for (i=0; i<SetNumber; i++) {
                SetX->Add(new onlinesvr::Matrix<double>());
                SetY->Add(new Vector<double>());
                for (j=0; j<SamplesPerSet; j++) {
                        SetX->GetValue(i)->AddRowCopy(TrainingSetX->GetRowRef(j*SetNumber+i));
                        SetY->GetValue(i)->Add(TrainingSetY->GetValue(j*SetNumber+i));
                }
        }

        // Open the CV output file
        ofstream File (ResultsFileName, ios::out);
        if (!File) {
                cerr << "Error. It's impossible to create the file." << endl;
                return;
        }		
        File << "Epsilon \t C \t KernelParam \t Error" << endl;

        // Cross Validation
        int IterationsNumber = EpsilonList->GetLength() * CList->GetLength() * KernelParamList->GetLength();
        int CurrentIteration = 0;
        double MinError = 10000;
        double MinEpsilon = 0;
        double MinC = 0;
        double MinKernelParam = 0;
        for (i=0; i<EpsilonList->GetLength(); i++) {
                for (j=0; j<CList->GetLength(); j++) {		
                        for (k=0; k<KernelParamList->GetLength(); k++) {
                                if(1==0)//if (CurrentIteration==33 || CurrentIteration==34 || CurrentIteration==42)
                                {CurrentIteration++; printf("%d\n",CurrentIteration);}
                                else {
                                double Error = CrossValidation (SetX, SetY, EpsilonList->GetValue(i), CList->GetValue(j), KernelParamList->GetValue(k), LoadModelFromFile);
                                File << EpsilonList->GetValue(i) << "\t" << CList->GetValue(j) << "\t" << KernelParamList->GetValue(k) << " \t" << Error << endl;
                                if (DebugMode){
                                        stringstream temp2;
                                        temp2 << "Test " << ++CurrentIteration << "/" << IterationsNumber << "\t" << "Epsilon=" << EpsilonList->GetValue(i) << "\t" << "C=" << CList->GetValue(j) << "\t" << "KernelParam=" << KernelParamList->GetValue(k) << " \t" << "Error=" << Error << endl;
                                        Write2Log(LogFileName,temp2.str());
                                }
                                if (Error<MinError) {
                                        MinError = Error;
                                        MinEpsilon = EpsilonList->GetValue(i);
                                        MinC = CList->GetValue(j);
                                        MinKernelParam = KernelParamList->GetValue(k);
                                }
                                }
                        }
                }
        }

        // Best Result		
        File << endl << "Best Solution:" << endl;
        File << MinEpsilon << "\t" << MinC << "\t" << MinKernelParam << " \t" << MinError << endl;
        if (DebugMode){
                stringstream temp;
                temp << "Best Solution:" << MinEpsilon << "\t" << MinC << "\t" << MinKernelParam << " \t" << MinError << endl;
                Write2Log(LogFileName,temp.str()); 
        }
        // Close the file
        File.close();

        // Free the memory
        for (i=0; i<SetNumber; i++) {
                delete SetX->GetValue(i);
                delete SetY->GetValue(i);
        }
        delete SetX;
        delete SetY;
}

//void CrossValidation (onlinesvr::Matrix<double>* TrainingSetX, Vector<double>* TrainingSetY, Vector<double>* EpsilonList, Vector<double>* CList, Vector<double>* KernelParamList, int SetNumber, char* ResultsFileName, int Algorithm, int LoadModelFromFile)
Vector<double>* CrossValidation (onlinesvr::Matrix<double>* TrainingSetX, Vector<double>* TrainingSetY, Vector<double>* EpsilonList, Vector<double>* CList, Vector<double>* KernelParamList, double KernelTypeSVR, double KernelTypeOGP,int SetNumber, char* ResultsFileName, int Algorithm, int LoadModelFromFile)

{
       if (DebugMode){ stringstream temp2; temp2 <<"Kernel Types: " <<KernelTypeSVR<< " "<< KernelTypeOGP <<" \n"; Write2Log(LogFileName, temp2.str()); }

        if (DebugMode){ stringstream temp2; temp2 <<"void CrossValidation (..... int Algorithm, int LoadModelFromFile)\n"; Write2Log(LogFileName, temp2.str()); }

        int i, j, k;

        // Partition of the training set
        int SamplesPerSet = static_cast<int>(TrainingSetX->GetLengthRows()/SetNumber);


        // Build the Sets
        Vector<onlinesvr::Matrix<double>*>* SetX = new Vector<onlinesvr::Matrix<double>*>();
        Vector<Vector<double>*>* SetY = new Vector<Vector<double>*>();		
        for (i=0; i<SetNumber; i++) {
                SetX->Add(new onlinesvr::Matrix<double>());
                SetY->Add(new Vector<double>());
                for (j=0; j<SamplesPerSet; j++) {
                        SetX->GetValue(i)->AddRowCopy(TrainingSetX->GetRowCopy(j*SetNumber+i));
                        SetY->GetValue(i)->Add(TrainingSetY->GetValue(j*SetNumber+i));
                }
        }
	
        if (DebugMode){
              stringstream temp2;
              temp2 <<"CV FILE OUTPUT: "<< ResultsFileName;
              Write2Log(LogFileName, temp2.str());
        }
        // Open the file
        ofstream File (ResultsFileName, ios::out);
        if (!File) {
                //cerr << "Error. It's impossible to create the CV output file." << endl;
                //if (DebugMode) 
                Write2Log(LogFileName, "CV Error: It's impossible to create the CV output file.");
                return NULL;
        }		
        File << "Epsilon \t C \t KernelParam \t Error" << endl;

        // Cross Validation
        
        // Compute Iterations Numbers according to the algorithm and the lists that will be used
        int IterationsNumber;
        if (Algorithm == 0) IterationsNumber = EpsilonList->GetLength() * CList->GetLength() * KernelParamList->GetLength();
        else if (Algorithm == 1) IterationsNumber = EpsilonList->GetLength() * CList->GetLength();
        else if (Algorithm == 2) IterationsNumber = EpsilonList->GetLength() * CList->GetLength();
        
        int CurrentIteration = 0;
        double MinError = 10000;
        double MinEpsilon = 0;
        double MinC = 0;
        double MinKernelParam = 0;
        for (i=0; i<EpsilonList->GetLength(); i++) {
            for (j=0; j<CList->GetLength(); j++) {		
                if (Algorithm == 0){
                    for (k=0; k<KernelParamList->GetLength(); k++) {
                        double Error = CrossValidation (SetX, SetY, EpsilonList->GetValue(i), CList->GetValue(j), KernelParamList->GetValue(k), KernelTypeSVR, KernelTypeOGP, Algorithm,  LoadModelFromFile);
                        File << EpsilonList->GetValue(i) << "\t" << CList->GetValue(j) << "\t" << KernelParamList->GetValue(k) << " \t" << Error << endl;
                        if (DebugMode){
                                stringstream temp2;
                                temp2 << "Test " << ++CurrentIteration << "/" << IterationsNumber << "\t" << "Epsilon=" << EpsilonList->GetValue(i) << "\t" << "C=" << CList->GetValue(j) << "\t" << "KernelParam=" << KernelParamList->GetValue(k) << " \t" << "Error=" << Error << endl;
                                Write2Log(LogFileName,temp2.str());
                        }
                        if (Error<MinError) {
                                MinError = Error;
                                MinEpsilon = EpsilonList->GetValue(i);
                                MinC = CList->GetValue(j);
                                MinKernelParam = KernelParamList->GetValue(k);
                        }
                    }
                }
                else if (Algorithm == 1){
                    if (DebugMode){
                        Write2Log(LogFileName,"Cross-Validation:: PA:: Got into the iterations");
                    }
                    double Error = CrossValidation (SetX, SetY, EpsilonList->GetValue(i), CList->GetValue(j), 0.0, KernelTypeSVR, KernelTypeOGP, Algorithm,  LoadModelFromFile);
                    File << EpsilonList->GetValue(i) << "\t" << CList->GetValue(j) << "\t" << Error << endl;
                    if (DebugMode){
                        stringstream temp2;
                        temp2 << "Test " << ++CurrentIteration << "/" << IterationsNumber << "\t" << "Epsilon=" << EpsilonList->GetValue(i) << "\t" << "C=" << CList->GetValue(j) << "\t" << "Error=" << Error << endl;
                        Write2Log(LogFileName,temp2.str());
                    }
                    if (Error<MinError) {
                            MinError = Error;
                            MinEpsilon = EpsilonList->GetValue(i);
                            MinC = CList->GetValue(j);
                            //MinKernelParam = KernelParamList->GetValue(k);
                    }
                }
                else if (Algorithm == 2){
                    Write2Log(LogFileName, "Cross-Validation:: OGP:: Got into the iterations");
                    double Error = CrossValidation(SetX, SetY, EpsilonList->GetValue(i), CList->GetValue(j), 0.0, KernelTypeSVR, KernelTypeOGP, Algorithm,  LoadModelFromFile);
                    File << EpsilonList->GetValue(i) << "\t" << CList->GetValue(j) << "\t" << Error << endl;
                    if (DebugMode){
                        stringstream temp2;
                        temp2 << "Test " << ++CurrentIteration << "/" << IterationsNumber << "\t" << "Epsilon=" << EpsilonList->GetValue(i) << "\t" << "C=" << (int)CList->GetValue(j) << "\t" << "Error=" << Error << endl;
                        Write2Log(LogFileName,temp2.str());
                    }
                    if (Error<MinError) {
                            MinError = Error;
                            MinEpsilon = EpsilonList->GetValue(i);
                            MinC = CList->GetValue(j);
                            //MinKernelParam = KernelParamList->GetValue(k);
                    }
                }
            }
        }
        
        Vector<double>* BestParam = new Vector<double>(3);
        // Best Result		
        File << endl << "Best Solution:" << endl;
        if (Algorithm == 0){
            File << MinEpsilon << "\t" << MinC << "\t" << MinKernelParam << " \t" << MinError << endl;
            BestParam->AddAt(MinEpsilon,0);
            BestParam->AddAt(MinC,1);
            BestParam->AddAt(MinKernelParam,2);
            
            if (DebugMode){
                stringstream temp;
                temp << "Best Solution:" << MinEpsilon << "\t" << MinC << "\t" << MinKernelParam << " \t" << MinError << endl;
                Write2Log(LogFileName,temp.str());
            }
        }
        else if (Algorithm == 1){
            File << MinEpsilon << "\t" << MinC << "\t" << MinError << endl;
            
            BestParam->AddAt(MinEpsilon,0);
            BestParam->AddAt(MinC,1);
            
            if (DebugMode){
                stringstream temp;
                temp << "Best Solution:" << MinEpsilon << "\t" << MinC << "\t" << MinError << endl;
                Write2Log(LogFileName,temp.str());
            }

        }
        else if (Algorithm == 2){
            File << MinEpsilon << "\t" << (int)MinC << "\t" << MinError << endl;
            
            BestParam->AddAt(MinEpsilon,0);
            BestParam->AddAt(MinC,1);
            
            if (DebugMode){
                stringstream temp;
                temp << "Best Solution:" << MinEpsilon << "\t" << (int)MinC << "\t" << MinError << endl;
                Write2Log(LogFileName,temp.str()); 
            }
        }
        

        // Close the file
        File.close();

        // Free the memory
        for (i=0; i<SetNumber; i++) {
                delete SetX->GetValue(i);
                delete SetY->GetValue(i);
        }
        delete SetX;
        delete SetY;
        
        return BestParam;
}


double CrossValidation (Vector<onlinesvr::Matrix<double>*>* SetX, Vector<Vector<double>*>* SetY, double Epsilon, double C, double KernelParam, double KernelTypeSVR, double KernelTypeOGP,int Algorithm, int LoadModelFromFile)
{
        if (DebugMode){ stringstream temp2; temp2 <<"double CrossValidation (..... int Algorithm, int LoadModelFromFile"; Write2Log(LogFileName, temp2.str()); }
        // Variables
        Vector<double>* Errors = new Vector<double>();
        int i, j, k;
            

        // Subdivide TrainingSet into SubSets
        for (i=0; i<SetX->GetLength(); i++)
        {
	    //sleep(10);
           // if (DebugMode){ stringstream temp2; temp2 <<"double CrossValidation (..... int Algorithm, int LoadModelFromFile"; Write2Log(LogFileName, temp2.str()); }
           // if (DebugMode){ stringstream temp2; temp2 <<"for loop start i:" << i; Write2Log(LogFileName, temp2.str()); }
            // FeaturesSet and Labels for the particular fold/iteration
            onlinesvr::Matrix<double>* FeaturesSet = new onlinesvr::Matrix<double>;
            Vector<double>* Labels = new Vector<double>();

            // New interface
            LearningInterface **Learn = initializeLearningAdaptors();
           // if (DebugMode){ stringstream temp2; temp2 <<"after LearningInterface **Learn = initializeLearningAdaptors();"; Write2Log(LogFileName, temp2.str()); }
          //  if (DebugMode){ stringstream temp; temp << "**Learn:" << (void*) Learn; Write2Log(LogFileName,temp.str()); }
          //  if (DebugMode){ stringstream temp; temp << "Algorithm:" << Algorithm; Write2Log(LogFileName,temp.str()); }

            // Initialize the parameters
            if (Algorithm == 0){
                Learn[Algorithm]->SetC(C);
                Learn[Algorithm]->SetEpsilon(Epsilon);
                Learn[Algorithm]->SetKernelType(KernelTypeSVR);
                Learn[Algorithm]->SetKernelParam1(KernelParam);
            }
            else if (Algorithm == 1){
                Learn[Algorithm]->SetC2((float) C);
                Learn[Algorithm]->SetEpsilon2((float) Epsilon);
            }
            else if (Algorithm == 2){
                Learn[Algorithm]->SetCapacity((int) C);
                Learn[Algorithm]->SetS20( Epsilon);
                Learn[Algorithm]->SetKernelGP(KernelTypeOGP);
            }
            
	    Learn[Algorithm]->InitMeanStd(NoOfFeatures);
            int help = 0;

            Write2Log(LogFileName, "CV: Set the parameters' lists");
            // Train with N-1 SubSet
            for (j=0; j<SetX->GetLength(); j++) {
                    if (DebugMode){ stringstream temp; temp << "CV:: start iteration on j:" << j; Write2Log(LogFileName,temp.str()); }
                    if (i != j) {
                            for (k = 0; k<SetY->GetValue(j)->GetLength(); k++){
                                if (DebugMode){ stringstream temp; temp << "CV:: start iteration on k:" << k; Write2Log(LogFileName,temp.str()); }

                                help++;
                                if (DebugMode){ stringstream temp; temp << "Help: "<< help << endl; Write2Log(LogFileName,temp.str());  }

				//FeaturesSet->AddRowRef((SetX->GetValue(j))->GetRowRef(k));
                                //FeaturesSet->AddRowCopy((SetX->GetValue(j))->GetRowCopy(k));
                                FeaturesSet->AddRowCopy((SetX->GetValue(j))->GetRowRef(k));
                                Labels->Add((SetY->GetValue(j))->GetValue(k));
                                Learn[Algorithm]->Train(FeaturesSet, Labels, help, LoadModelFromFile);
                                if (DebugMode){ stringstream temp; temp << "CV:: end iteration on k:" << k; Write2Log(LogFileName,temp.str()); }
                            }
                            
                    }
                  //  if (DebugMode){ stringstream temp; temp << "CV:: end iteration on j:" << j << endl; Write2Log(LogFileName,temp.str()); }
            }
	    //sleep(10);
            // Test with the other SubSet
           // if (DebugMode){ stringstream temp; temp << "CV:: Test on the following matrix of dimensions : " << SetX->GetValue(i)->GetLengthRows() << " " <<  SetX->GetValue(i)->GetLengthCols()<< endl; Write2Log(LogFileName,temp.str());}
            Vector<double>* ErrorList = Learn[Algorithm]->Margin(SetX->GetValue(i),SetY->GetValue(i));
           // if (DebugMode){ Write2Log(LogFileName,"CV:: Computed the errors");}
            double MeanError = ErrorList->AbsSum() / static_cast<double>(SetX->GetValue(i)->GetLengthRows());
            Errors->Add(MeanError);
           // if (DebugMode){ Write2Log(LogFileName,"CV:: Computed the mean");}
            
            // Free some memory
            delete ErrorList;
            if (DebugMode){ Write2Log(LogFileName, "Deleted ErrorList"); }
            delete Labels;
            if (DebugMode){ Write2Log(LogFileName, "Deleted Labels"); }
            delete FeaturesSet;
            if (DebugMode){ Write2Log(LogFileName, "Deleted FeaturesSet"); }
	    if (DebugMode){ stringstream temp; temp << "CV:: end iteration on j:" << j << endl; Write2Log(LogFileName,temp.str()); }
            Learn[Algorithm]->ClearMeanStd();
            destroyLearningAdaptors(Learn);
            if (DebugMode){ Write2Log(LogFileName, "Deleted Learn"); }
            if (DebugMode){ Write2Log(LogFileName, "Deleted stuff to free memory"); }
            if (DebugMode){ stringstream temp2; temp2 <<"for loop end i:" << i; Write2Log(LogFileName, temp2.str()); }
        }

        // Free the memory
        double MeanError = Errors->AbsSum() / Errors->GetLength();		
        delete Errors;
        return MeanError;
}

double CrossValidation (Vector<onlinesvr::Matrix<double>*>* SetX, Vector<Vector<double>*>* SetY, double Epsilon, double C, double KernelParam, int LoadModelFromFile)
{
        if (DebugMode){ stringstream temp2; temp2 <<"double CrossValidation (....., int LoadModelFromFile"; Write2Log(LogFileName, temp2.str()); }
        // Variables
        Vector<double>* Errors = new Vector<double>();
        int i, j;

        // Subdivide TrainingSet into SubSets
        for (i=0; i<SetX->GetLength(); i++)
        {
                // New SVR
                OnlineSVR* SVR = new OnlineSVR();
                SVR->SetEpsilon(Epsilon);
                SVR->SetC(C);
                SVR->SetKernelType(OnlineSVR::KERNEL_LINEAR);
                SVR->SetKernelParam(KernelParam);
                SVR->SetVerbosity(OnlineSVR::VERBOSITY_NO_MESSAGES);
                // Train with N-1 SubSet
                for (j=0; j<SetX->GetLength(); j++) {
                        if (i != j) {					
                                SVR->Train(SetX->GetValue(j), SetY->GetValue(j));
                        }
                }
                // Test with the other SubSet
                Vector<double>* ErrorList = SVR->Margin(SetX->GetValue(i),SetY->GetValue(i));			
                double MeanError = ErrorList->AbsSum() / static_cast<double>(SetX->GetValue(i)->GetLengthRows());
                delete ErrorList;
                // Free
                Errors->Add(MeanError);
                delete SVR;
        }

        double MeanError = Errors->AbsSum() / Errors->GetLength();		
        // Free the memory
        delete Errors;
        return MeanError;
}



#endif	/* CROSSVALIDATION_H */

