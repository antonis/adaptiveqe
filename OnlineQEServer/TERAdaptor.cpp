#include <iostream>
#include <fstream>
#include <string.h>
#include <vector>
#include "tools.h"
#include "hashMapStringInfos.h"
#include "tercalc.h"
#include "stringInfosHasher.h"
#include "sgmlDocument.h"
#include "multiTxtDocument.h"
#include "xmlStructure.h"
#include "tools.h"
#include "multiEvaluation.h"

using namespace TERCpp;
using namespace std;
using namespace HashMapSpace;

class ExecuteInterface {
  public:
    // 1. Specify the new interface
    virtual ~ExecuteInterface(){}
    virtual float execute(string ref, string hyp) = 0;
};

// 2. Design a "wrapper" or "adapter" class
template <class TYPE>
class ExecuteAdapter: public ExecuteInterface {
  public:
    ExecuteAdapter(TYPE *o, float(TYPE:: *m)(string ref, string hyp)) {
      object = o;
      method = m;
    }
    ~ExecuteAdapter() {
      delete object;
    }

    // 4. The adapter/wrapper "maps" the new to the legacy implementation
    float execute(string ref, string hyp) {  /* the new */
      (object->*method)(ref, hyp);
    }
  private:
    TYPE *object; // ptr-to-object attribute

    float(TYPE:: *method)(string ref, string hyp); /* the old */     // ptr-to-member-function attribute
};


// The old: three totally incompatible classes
// no common base class,
class Fea {
  public:
  // no hope of polymorphism
  ~Fea() {
    cout << "Fea::dtor" << endl;
  }
  void doThis() {
    cout << "Fea::doThis()" << endl;
  }
};

class Feye {
  public:~Feye() {
    cout << "Feye::dtor" << endl;
  }
  void doThat() {
    cout << "Feye::doThat()" << endl;
  }
};

class Pheau {
  public:
  ~Pheau() {
    cout << "Pheau::dtor" << endl;
  }
  void doTheOther() {
    cout << "Pheau::doTheOther()" << endl;
  }
};

class TerCpp {
public:
    ~TerCpp(){
        cout << "TerCpp::dtor" << endl;
    }
    float getTERScore(string ref, string hyp){
        param p;
        
        // Init Parameters
        p.debugMode=false;
        p.debugLevel=-1;
        p.caseOn=false;
        p.noPunct=false;
        p.normalize=false;
        p.tercomLike=false;
        p.sgmlInputs=false;
        p.noTxtIds=false;
        p.printAlignmentsToFile = false;
        p.printAlignmentsToSTDO = false;
        p.printDifferenceToHtml = false;
        p.tokenizedText = false; 
        p.atSentenceLevel = false; 
        p.WER=false;
        p.blockSizePercent=0;
        
        // Set Parameters
        p.referenceSentence = ref;
        p.hypothesisSentence = hyp;
        p.noTxtIds = true;
        p.caseOn = true;
        p.noPunct = true;
        
        multiEvaluation l_eval(p);
        
        if (!p.sgmlInputs)
        {

            if (((int)p.referenceSentence.size()!=0) && ((int)p.hypothesisSentence.size()!=0))
            {
                    l_eval.addReferenceSentence();
                    l_eval.setHypothesisSentence();
                    l_eval.launchTxtEvaluationAlignToCout();
            }else{
                    l_eval.addReferences();
                    l_eval.setHypothesis();
                    //l_eval.launchTxtEvaluation();
                    l_eval.launchTxtEvaluationAlignToCout();
            }
        }
        else
        {
            l_eval.addSGMLReferences();
            l_eval.setSGMLHypothesis();
            l_eval.launchSGMLEvaluation();

        }
        
        return 1.0;

    
    }
};

/* the new is returned */
ExecuteInterface **initialize() {
  ExecuteInterface **array = new ExecuteInterface *[1];

  /* the old is below */
  //array[0] = new ExecuteAdapter < Fea > (new Fea(), &Fea::doThis);
  //array[1] = new ExecuteAdapter < Feye > (new Feye(), &Feye::doThat);
  //array[2] = new ExecuteAdapter < Pheau > (new Pheau(), &Pheau::doTheOther);
  array[0] = new ExecuteAdapter < TerCpp > (new TerCpp(), &TerCpp::getTERScore);
  return array;
}

//int main() {
//  ExecuteInterface **objects = initialize();
//  
//  for (int i = 0; i < 1; i++) {
//   objects[i]->execute();
//  }
// 
//  // 3. Client uses the new (polymporphism)
//  for (int i = 0; i < 3; i++) {
//    delete objects[i];
//  }
//  delete objects;
//  return 0;
//}
