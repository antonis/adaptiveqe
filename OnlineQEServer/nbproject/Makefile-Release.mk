#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/newmat/newmat5.o \
	${OBJECTDIR}/tinyxml.o \
	${OBJECTDIR}/newmat/myexcept.o \
	${OBJECTDIR}/Sofiaml.o \
	${OBJECTDIR}/newmat/newmatrm.o \
	${OBJECTDIR}/Show.o \
	${OBJECTDIR}/terAlignment.o \
	${OBJECTDIR}/newmat/fft.o \
	${OBJECTDIR}/newmat/svd.o \
	${OBJECTDIR}/Variations.o \
	${OBJECTDIR}/multiTxtDocument.o \
	${OBJECTDIR}/SFDataSet.o \
	${OBJECTDIR}/newmat/evalue.o \
	${OBJECTDIR}/TERCpp.o \
	${OBJECTDIR}/SFWeightVector.o \
	${OBJECTDIR}/SFHashInline.o \
	${OBJECTDIR}/CrossValidation.o \
	${OBJECTDIR}/newmat/submat.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/newmat/bandmat.o \
	${OBJECTDIR}/terShift.o \
	${OBJECTDIR}/tercalc.o \
	${OBJECTDIR}/File.o \
	${OBJECTDIR}/multiEvaluation.o \
	${OBJECTDIR}/segmentStructure.o \
	${OBJECTDIR}/newmat/hholder.o \
	${OBJECTDIR}/TrainInstance.o \
	${OBJECTDIR}/tinystr.o \
	${OBJECTDIR}/stringInfosHasher.o \
	${OBJECTDIR}/hashMapStringInfos.o \
	${OBJECTDIR}/Forget.o \
	${OBJECTDIR}/hashMap.o \
	${OBJECTDIR}/newmat/newmatex.o \
	${OBJECTDIR}/infosHasher.o \
	${OBJECTDIR}/alignmentStruct.o \
	${OBJECTDIR}/stringHasher.o \
	${OBJECTDIR}/Train.o \
	${OBJECTDIR}/tinyxmlerror.o \
	${OBJECTDIR}/sgmlDocument.o \
	${OBJECTDIR}/SOGP.o \
	${OBJECTDIR}/SFHashWeightVector.o \
	${OBJECTDIR}/newmat/newmat9.o \
	${OBJECTDIR}/newmat/sort.o \
	${OBJECTDIR}/helping.o \
	${OBJECTDIR}/SFSparseVector.o \
	${OBJECTDIR}/newmat/newmat7.o \
	${OBJECTDIR}/newmat/jacobi.o \
	${OBJECTDIR}/tools.o \
	${OBJECTDIR}/SofiamlMethods.o \
	${OBJECTDIR}/Kernell.o \
	${OBJECTDIR}/tinyxmlparser.o \
	${OBJECTDIR}/newmat/newmat6.o \
	${OBJECTDIR}/newmat/newmat4.o \
	${OBJECTDIR}/xmlStructure.o \
	${OBJECTDIR}/Stabilize.o \
	${OBJECTDIR}/documentStructure.o \
	${OBJECTDIR}/newmat/newfft.o \
	${OBJECTDIR}/SOGP_aux.o \
	${OBJECTDIR}/newmat/newmat1.o \
	${OBJECTDIR}/newmat/cholesky.o \
	${OBJECTDIR}/newmat/newmat8.o \
	${OBJECTDIR}/OnlineSVR.o \
	${OBJECTDIR}/newmat/newmat2.o \
	${OBJECTDIR}/newmat/newmat3.o \
	${OBJECTDIR}/hashMapInfos.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-I /usr/local/include
CXXFLAGS=-I /usr/local/include

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-L/usr/local/include -L/usr/local/Cellar/boost/1.55.0_2/lib -Lnewmat

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/onlineqeserver

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/onlineqeserver: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -lboost_regex -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/onlineqeserver ${OBJECTFILES} ${LDLIBSOPTIONS} 

${OBJECTDIR}/newmat/newmat5.o: newmat/newmat5.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/newmat5.o newmat/newmat5.cpp

${OBJECTDIR}/tinyxml.o: tinyxml.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/tinyxml.o tinyxml.cpp

${OBJECTDIR}/newmat/myexcept.o: newmat/myexcept.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/myexcept.o newmat/myexcept.cpp

${OBJECTDIR}/Sofiaml.o: Sofiaml.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/Sofiaml.o Sofiaml.cpp

${OBJECTDIR}/newmat/newmatrm.o: newmat/newmatrm.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/newmatrm.o newmat/newmatrm.cpp

${OBJECTDIR}/Show.o: Show.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/Show.o Show.cpp

${OBJECTDIR}/terAlignment.o: terAlignment.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/terAlignment.o terAlignment.cpp

${OBJECTDIR}/newmat/fft.o: newmat/fft.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/fft.o newmat/fft.cpp

${OBJECTDIR}/newmat/svd.o: newmat/svd.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/svd.o newmat/svd.cpp

${OBJECTDIR}/Variations.o: Variations.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/Variations.o Variations.cpp

${OBJECTDIR}/multiTxtDocument.o: multiTxtDocument.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/multiTxtDocument.o multiTxtDocument.cpp

${OBJECTDIR}/SFDataSet.o: SFDataSet.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/SFDataSet.o SFDataSet.cpp

${OBJECTDIR}/newmat/evalue.o: newmat/evalue.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/evalue.o newmat/evalue.cpp

${OBJECTDIR}/TERCpp.o: TERCpp.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/TERCpp.o TERCpp.cpp

${OBJECTDIR}/SFWeightVector.o: SFWeightVector.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/SFWeightVector.o SFWeightVector.cpp

${OBJECTDIR}/SFHashInline.o: SFHashInline.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/SFHashInline.o SFHashInline.cpp

${OBJECTDIR}/CrossValidation.o: CrossValidation.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/CrossValidation.o CrossValidation.cpp

${OBJECTDIR}/newmat/submat.o: newmat/submat.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/submat.o newmat/submat.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/newmat/bandmat.o: newmat/bandmat.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/bandmat.o newmat/bandmat.cpp

${OBJECTDIR}/terShift.o: terShift.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/terShift.o terShift.cpp

${OBJECTDIR}/tercalc.o: tercalc.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/tercalc.o tercalc.cpp

${OBJECTDIR}/File.o: File.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/File.o File.cpp

${OBJECTDIR}/multiEvaluation.o: multiEvaluation.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/multiEvaluation.o multiEvaluation.cpp

${OBJECTDIR}/segmentStructure.o: segmentStructure.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/segmentStructure.o segmentStructure.cpp

${OBJECTDIR}/newmat/hholder.o: newmat/hholder.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/hholder.o newmat/hholder.cpp

${OBJECTDIR}/TrainInstance.o: TrainInstance.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/TrainInstance.o TrainInstance.cpp

${OBJECTDIR}/tinystr.o: tinystr.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/tinystr.o tinystr.cpp

${OBJECTDIR}/stringInfosHasher.o: stringInfosHasher.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/stringInfosHasher.o stringInfosHasher.cpp

${OBJECTDIR}/hashMapStringInfos.o: hashMapStringInfos.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/hashMapStringInfos.o hashMapStringInfos.cpp

${OBJECTDIR}/Forget.o: Forget.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/Forget.o Forget.cpp

${OBJECTDIR}/hashMap.o: hashMap.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/hashMap.o hashMap.cpp

${OBJECTDIR}/newmat/newmatex.o: newmat/newmatex.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/newmatex.o newmat/newmatex.cpp

${OBJECTDIR}/infosHasher.o: infosHasher.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/infosHasher.o infosHasher.cpp

${OBJECTDIR}/alignmentStruct.o: alignmentStruct.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/alignmentStruct.o alignmentStruct.cpp

${OBJECTDIR}/stringHasher.o: stringHasher.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/stringHasher.o stringHasher.cpp

${OBJECTDIR}/Train.o: Train.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/Train.o Train.cpp

${OBJECTDIR}/tinyxmlerror.o: tinyxmlerror.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/tinyxmlerror.o tinyxmlerror.cpp

${OBJECTDIR}/sgmlDocument.o: sgmlDocument.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/sgmlDocument.o sgmlDocument.cpp

${OBJECTDIR}/SOGP.o: SOGP.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/SOGP.o SOGP.cpp

${OBJECTDIR}/SFHashWeightVector.o: SFHashWeightVector.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/SFHashWeightVector.o SFHashWeightVector.cpp

${OBJECTDIR}/newmat/newmat9.o: newmat/newmat9.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/newmat9.o newmat/newmat9.cpp

${OBJECTDIR}/newmat/sort.o: newmat/sort.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/sort.o newmat/sort.cpp

${OBJECTDIR}/helping.o: helping.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/helping.o helping.cpp

${OBJECTDIR}/SFSparseVector.o: SFSparseVector.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/SFSparseVector.o SFSparseVector.cpp

${OBJECTDIR}/newmat/newmat7.o: newmat/newmat7.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/newmat7.o newmat/newmat7.cpp

${OBJECTDIR}/newmat/jacobi.o: newmat/jacobi.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/jacobi.o newmat/jacobi.cpp

${OBJECTDIR}/tools.o: tools.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/tools.o tools.cpp

${OBJECTDIR}/SofiamlMethods.o: SofiamlMethods.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/SofiamlMethods.o SofiamlMethods.cpp

${OBJECTDIR}/Kernell.o: Kernell.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/Kernell.o Kernell.cpp

${OBJECTDIR}/tinyxmlparser.o: tinyxmlparser.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/tinyxmlparser.o tinyxmlparser.cpp

${OBJECTDIR}/newmat/newmat6.o: newmat/newmat6.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/newmat6.o newmat/newmat6.cpp

${OBJECTDIR}/newmat/newmat4.o: newmat/newmat4.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/newmat4.o newmat/newmat4.cpp

${OBJECTDIR}/xmlStructure.o: xmlStructure.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/xmlStructure.o xmlStructure.cpp

${OBJECTDIR}/Stabilize.o: Stabilize.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/Stabilize.o Stabilize.cpp

${OBJECTDIR}/documentStructure.o: documentStructure.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/documentStructure.o documentStructure.cpp

${OBJECTDIR}/newmat/newfft.o: newmat/newfft.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/newfft.o newmat/newfft.cpp

${OBJECTDIR}/SOGP_aux.o: SOGP_aux.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/SOGP_aux.o SOGP_aux.cpp

${OBJECTDIR}/newmat/newmat1.o: newmat/newmat1.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/newmat1.o newmat/newmat1.cpp

${OBJECTDIR}/newmat/cholesky.o: newmat/cholesky.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/cholesky.o newmat/cholesky.cpp

${OBJECTDIR}/newmat/newmat8.o: newmat/newmat8.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/newmat8.o newmat/newmat8.cpp

${OBJECTDIR}/OnlineSVR.o: OnlineSVR.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/OnlineSVR.o OnlineSVR.cpp

${OBJECTDIR}/newmat/newmat2.o: newmat/newmat2.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/newmat2.o newmat/newmat2.cpp

${OBJECTDIR}/newmat/newmat3.o: newmat/newmat3.cpp 
	${MKDIR} -p ${OBJECTDIR}/newmat
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmat/newmat3.o newmat/newmat3.cpp

${OBJECTDIR}/hashMapInfos.o: hashMapInfos.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/hashMapInfos.o hashMapInfos.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/onlineqeserver

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
