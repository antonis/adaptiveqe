/* 
 * File:   OnlineGPInterface.h
 * Author: anastas
 *
 * Created on June 30, 2014, 1:12 PM
 */

#ifndef ONLINEGPINTERFACE_H
#define	ONLINEGPINTERFACE_H

#include "OnlineSVR.h" // Needed for OnlineSVR
#include "SOGP.h" // Needed for OnlineGP
#include "Sofiaml.h" // Needed for all Sofiaml Algorithms
#include "SofiaInterface.h"
#include <iostream>
#include <map>
#include <cstring>
#include <sstream> 
#include "helping.h"
using namespace std;
using namespace onlinesvr; 

extern int DebugMode;
extern string LogFileName;

class OnlineGPAdapter : public SOGP {
public:
    OnlineGPAdapter():SOGP(){
	if (DebugMode){ stringstream temp; temp << "DebugMode: " << DebugMode << " OnlineGPAdapter::OnlineGPAdapter()" << endl; Write2Log(LogFileName, temp.str()); }
    }
    //OnlineSVRAdapter(const OnlineSVRAdapter& orig);
    virtual ~OnlineGPAdapter(){
	if (DebugMode){ stringstream temp; temp << "DebugMode: " << DebugMode << " OnlineGPAdapter::~OnlineGPAdapter()" << endl; Write2Log(LogFileName, temp.str()); }
    }

    Vector<double>* means;// = new Vector<double>();
    Vector<double>* stdev;// = new Vector<double>();
    int FeaturesNo;
    int NoOfPreComputed;
    int CurrentInstances;

/*
    string LogFileName; 
    int DebugMode;
*/
  
    void initLog(string LogF, int DebugM){
        LogFileName = LogF;
        DebugMode = DebugM;
        
    }
    
    void clearMeansStDev(){
        delete means;
        delete stdev;
    }

    void initMeansStDev(int featureNo){
        means = new Vector<double>();
        stdev = new Vector<double>();
        FeaturesNo = featureNo;
        NoOfPreComputed = 0;
        CurrentInstances = 0;

        for (int i=0; i<featureNo; i++){
            means->Add(0.0);
            stdev->Add(0.0);
        }
    }
    
  
    
    float myPredict(onlinesvr::Matrix<double>* Feats, std::map<int, float> FeatDict){
        int dim = FeaturesNo;
        int N =  Feats->GetLengthRows();
        
        if(DebugMode){ 
            stringstream temp3;
            temp3 << "OnlineGP:: Predict:: N = " << N << " dim: " << dim;

            Write2Log(LogFileName, temp3.str());
        
        }
        // Change: No need to check if N == 0, problem with LoadModel
        if(CurrentInstances+NoOfPreComputed==0){
            //if( == 0){
            means = new Vector<double>();
            stdev = new Vector<double>();
            for( std::map<int,float>::iterator j=FeatDict.begin(); j!=FeatDict.end(); j++){
                means->Add(0.0);
                stdev->Add(0.0);
            }

            return 0.0;
        }
       
        else{
            int i = 1;
            NEWMAT::Matrix in(dim,1);
            NEWMAT::Matrix ans;

            for( std::map<int,float>::iterator j=FeatDict.begin(); j!=FeatDict.end(); j++){
                // Normalise and store
                if (stdev->GetValue(i-1) == 0) in(i,1) = 0.0;
                else in(i,1) = (((*j).second - means->GetValue(i-1))/stdev->GetValue(i-1));
                i++;
            }
          
            
            ans = this->predictM(in);
           
            
            if (ans.AsScalar() < 0.0 ) ans(1,1) = 0.0;
            if (ans.AsScalar() > 1.0 ) ans(1,1) = 1.0;
	    if (isnan(ans(1,1))) ans(1,1) =1.0;

            float answer = ans(1,1);
            //return ans.AsScalar();
            return answer;
            //return this->OnlineSVR::Predict(TestX);
        }
        //else return 0.0;
    }
   
    
    void myTrain(onlinesvr::Matrix<double>* Feats, Vector<double>* Labels, int CurrentIt, int LoadModelFromFile){

        
       if(DebugMode){
            stringstream temp5;
          
            temp5 << "OnlineGP:: Train:: ID: "<< CurrentIt;
            Write2Log(LogFileName, temp5.str());
        }
        this->CurrentInstances = Labels->GetLength() + NoOfPreComputed;
                
        int N = CurrentInstances;

        if(DebugMode){
            stringstream temp5;
          
            temp5 << "OnlineGP:: Train:: N: "<< N;
            Write2Log(LogFileName, temp5.str());
        }
        
        int dim = FeaturesNo;
        
        
        // Compute Normalisation parameters for each row
        if (LoadModelFromFile == 0){
            if (N==1){
                stringstream te;
                //Write2Log("./QE.log", "OnlineGP:: Train:: Will instantiate means and stdev with the first instance");
                for (int i=0; i<dim; i++){           
                    stdev->SetValue(i, 0.0);

                    means->SetValue(i, Feats->GetValue(0,i));
                    //te<< means->GetValue(i)<<" ";
                }
               // Write2Log("./QE.log", te.str());
            }
            else{
        
                
                for (int i = 0; i < dim; i++){
                    double newmean = ((N-1)*means->GetValue(i) + Feats->GetValue(N-1,i))/((double) N);
                    double newvar = ((((double)N)-2)/(((double) N)-1)) *(stdev->GetValue(i) * stdev->GetValue(i)) + (1.0/((double) N)) * (Feats->GetValue(N-1, i) -  means->GetValue(i)) * (Feats->GetValue(N-1, i) -  means->GetValue(i));

                    stdev->SetValue(i, sqrt(newvar));
                    means->SetValue(i, newmean);

                  
                }

            }
        }
        else{
           
            
            for (int i = 0; i < dim; i++){
                double newmean = ((N-1)*means->GetValue(i) + Feats->GetValue(Labels->GetLength()-1,i))/((double) N);
                 double newvar = ((((double)N)-2)/(((double) N)-1)) *(stdev->GetValue(i) * stdev->GetValue(i)) + (1.0/((double) N)) * (Feats->GetValue((Labels->GetLength()-1), i) -  means->GetValue(i)) * (Feats->GetValue((Labels->GetLength()-1), i) -  means->GetValue(i));

                stdev->SetValue(i, sqrt(newvar));
                means->SetValue(i, newmean);

               
            }
        
        }
         //Train : this if (N>0) is probably redundant
        if (N>0){
            NEWMAT::Matrix in(dim,1), out(1,1);
            //float Label = Labels->GetValue(N-1);
            out(1,1) = Labels->GetValue(Labels->GetLength() -1);
            // Clips the output in [0,1] range
            if (out(1,1) < 0.0) out(1,1) = 0.0;
            if (out(1,1) > 1.0) out(1,1) = 1.0;
                
           
            stringstream temp;
            for (int j = 0; j < dim; j++){
                // Normalise and store 
                if (stdev->GetValue(j) == 0) {
                    in(j+1,1) = 0.0;
                    temp <<in(j+1,1) << " ";
                }
                else {
                    
                    in(j+1,1) = ((Feats->GetValue(Labels->GetLength() -1,j) - means->GetValue(j))/(stdev->GetValue(j)));
                    temp <<in(j+1,1) << " ";
                }
  

            }

            if(DebugMode){
                    
                    Write2Log(LogFileName, temp.str());
                    temp << "OnlineGP:: Train:: Normalised correctly ";

            }
  
            this->addM(in,out);


        }
    }
    
    Vector<double>* myMargin(onlinesvr::Matrix<double>* X, Vector<double>* Y){
        int dim = X->GetLengthCols();
        int N = X->GetLengthRows();
  /*      NEWMAT::Matrix ans;
        NEWMAT::Matrix in(dim,N), out(1,N);
        Vector<double> Answer(N);
        
        for (int i = 0; i<N; i++){
            for (int j = 0; j<dim; j++){
                in(j+1,i+1) = X->GetValue(i, j);
            }
            out(1,i+1) = Y->GetValue(i);
        }
        
        ans = this->predictM(in);
*/
        Vector<double> Answer(N);
        NEWMAT::Matrix in(dim,N), out(1,N);
        NEWMAT::Matrix ans;

        stringstream temp;
        for (int i = 0; i<N; i++){
            for (int j = 0; j<dim; j++){
                in(j+1,i+1) = (X->GetValue(i, j) - means->GetValue(j))/stdev->GetValue(j);
                temp << in(j+1,i+1) << " ";
                
            }
           // if(DebugMode){
                    
            //        Write2Log(LogFileName, temp.str());
            //      temp << "OnlineGP:: Margin:: Normalized Features ";
            //        temp.clear();
            //}
            
            
            out(1,i+1) = Y->GetValue(i);
        }
            
        
        
            
        ans = this->predictM(in);
        
        
       //   stringstream temp;
        for (int i=0; i<N; i++){
            //cap the predictions if smaller than 0 and larger than 1
            double val = ans(1,i+1);
            if (val < 0.0) val = 0.0;
            else if (val > 1.0) val = 1.0;
            
             temp <<val << " ";
            Answer.AddAt((val - Y->GetValue(i)), i);

        }
          
        if(DebugMode){

                 Write2Log(LogFileName, temp.str());
                 temp << "OnlineGP:: Margin:: Predicted values ";
                 temp.clear();
         }
          
        //delete in, out;
        return Answer.Clone();
    }
    
    void myLoad(char* filename){
    
        this->load2(filename,true);
      
    }
 
    
    void mySave(char* filename){
        this->save2(filename,true);
    }
    
    bool save2(const char *fn,bool ascii=true){
    FILE *fp = fopen(fn,"w");
    int ret=printTo(fp,ascii);
    if(ascii){
        fprintf(fp,"%d\t%d\n", FeaturesNo, CurrentInstances);
        for (int i=0; i<FeaturesNo; i++)
            fprintf(fp,"%f\t", means->GetValue(i));
        fprintf(fp,"\n");
        for (int i=0; i<FeaturesNo; i++)
            fprintf(fp,"%f\t", stdev->GetValue(i));
        fprintf(fp,"\n");
    }
    else{
        fwrite(&(FeaturesNo),sizeof(int),1,fp);
        fwrite(&("\t"),sizeof(char),1,fp);
        fwrite(&(CurrentInstances),sizeof(int),1,fp);
        fwrite(&("\n"),sizeof(char),1,fp);
        for (int i=0; i<FeaturesNo; i++){
            double tmean = means->GetValue(i);
            fwrite(&(tmean), sizeof(double),1,fp);
            fwrite(&("\t"),sizeof(char),1,fp);
        }
        fwrite(&("\n"),sizeof(char),1,fp);
        for (int i=0; i<FeaturesNo; i++){
            double tsd = stdev->GetValue(i);
            fwrite(&(tsd), sizeof(double),1,fp);
            fwrite(&("\t"),sizeof(char),1,fp);
        }
        fwrite(&("\n"),sizeof(char),1,fp);
    }
	
    fclose(fp);
    if(DebugMode){
        stringstream temp;
        temp << "OnlineGP:: Saved the model to file.";
        Write2Log(LogFileName, temp.str());
    }
    
    return ret;
  }
  //bool printTo(FILE *fp,bool ascii=false);
  bool load2(const char *fn,bool ascii=true){
    FILE *fp = fopen(fn,"r");
    int ret=readFrom(fp,ascii);
        
    if(ascii){
        int tf, tci;
        fscanf(fp,"%d",&tf);
        fscanf(fp, "\t");
        fscanf(fp, "%d", &tci);
        this->FeaturesNo = tf;
        this->NoOfPreComputed = tci;
        fscanf(fp, "\n");
        //std::string means_string, std_string;
        //std::getline(fp,means_string);
        char means_string[4000];
        char std_string[4000];
        fgets(means_string, 4000, fp);
        fgets(std_string, 4000, fp);
        //std::getline(fp,std_string);
        
        stringstream mtemp(means_string);
        stringstream stdtemp(std_string);
        if(DebugMode){

            Write2Log(LogFileName, mtemp.str());
            

            Write2Log(LogFileName, stdtemp.str());
        }
        means = new Vector<double>(FeaturesNo);
        stdev = new Vector<double>(FeaturesNo);
        for (int i = 0; i<FeaturesNo; i++){
              double m,s;
              mtemp >> m;
              stdtemp >> s;
              means->AddAt(m,i);
              stdev->AddAt(s,i );
        }
          
          
        
     
        
    }
    else{
        int tf, tci;
        char tempc;
        fread(&(tf),sizeof(int),1,fp);
        fread(&(tempc),sizeof(char),1,fp);
        fread(&(tci),sizeof(int),1,fp);
        this->FeaturesNo = tf;
        this->NoOfPreComputed = tci;
        fread(&(tempc), sizeof(char),1,fp);
        for (int i=0; i<FeaturesNo; i++){
            double tmean;
            fread(&(tmean), sizeof(double),1,fp);
            means->Add(tmean);
            fread(&(tempc), sizeof(char),1,fp);
        }
        fread(&(tempc), sizeof(char),1,fp);
        for (int i=0; i<FeaturesNo; i++){
            double tsd;
            fread(&(tsd), sizeof(double),1,fp);
            stdev->Add(tsd);
            fread(&(tempc), sizeof(char),1,fp);
        }
    }
    fclose(fp);
    return ret;
  }


    
    string myGetWeights(){
 
    }
    
    void myPrintWeights(char* WeightsFileName){

    }
    
    void doNothing0(){}
    void doNothing1(double i){}
    void doNothing5(int i){}
    void doNothing6(EtaType e){}
    void doNothing7(float f){}
    float doNothing2(){}
    int doNothing3(){}
    double doNothing4(){}
    EtaType doNothing8(){}
    float doNothing9(onlinesvr::Matrix<double>* Feats, std::map<int, float> FeatDict){}


    

private:

};



#endif	/* ONLINEGPINTERFACE_H */

