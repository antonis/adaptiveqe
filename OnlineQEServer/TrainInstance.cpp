/* 
 * File:   TrainInstance.cpp
 * Author: anastas
 * 
 * Created on July 4, 2013, 2:21 PM
 */

#include "TrainInstance.h"
#include <cstdlib>
#include <sstream>
//#include <iostream>
#include <iostream>
#include <fstream>
#include <string.h> 
#include <stdio.h>

#include <bitset>      // std::istringstream

using namespace std;

TrainInstance::TrainInstance(int seg_id, string s, string t) {
    //id = new int;
    //src = new string;
    //trg = new string;
    id = seg_id;
    src = s;
    trg = t;
    seen = true;
}

void TrainInstance::setFeatures(int FeatureNo, string FeatureString){
        //stringstream temp3;
       //// temp3 << FeatureString<<"\n";
        //temp3 << "Inside Set Feature ";
  
        typedef basic_istringstream<char> istringstream;
        float temp;
        std::istringstream iss(FeatureString);
        for(int i=1; i<=FeatureNo; i++){
            iss >> temp;
            
            //if (temp < 0.01) temp += 1000;
            //cout << (*i).first << "temp is: " << temp << endl;
            //(*i).second = temp;
            //temp3<<"i:"<<i << "#temp:" <<temp;
            
            FeatureVals.insert(make_pair(i, temp));
        } 
        
        // Write2Log("./QE.log", temp3.str());
}


std::map<int, float> TrainInstance::getFeatures(){
    return FeatureVals;
}

string TrainInstance::getFeaturesstr(){
    stringstream temp;
    for( std::map<int,float>::iterator j=FeatureVals.begin(); j!=FeatureVals.end(); ++j){
             temp << (*j).second << " ";
     }
    return temp.str();
}

void TrainInstance::setID(int i){
    id = i;
}

int TrainInstance::getID(){
    return id;
}

void TrainInstance::setSource(string s){
    src = s;
}

string TrainInstance::getSource(){
    return src;
}

void TrainInstance::setTarget(string s){
    trg = s;
}

string TrainInstance::getTarget(){
    return trg;
}

void TrainInstance::setHTER_Prediction(float pred){
    hterPrediction = pred;
}

float TrainInstance::getHTER_Prediction(){
    return hterPrediction;
}

void TrainInstance::setPostEditorID(int a){
    PostEditorID = a;
}

int TrainInstance::getPostEditorID(){
    return PostEditorID;
}

void TrainInstance::setPostEdited(string s){
    pe = s;
}

string TrainInstance::getPostEdited(){
    return pe;
}

void TrainInstance::setHTER(float hter){
    HTER = hter;
}

float TrainInstance::getHTER(){
    return HTER;
}

void TrainInstance::setSeen(bool s){
    seen = s;
}

bool TrainInstance::getSeen(){
    return seen;
}

void TrainInstance::Write2Log(string LogFileName, string s){
    ofstream LogFile;
    LogFile.open(LogFileName.c_str(), ios::app);
    if (LogFile.is_open()) {
        LogFile << /*buffer <<*/ s << endl;
        LogFile.close();
    }

}
 
//TrainInstance::TrainInstance(const TrainInstance& orig) {
//}

TrainInstance::~TrainInstance() {
    delete &id;
    delete &src;
    delete &trg;
    delete &FeatureVals;
    delete &hterPrediction;
    delete &PostEditorID;
    delete &pe;
    delete &HTER;
    delete &seen;
}

